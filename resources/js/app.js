require('./bootstrap');
window.Vue = require('vue');
import datetime from 'vuejs-datetimepicker';

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('aerolineas-component', require('./components/Aerolineas/AerolineasComponent').default);
Vue.component('vuelos-component', require('./components/Vuelos/VuelosComponent').default);

const app = new Vue({
    el: '#app',
});
