@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row col-md-10">
      <div class="col-md-12 col-md-offset-2 ">
         <div class="panel panel-default">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mt-sm-0">
                  <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                  <li class="breadcrumb-item active"><a href="{{route('airplanes.index')}}"><i class="fas fa-sitemap"></i> Aeroplanos</a></li>
                  <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-edit"></i> Editar Aeroplano</a></li>
                  <li class="breadcrumb-item"><a href="#"></a></li>
               </ol>
            </nav>
            <div class="row">
               <div class="col-lg-12 margin-tb">
                  <div class="pull-left">
                     <h2><i class="fas fa-edit"></i> Editar airplane {{$airplane->nombre_airplane}}</h2>
                  </div>
               </div>
            </div>
            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
               <p>{{ $message }}</p>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
               <strong>Whoops!</strong> Hubo algunos problemas con su entrada!<br><br>
               <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
               </ul>
            </div>
            @endif
            <form action="{{ route('airplanes.update',$airplane->id) }}" method="POST">
               @csrf
               @method('PUT')
               <div class="row">
                  <div class="col-sm-10">
                     <div class="form-group {{ $errors->has('modelo_id') ? 'has-error' : "" }}">
                     <strong>Marca:</strong>
                     <i>
                        <select name="modelo_id" class="form-control">
                           <option value="{{$airplane->modelo->id}}" selected>{{$airplane->modelo->id}}. {{$airplane->modelo->nombre_modelo}}</option>
                           @foreach($modelos as $modelo)
                           <option value="{{$modelo->id}}">{{$modelo->id}}. {{$modelo->nombre_modelo }}</option>
                           @endforeach
                        </select>
                     </i>
                     <div class="help-block"> 
                        <strong>{{ $errors->first('modelo_id', 'Seleccione uno') }}</strong>
                     </div>
                  </div>
               </div>

               <div class="col-sm-10">
                  <div class="form-group {{ $errors->has('nombre_aeroplano') ? 'has-error' : "" }}">
                     <strong>Nombre del Aeroplano:</strong>
                     <i >
                        <input type="text" name="nombre_aeroplano" value="{{ $airplane->nombre_aeroplano }}" class="form-control" placeholder="airplane">
                     </i>
                  <div class="help-block"> 
                     <strong>{{ $errors->first('nombre_aeroplano', 'Ingrese A-Z a-z') }}</strong>
                  </div>
                  </div>
               </div>

               <div class="col-sm-10">
                     <div class="form-group">
                        <strong>Estado:</strong>
                        <select name="estado" class="form-control">
                           <?php if($airplane->estado=='1'): ?>
                           <option value="{{$airplane->estado}}" >Activa</option>
                           <?php endif; ?>
                           <?php if($airplane->estado=='0'): ?>
                           <option value="{{$airplane->estado}}" >Inactiva</option>
                           <?php endif; ?>
                           <option value='1'>Activa</option>
                           <option value='0'>Inactiva</option>
                        </select>
                     </div>
                  </div>
               <div class="col-sm-10">
                  <div class="form-group {{ $errors->has('capacidad') ? 'has-error' : "" }}">
                     <strong>capacidad:</strong>
                     <i >
                        <input type="number" name="capacidad" value="{{ $airplane->capacidad }}" class="form-control" placeholder="airplane" min="0" step="1">
                     </i>
                  <div class="help-block"> 
                     <strong>{{ $errors->first('capacidad', 'Ingrese solo numeros Enteros') }}</strong>
                  </div>
                  </div>
               </div>
               <div class="col-sm-10">
                  <div class="form-group {{ $errors->has('tipo_avion') ? 'has-error' : "" }}">
                     <strong>Marca:</strong>
                     <i>
                     <select name="tipo_avion" class="form-control">
                        <option value="{{$airplane->tipo_avion}}" selected>{{$airplane->tipo_avion}}</option>
                        <option value="Carga">Carga</option>
                        <option value="Transporte de Pasajeros">Transporte de Pasajeros</option>
                        <option value="Entrenamiento">Entrenamiento</option>
                        <option value="Sanitario">Sanitario</option>
                        <option value="Contra Incendios">Contra Incendios</option>
                        <option value="Privado">Privado</option>
                     </select>
                     </i>
                  <div class="help-block"> 
                     <strong>{{ $errors->first('tipo_avion', 'Seleccione uno') }}</strong>
                  </div>
               </div>
         </div>
         <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fas fa-save"></i> Actualizar</button>
            <a class="btn btn-sm btn-danger" href="{{ route('airplanes.index') }}">Cancelar</a>
         </div>
      </div>
      </form>
   </div>
</div>
</div>
</div>
@endsection