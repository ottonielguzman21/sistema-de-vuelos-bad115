@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row col-md-10">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mt-sm-0">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                            <li class="breadcrumb-item active"><a href="{{ route('airplanes.index') }}"><i class="fas fa-sitemap"></i> Aeroplanos</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-eye"></i> Mostrar Aeroplano {{$airplane->id}}</a></li>
                            <li class="breadcrumb-item"><a href="#"></a></li>
                        </ol>
                    </nav>
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h2><i class="fas fa-info-circle"></i> Detalle del Aeroplano</h2>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-primary" href="{{ route('airplanes.index') }}"><i class="fas fa-arrow-left"></i> </a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Modelo:</strong>
                                {{$airplane->modelo->nombre_modelo}}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Nombre del Aeroplano:</strong>
                                {{$airplane->nombre_aeroplano}}
                            </div>
                        </div>
                        <?php if($airplane->estado=='1'): ?>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Estado:</strong>
                                Activa
                            </div>
                        </div> 
                        <?php endif; ?>
                        <?php if($airplane->estado=='0'): ?>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Estado:</strong>
                                Inactiva
                            </div>
                        </div> 
                        <?php endif; ?>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Capacidad:</strong>
                                {{ $airplane->capacidad }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Tipo de Avion:</strong>
                                {{ $airplane->tipo_avion }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
