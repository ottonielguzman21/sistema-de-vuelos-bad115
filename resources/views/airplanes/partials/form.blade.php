      <div class="row">
         <div class="col-sm-2">
            {!! form::label('Modelo') !!}
         </div>
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('modelo_id') ? 'has-error' : "" }}">
            <i>
               <select name="modelo_id" class="form-control">
                  <option disabled selected>Seleccione Modelo</option>
                  @foreach($modelos as $modelo)
                  <option value="{{$modelo->id}}">{{$modelo->id}}. {{$modelo->nombre_modelo }}</option>
                  @endforeach
               </select>
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('modelo_id', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
   <div class="col-sm-2">
      {!! form::label('nombre_aeroplano', 'Nombre del Aeroplano') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('nombre_aeroplano') ? 'has-error' : "" }}">
      <i>{{ Form::text('nombre_aeroplano',NULL, ['class'=>'form-control', 'id'=>'nombre_aeroplano', 'placeholder'=>'Nombre del Aeroplano']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('nombre_aeroplano', 'Ingrese: A-Z a-z') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('capacidad', 'Capacidad') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('capacidad') ? 'has-error' : "" }}">
      <i>{{ Form::number('capacidad',NULL, ['class'=>'form-control', 'id'=>'capacidad', 'placeholder'=>'capacidad=600', 'min'=>'0', 'step'=>'1',]) }}</i>
      <div class="help-block required">
         <strong>{{ $errors->first('capacidad', 'Ingrese numeros enteros Mayores a 0') }}</strong> 
      </div>
   </div>
</div>
</div>
   <div class="row">
         <div class="col-sm-2">
            {!! form::label('Tipo de Avion') !!}
         </div>
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('tipo_avion') ? 'has-error' : "" }}">
            <i>
               <select name="tipo_avion" class="form-control">
                  <option disabled selected>Seleccione Tipo de Avión</option>
                  <option value="Carga">Carga</option>
                  <option value="Transporte de Pasajeros">Transporte de Pasajeros</option>
                  <option value="Entrenamiento">Entrenamiento</option>
                  <option value="Sanitario">Sanitario</option>
                  <option value="Contra Incendios">Contra Incendios</option>
                  <option value="Privado">Privado</option>
               </select>
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('tipo_avion', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
   </div>
<div class="form-group text-center">
   {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
   <a class="btn btn-sm btn-danger" href="{{ route('airplanes.index') }}">Cancelar</a>
</div>