@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row">
      <div class="col-md-12 col-md-offset-2">
         <div class="panel panel-default">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mt-sm-0">
                  <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"> </i>Inicio</a></li>
                  <li class="breadcrumb-item active"><a href="{{url('airplanes')}}"><i class="fas fa-sitemap"></i> Aeroplanos</a></li>
                  <li class="breadcrumb-item"><a href="#"></a></li>
               </ol>
            </nav>
            <div class="panel-heading">
               <h3><b><i class="fas fa-sitemap"></i> Gestión de Aviones (Aeroplanos)</b></h3>
            </div>
            {!! Form::open(['route'=>'airplanes.index','method'=>'GET', 'class'=>'form-inline float-right']) !!}
            <div class="form-group">
               {!! Form::text('tipo_avion', null, ['class' => 'form-control','placeholder'=>'Tipo de Avion']) !!}
            </div>
            <button type="submit" class="btn btn-info "><i class="fas fa-search"></i> Buscar</button>
            {!! Form::close() !!}
            <div class="pull-right">
               <a class="btn btn-success" href="{{ route('airplanes.create') }}"><i class="fas fa-plus"></i>  Crear Nuevo Avión Aeroplano</a>
            </div>
            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
               <p>{{ $message }}</p>
            </div>
            @endif
            <br>
            <table class="table table-bordered table-striped">
               <tr>
                  <th>No</th>
                  <th>Modelo</th>
                  <th>Nombre del Aeroplano</th>
                  <th>Tipo Avión</th>
                  <th>Capacidad</th>
                  <th>Estado</th>
                  <th width="280px">Accion</th>
               </tr>
               @foreach ($airplanes as $airplane)
               <tr>
                  <td>{{ ++$i }}</td>
                  <td>{{ $airplane->modelo->nombre_modelo }}</td>
                  <td>{{ $airplane->nombre_aeroplano }}</td>
                  <td>{{ $airplane->tipo_avion }}</td>
                  <td>{{ $airplane->capacidad }}</td>
                  @if($airplane->estado==1)
                  <td>Activo</td>
                  @endif
                  @if($airplane->estado==0)
                  <td>Inactivo</td>
                  @endif
                  <td width="350px">
                     <form action="{{ route('airplanes.destroy',$airplane->id) }}" method="POST">
                        <a class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{ route('airplanes.show',$airplane->id) }}"><i class="fas fa-eye"></i>Mostrar</a>
                        <a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Editar" href="{{ route('airplanes.edit',$airplane->id) }}"><i class="fas fa-edit"></i>Editar</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i>Eliminar</button>
                     </form>
                  </td>
               </tr>
               @endforeach
            </table>
            {!! $airplanes->links() !!}
         </div>
      </div>
   </div>
</div>
@endsection