@extends('layouts.app')
@section('content')
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-md-offset-2">
                    <div class="alert alert-success">
                       Bienvenido: {{$usuario->name}} (Administrador de Aerolinea)
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-md-offset-2 ">
                    <div class="panel panel-default">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mt-sm-0">
                                <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"> </i>Inicio</a></li>
                                <li class="breadcrumb-item active"><a href="{{url('mi-aerolinea')}}"><i class="fas fa-plane"></i> mi-aerolinea</a></li>
                                <li class="breadcrumb-item"><a href="#"></a></li>
                            </ol>
                        </nav>
                        <div class="panel-heading">
                            <h3><b><i class="fas fa-plane"></i>Gestion de vuelos: {{$aerolinea->nombre_oficial}}</b></h3>
                            <img class="img-fluid" src="/images/{{$aerolinea->url_imagen}}" alt="Logo" width="100" height="100">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <vuelos-component/>
@endsection
