<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Flight Systems &copy; </title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/t_style.css" type="text/css" media="all">
    <script type="text/javascript" src="js/jquery-1.4.2.js" ></script>
    <script type="text/javascript" src="js/cufon-yui.js"></script>
    <script type="text/javascript" src="js/cufon-replace.js"></script>
    <script type="text/javascript" src="js/Myriad_Pro_italic_600.font.js"></script>
    <script type="text/javascript" src="js/Myriad_Pro_italic_400.font.js"></script>
    <script type="text/javascript" src="js/Myriad_Pro_400.font.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/ie6_script_other.js"></script>
    <script type="text/javascript" src="js/html5.js"></script>
    <![endif]-->
</head>
<body id="page1">
<!-- START PAGE SOURCE -->
<div class="body1">
    <div class="main">
        <header>
            <div class="wrapper">
                <h1><a href="#" id="logo">AirLines</a><span id="slogan">Flight Systems &copy;</span></h1>
                <div class="right">

                    <nav>
                        <ul id="menu">
                            <li id="menu_active"><a href="{{ url('/') }}">Inicio</a></li>
                            <li><a href="{{url('aircrafts')}}">Nuestros Servicios</a></li>
                            <li><a href="{{url('safety')}}">Seguridad</a></li>
                            <li><a href="{{url('contact')}}">Contactanos</a></li>
                            @if (Route::has('login'))
                                <div class="top-right links">
                                    @auth
                                        <li><a href="{{ url('/home') }}">Administracion</a></li>
                                    @else
                                        <li><a href="{{ route('login') }}">Inicia Sesion</a></li>

                                        @if (Route::has('register'))
                                            <li><a href="{{ route('register') }}">Registrate</a></li>
                                        @endif
                                    @endauth
                                </div>
                            @endif
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
    </div>
</div>
<div class="main">
    <div id="banner">
        <div class="text1">DESDE <span>EL SALVADOR!</span>
            <p>Viaja hacia cualquier parte del mundo escogiendo la aerolinea que mas se adapte a tus necesidades. Reserva tus boletos ahora mismo.</p>
        </div>
        <a href="#" class="button_top">Adquiere tus boletos</a></div>
</div>
<div class="main">
    <section id="content">
        <article class="col1">
            <div class="pad_1">
                <h2>¡Planea tu vuelo!</h2>
                <form id="form_1" action="#" method="post">
                    <div class="wrapper pad_bot1">
                        <div class="radio marg_right1">
                            <input type="radio" name="name1">
                            Ida y regreso<br>
                            <input type="radio" name="name1">
                            Solo ida</div>
                        <div class="radio">
                            <input type="radio" name="name1">
                            Equipaje Extra<br>
                            <input type="radio" name="name1">
                            Multi-Leg </div>
                    </div>
                    <div class="wrapper"> El Salvador:
                        <div class="bg">
                            <input type="text" class="input input1" value="Ingresa tu Ciudad o Codigo de Aeropuerto" onBlur="if(this.value=='') this.value='Ingresa tu Ciudad o Codigo de Aerepuerto'" onFocus="if(this.value =='Enter City or Airport Code' ) this.value=''">
                        </div>
                    </div>
                    <div class="wrapper"> Destino:
                        <div class="bg">
                            <input type="text" class="input input1" value="Ingresa tu Ciudad o Codigo de Aeropuerto" onBlur="if(this.value=='') this.value='Ingresa tu Ciudad o Codigo de Aeropuerto'" onFocus="if(this.value =='Enter City or Airport Code' ) this.value=''">
                        </div>
                    </div>
                    <div class="wrapper"> Fecha y hora de salida:
                        <div class="wrapper">
                            <div class="bg left">
                                <input type="text" class="input input2" value="mm/dd/yyyy " onBlur="if(this.value=='') this.value='mm/dd/yyyy '" onFocus="if(this.value =='mm/dd/yyyy ' ) this.value=''">
                            </div>
                            <div class="bg right">
                                <input type="text" class="input input2" value="12:00am" onBlur="if(this.value=='') this.value='12:00am'" onFocus="if(this.value =='12:00am' ) this.value=''">
                            </div>
                        </div>
                    </div>
                    <div class="wrapper"> Fecha y hora de regreso:
                        <div class="wrapper">
                            <div class="bg left">
                                <input type="text" class="input input2" value="mm/dd/yyyy " onBlur="if(this.value=='') this.value='mm/dd/yyyy '" onFocus="if(this.value =='mm/dd/yyyy ' ) this.value=''">
                            </div>
                            <div class="bg right">
                                <input type="text" class="input input2" value="12:00am" onBlur="if(this.value=='') this.value='12:00am'" onFocus="if(this.value =='12:00am' ) this.value=''">
                            </div>
                        </div>
                    </div>
                    <div class="wrapper">
                        <p>Pasajero(s):</p>
                        <div class="bg left">
                            <input type="text" class="input input2" value="# passengers" onBlur="if(this.value=='') this.value='# passengers'" onFocus="if(this.value =='# passengers' ) this.value=''">
                        </div>
                        <a href="#" class="button2">Reserva!</a> </div>
                </form>
                <h2>Ultimos vuelos!</h2>
                <p class="under"><a href="#" class="link1">Visita moscu</a><br>
                    Junio 26, 2020</p>
                <p class="under"><a href="#" class="link1">Visita tokio la capital de la tecnologia</a><br>
                    Junio 27, 2020</p>
                <p><a href="#" class="link1">Vacaciones en la Habana, Cuba</a><br>
                    Agosto 6, 2020</p>
            </div>
        </article>
        <!-- articulo para safety-->
        <article class="col2 pad_left1">
            <h2>La Seguridad al Volar</h2>
            <div class="wrapper">
                <figure class="left marg_right1"><img src="images/page3_img1.jpg" alt=""></figure>
                <p><strong>La Seguridad Regulada</strong> En la promoción de la seguridad al volar,  interviene un ente regulador, la autoridad aeronáutica que otorga a la aerolínea un certificado de operador aéreo, que a su vez sigue estándares universales de seguridad y técnicos. Esta autoridad extiende ese certificado, cuando ha recibido la constancia de la aptitud y disponibilidad de recursos de todo tipo. La misma autoridad ejerce luego una vigilancia continuada de la seguridad operacional mediante inspecciones y auditorías. </p>
            </div>
            <div class="wrapper">
                <figure class="left marg_right1"><img src="images/page3_img2.jpg" alt=""></figure>
                <p>En la mayoria de los casos las personas son vistas muchas veces como las que crean los problemas, pero en la práctica la mayoría de las veces son quienes los resuelven.</p>
            </div>
            <div class="wrapper"><a href="#" class="button1">Leer Mas</a></div>
            <h2>¿Que es la seguridad entonces?</h2>
            <p>La seguridad es un deseo y un empeño para no tener accidentes ni incidentes serios. Casi todo el mundo entiende por seguridad la no ocurrencia de accidentes o incidentes que puedan representar daños de cualquier tipo. Por lo tanto, el deseo mayor de nuestra aerolinea, es que sus pasajeros no sufran ningun daño.</p>
            <div class="wrapper pad_bot1">
                <ul class="list1 cols marg_right1">

                </ul>
            </div>
        </article>
        <!-- fin del articulo articulo para safety-->
    </section>
</div>
<div class="body2">
    <div class="main">
        <footer>
            <div class="footerlink">
                <p class="lf">Copyright &copy; 2020 Flight Systems- Grupo 8 -Todos los derechos reservados-</p>
                <p class="lf">-  correo: <span>flightsystems@info.com</span> - (503) 2215-2022</p>
                <p class="rf">&nbsp;<b>BAD-115-Escuela de sistemas informaticos, UES </b></p>
                <div style="clear:both;"></div>
            </div>
        </footer>
    </div>
</div>
<script type="text/javascript"> Cufon.now(); </script>
<!-- END PAGE SOURCE -->
</body>
</html>
