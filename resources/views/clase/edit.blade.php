@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row col-md-10">
            <div class="col-md-12 col-md-offset-2 ">
                <div class="panel panel-default">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mt-sm-0">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                            <li class="breadcrumb-item active"><a href="{{route('clase.index')}}"><i class="fas fa-sitemap"></i> Clases</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-edit"></i> Editar clase</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('#')}}">{{$clase->id}}</a></li>
                            <li class="breadcrumb-item"><a href="#"></a></li>
                        </ol>
                    </nav>
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h2><i class="fas fa-edit"></i> Editar Clases de Vuelos</h2>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-primary" href="{{ route('clase.index') }}"><i class="fas fa-arrow-left"></i> </a>
                            </div>
                        </div>
                    </div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> Hubo algunos problemas con su entrada!<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ route('clase.update',$clase->id) }}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Nombre:</strong>
                                    <input type="text" name="nombre_clase" value="{{ $clase->nombre_clase }}" class="form-control" placeholder="Nombre de la clase">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>Cantidad de asientos:</strong>
                                    <input type="text" name="cantidad_asientos" value="{{ $clase->cantidad_asientos }}" class="form-control" placeholder="Cantidad de asientos">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Guardar</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
