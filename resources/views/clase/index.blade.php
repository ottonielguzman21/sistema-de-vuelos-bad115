@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-2 ">
                <div class="panel panel-default">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mt-sm-0">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"> </i>Inicio</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('clases')}}"><i class="fas fa-sitemap"></i> Clases</a></li>
                            <li class="breadcrumb-item"><a href="#"></a></li>
                        </ol>
                    </nav>
                    <div class="panel-heading">
                        <h3><b><i class="fas fa-sitemap"></i> Gestion de clases de vuelos</b></h3>
                    </div>
                    {!! Form::open(['route'=>'clase.index','method'=>'GET', 'class'=>'form-inline float-right']) !!}
                    <div class="form-group">
                        {!! Form::text('nombre_clase', null, ['class' => 'form-control','placeholder'=>'Nombre']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::text('cantidad_asiento', null, ['class' => 'form-control','placeholder'=>'Asientos']) !!}
                    </div>
                    <button type="submit" class="btn btn-info "><i class="fas fa-search"></i> Buscar</button>
                    {!! Form::close() !!}
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('clase.create') }}"><i class="fas fa-plus"></i>  Crear nueva clase</a>
                    </div>
                    @if ($message = Session::get('info'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>No</th>
                            <th>Nombre</th>
                            <th>Cantidad de Asientos</th>
                            <th width="280px">Accion</th>
                        </tr>
                        @foreach ($classes as $class)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $class->nombre_clase }}</td>
                                <td>{{ $class->cantidad_asientos }}</td>
                                <td width="350px">
                                    <form action="{{ route('clase.destroy',$class->id) }}" method="POST">

                                        <a class="btn btn-info" href="{{ route('clase.show',$class->id) }}"><i class="fas fa-eye"></i> Mostrar</a>

                                        <a class="btn btn-primary" href="{{ route('clase.edit',$class->id) }}"><i class="fas fa-edit"></i> Editar</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i> Eliminar</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>

                    {!! $classes->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
