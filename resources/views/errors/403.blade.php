@extends('errors::layout')

@section('title','403')
<link rel="stylesheet" href="<?php echo asset('css/403.css')?>" type="text/css">
<div class="scene">
    <div class="overlay"></div>
    <div class="overlay"></div>
    <div class="overlay"></div>
    <div class="overlay"></div>
    <span class="bg-403">403</span>
    <div class="text">
        <span class="hero-text"></span>
        <span class="msg">Acceso <span> no </span> Autorizado</span>
        <span class="support">
      <span>¿Inesperado?</span>
      <a href="#">Envie un correo electronico a: flightsystems@help.com</a>
    </span>
    </div>
    <div class="lock"></div>
</div>
