@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row col-md-10">
            <div class="col-md-12 col-md-offset-2 ">
                <div class="panel panel-default">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mt-sm-0">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('roles')}}"><i class="fas fa-user-tie"></i> Rol</a></li>
                            <li class="breadcrumb-item"><a href="#"></a>{{$role->id}}</li>
                        </ol>
                    </nav>
                    <div class="panel-heading">
                        <h3><b><i class="fas fa-edit"></i> Edicion del rol</b></h3>
                    </div>
                    <div class="panel-body">
                        {!! Form::model($role, ['route' => ['roles.update', $role->id],
                        'method' => 'PUT']) !!}

                        @include('roles.partials.form')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
