@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row ">
            <div class="col-md-12 col-md-offset-2 ">
                <div class="panel panel-default">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mt-sm-0">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fa fa-home"></i> Inicio</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('roles')}}"><i class="fa fa-user-tie"></i> Roles</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fa fa-eye"></i> Mostrar rol</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('#')}}">{{$role->id}}</a></li>
                            <li class="breadcrumb-item"><a href="#"></a></li>
                        </ol>
                    </nav>
                        <div class="panel-heading">
                            <h3><b><i class="fa fa-info-circle"></i> Informacion sobre el rol</b></h3>
                        </div>

                        <div class="panel-body">
                            <p><strong>Nombre</strong>     {{ $role->name }}</p>
                            <p><strong>Slug</strong>       {{ $role->slug }}</p>
                            <p><strong>Descripción</strong>  {{ $role->description }}</p>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
