@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-2 ">
                <div class="panel panel-default">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mt-sm-0">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fa fa-home"></i> Inicio</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('permissions')}}"><i class="fas fa-user-tie"></i> Roles</a></li>
                            <li class="breadcrumb-item"><a href="#"></a></li>
                        </ol>
                    </nav>
                    <div class="panel-heading">
                        <h3><b><i class="fas fa-user-tie"></i> Gestion de roles del sistema</b></h3>
                        @can('roles.create')
                            <a href="{{ route('roles.create') }}"
                               class="btn btn-sm btn-primary float-left">
                                <i class="fas fa-plus"></i> Crear nuevo rol
                            </a>
                        @endcan

                    </div>

                    <div class="panel-body">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th width="10px">ID</th>
                                <th>Nombre</th>
                                <th colspan="3">Opciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->id }}</td>
                                    <td>{{ $role->name }}</td>
                                    @can('roles.show')
                                        <td>
                                            <a href="{{ route('roles.show', $role->id) }}"
                                               class="btn btn-sm btn-default btn-info">
                                                <i class="fas fa-eye"></i> Ver informacion del rol
                                            </a>
                                        </td>
                                    @endcan
                                    @can('roles.edit')
                                        <td>
                                            <a href="{{ route('roles.edit', $role->id) }}"
                                               class="btn btn-sm btn-default btn-success">
                                                <i class="fas fa-edit"></i> Editar rol y manejo de permisos
                                            </a>
                                        </td>
                                    @endcan
                                    @can('roles.destroy')
                                        <td>
                                            {!! Form::open(['route' => ['roles.destroy', $role->id],
                                            'method' => 'DELETE']) !!}
                                            <button class="btn btn-sm btn-danger">
                                                <i class="fas fa-trash-alt"></i> Eliminar
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    @endcan
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $roles->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
