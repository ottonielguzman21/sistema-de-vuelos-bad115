@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row col-md-8">
            <div class="col-md-12 col-md-offset-2 ">
                <div class="panel panel-default">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mt-sm-0">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Inicio</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('roles')}}">Roles</a></li>
                            <li class="breadcrumb-item"><a href="#"></a></li>
                        </ol>
                    </nav>
                    <div class="panel-heading">
                        <h3><b>Nuevo rol</b></h3>
                    </div>

                    <div class="panel-body">
                        {{ Form::open(['route' => 'roles.store']) }}

                        @include('roles.partials.form')

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
