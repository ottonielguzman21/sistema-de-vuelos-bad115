@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row col-md-10">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">
                    <nav aria-label="breadcrumb">
                       <ol class="breadcrumb mt-sm-0">
                          <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                          <li class="breadcrumb-item active"><a href="{{route('city_countrys.index')}}"><i class="fas fa-sitemap"></i> Asignacion Ciudad-País</a></li>
                          <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-edit"></i> Ver Asignacion Ciudad al País: {{ $city_countrys->country->nombre_pais }}</a></li>
                          <li class="breadcrumb-item"><a href="#"></a></li>
                       </ol>
                    </nav>
                    <div class="row">
                       <div class="col-lg-12 margin-tb">
                          <div class="pull-left">
                             <h2><i class="fas fa-edit"></i> Ver Asignacion Ciudad-País: {{ $city_countrys->country->nombre_pais }}</h2>
                          </div>
                       </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>País:</strong>
                                {{ $city_countrys->country->id }}. {{ $city_countrys->country->nombre_pais }}, {{ $city_countrys->country->cod_area }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Aeroplano:</strong>
                                {{ $city_countrys->city->id }}. {{ $city_countrys->city->nombre_ciudad }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
