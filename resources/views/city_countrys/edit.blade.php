@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row col-md-10">
      <div class="col-md-12 col-md-offset-2 ">
         <div class="panel panel-default">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mt-sm-0">
                  <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                  <li class="breadcrumb-item active"><a href="{{route('city_countrys.index')}}"><i class="fas fa-sitemap"></i> Flotas</a></li>
                  <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-edit"></i> Agregar Ciudad a País</a></li>
                  <li class="breadcrumb-item"><a href="#"></a></li>
               </ol>
            </nav>
            <div class="row">
               <div class="col-lg-12 margin-tb">
                  <div class="pull-left">
                     <h2><i class="fas fa-edit"></i> Editar Asignacion Ciudad a País.</h2>
                  </div>
               </div>
            </div>
            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
               <p>{{ $message }}</p>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
               <strong>Whoops!</strong> Hubo algunos problemas con su entrada!<br><br>
               <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
               </ul>
            </div>
            @endif
            <form action="{{ route('city_countrys.update',$city_countrys->id) }}" method="POST">
               @csrf
               @method('PUT')
               <div class="row">
      <div class="col-sm-10">
         <div class="form-group {{ $errors->has('country_id') ? 'has-error' : "" }}">
            <strong>País:</strong>
         <i>
            <select name="country_id" class="form-control">
               <option selected value="{{ $city_countrys->country->id }}">{{ $city_countrys->country->id }}. {{ $city_countrys->country->nombre_pais }}, {{ $city_countrys->country->cod_area }}</option>
               @foreach($countrys as $country)
               <option value="{{$country->id}}">{{$country->id}}. {{$country->nombre_pais }} {{$country->cod_area }}</option>
               @endforeach
            </select>
         </i>
         <div class="help-block"> 
            <strong>{{ $errors->first('country_id', 'Seleccione uno') }}</strong>
         </div>
         </div>
      </div>
      <div class="col-sm-10">
         <div class="form-group {{ $errors->has('city_id') ? 'has-error' : "" }}">
            <strong>Aeroplano:</strong>
         <i>
            <select name="city_id" class="form-control">
               <option selected value="{{ $city_countrys->city->id }}">{{ $city_countrys->city->id }}. {{$city_countrys->city->nombre_ciudad }}</option>
                  @foreach($cities as $city)
                  <option value="{{$city->id}}">{{$city->id}}. {{$city->nombre_ciudad }}</option>
                  @endforeach
               </select>
         </i>
         <div class="help-block"> 
            <strong>{{ $errors->first('city_id', 'Seleccione uno') }}</strong>
         </div>
         </div>
      </div>
         <div class="col-xs-12 col-sm-12 col-md-12 text-center">
         <button type="submit" class="btn btn-sm btn-primary"><i class="fas fa-save"></i> Actualizar</button>
         <a class="btn btn-sm btn-danger" href="{{ route('city_countrys.index') }}">Cancelar</a>
         </div>
      </div>
      </form>
   </div>
</div>
</div>
</div>
@endsection