@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row">
      <div class="col-md-8">
         <div class="panel panel-default">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mt-sm-0">
                  <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                  <li class="breadcrumb-item active"><a href="{{route('city_countrys.index')}}"><i class="fas fa-sitemap"></i> Flotas</a></li>
                  <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-edit"></i> Agregar Ciudad a País</a></li>
                  <li class="breadcrumb-item"><a href="#"></a></li>
               </ol>
            </nav>
            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
               <p>{{ $message }}</p>
            </div>
            @endif
            <div class="panel-heading">
               <h3>
                  <b>Agregar Ciudad a País</b>
               </h3>
            </div>
            <div class="panel-body">
               {{ Form::open(['route' => 'city_countrys.store']) }}
               @include('city_countrys.partials.form')
               {{ Form::close() }}
            </div>
         </div>
      </div>
   </div>
</div>
@endsection