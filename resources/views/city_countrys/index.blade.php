@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row">
      <div class="col-md-12 col-md-offset-2">
         <div class="panel panel-default">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mt-sm-0">
                  <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"> </i>Inicio</a></li>
                  <li class="breadcrumb-item active"><a href="{{url('city_countrys')}}"><i class="fas fa-sitemap"></i> Flotas</a></li>
                  <li class="breadcrumb-item"><a href="#"></a></li>
               </ol>
            </nav>
            <div class="panel-heading">
               <h3><b><i class="fas fa-sitemap"></i> Gestión de Asignacion Ciudad-País</b></h3>
            </div>
            {!! Form::open(['route'=>'city_countrys.index','method'=>'GET', 'class'=>'form-inline float-right']) !!}
            <div class="form-group">
               {!! Form::text('city_id', null, ['class' => 'form-control','placeholder'=>'ID de Ciudad']) !!}
            </div>
            <button type="submit" class="btn btn-info "><i class="fas fa-search"></i> Buscar</button>
            {!! Form::close() !!}
            <div class="pull-right">
               <a class="btn btn-success" href="{{ route('city_countrys.create') }}"><i class="fas fa-plus"></i> Gestión de Asignacion Ciudad-País</a>
            </div>
            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
               <p>{{ $message }}</p>
            </div>
            @endif
            <br>
            <table class="table table-bordered table-striped">
               <tr>
                  <th>No</th>
                  <th>Nombre Pais</th>
                  <th>Codigo de área</th>
                  <th>Estado del País</th> 
                  <th>Nombre de la Ciudad</th>
                  <th width="280px">Accion</th>
               </tr>
               @foreach ($city_countrys as $city_country)
               <tr>
                  <td>{{ ++$i }}</td>
                  <td>{{ $city_country->country->nombre_pais }}</td>
                  <td>{{ $city_country->country->cod_area }}</td>
                  @if($city_country->country->estado==1)
                  <td>Activo</td>
                  @endif
                  @if($city_country->country->estado==0)
                  <td>Inactivo</td>
                  @endif
                  <td>{{ $city_country->city->nombre_ciudad }}</td>                  
                  <td width="350px">
                     <form action="{{ route('city_countrys.destroy',$city_country->id) }}" method="POST">
                        <a class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{ route('city_countrys.show',$city_country->id) }}"><i class="fas fa-eye"></i>Mostrar</a>
                        <a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Editar" href="{{ route('city_countrys.edit',$city_country->id) }}"><i class="fas fa-edit"></i>Editar</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i>Eliminar</button>
                     </form>
                  </td>
               </tr>
               @endforeach
            </table>
            {!! $city_countrys->links() !!}
         </div>
      </div>
   </div>
</div>
@endsection