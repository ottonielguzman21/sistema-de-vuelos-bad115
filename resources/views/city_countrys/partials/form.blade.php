   <div class="row">
         <div class="col-sm-2">
            {!! form::label('País') !!}
         </div>
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('country_id') ? 'has-error' : "" }}">
            <i>
               <select name="country_id" class="form-control">
                  <option disabled selected>Seleccione País</option>
                  @foreach($countries as $country)
                  <option value="{{$country->id}}">{{$country->id}}. {{$country->nombre_pais }} {{$country->cod_area }}</option>
                  @endforeach
               </select>
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('country_id', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
         <div class="col-sm-2">
            {!! form::label('Ciudad') !!}
         </div>
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('city_id') ? 'has-error' : "" }}">
            <i>
               <select name="city_id" class="form-control">
                  <option disabled selected>Seleccione Ciudad</option>
                  @foreach($cities as $citie)
                  <option value="{{$citie->id}}">{{$citie->id}}. {{$citie->nombre_ciudad }}</option>
                  @endforeach
               </select>
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('city_id', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
   </div>
   <div class="form-group text-center">
   {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
   <a class="btn btn-sm btn-danger" href="{{ route('city_countrys.index') }}">Cancelar</a>
</div>