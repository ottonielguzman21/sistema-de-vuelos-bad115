@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mt-sm-0">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('permissions')}}"><i class="fas fa-users"></i> Usuarios</a></li>
                            <li class="breadcrumb-item"><a href="#"></a></li>
                        </ol>
                    </nav>
                    {!! Form::open(['route'=>'users.index','method'=>'GET', 'class'=>'form-inline float-right']) !!}
                    <div class="form-group">
                        {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Nombre']) !!}
                    </div>
                    <button type="submit" class="btn btn-info "><i class="fas fa-search"></i> Buscar</button>
                    {!! Form::close() !!}
                    <div class="panel-heading">
                        <h3><b><i class="fa fa-users"></i> Gestion de usuarios del sistema</b></h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th width="10px">ID</th>
                                <th>Nombre</th>
                                <th colspan="3">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>

                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    @can('users.show')
                                        <td>
                                            <a href="{{ route('users.show', $user->id) }}"
                                               class="btn btn-sm btn-default btn-info">
                                                <i class="fas fa-eye"></i>
                                                Ver informacion del usuario
                                            </a>
                                        </td>
                                    @endcan
                                    @can('users.edit')
                                        <td>
                                            <a href="{{ route('users.edit', $user->id) }}"
                                               class="btn btn-sm btn-default btn-success">
                                                <i class="fas fa-edit"></i>
                                                Modificar Roles y Permisos
                                            </a>
                                        </td>
                                    @endcan
                                    @can('users.destroy')
                                        <td width="10px">
                                            {!! Form::open(['route' => ['users.destroy', $user->id],
                                            'method' => 'DELETE']) !!}
                                            <button class="btn btn-sm btn-danger">
                                                <i class="fas fa-trash"></i>
                                                Eliminar
                                            </button>
                                            {!! Form::close() !!}
                                        </td>
                                    @endcan
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $users->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
