@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mt-sm-0">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('users')}}"><i class="fas fa-users"></i> Usuarios</a></li>
                            <li class="breadcrumb-item"><a href="#"></a>{{$user->id}}</li>
                        </ol>
                    </nav>
                    <div class="panel-heading">
                       <h3> <b><i class="fas fa-info-circle"></i> Informacion del usuario</b></h3>
                    </div>
                        <p><strong>Nombre:</strong>     {{ $user->name }}</p>
                        <p><strong>Email:</strong>      {{ $user->email }}</p>
                        <p><strong>Registrado:</strong>  {{ $user->created_at }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
