@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="<?php echo asset('css/style.css')?>" type="text/css">
    <div class="container modal-dialog-centered">
        <div class="col">
            <div class="col-md-10 panel float-sm-right">
                <div class="abs-center">
                    <div class="card">
                        <div class="panel float-sm-right">
                            <div class="card-header ">
                                Gestion de productos
                                @can('products.create')
                                    <a href="{{route('products.create')}}"
                                       class="btn btn-sm btn-primary col-sm-2 float-sm-right">
                                        Crear nuevo
                                    </a>
                                @endcan
                            </div>
                            <div class="card-body">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th width="10px">ID</th>
                                        <th>Nombre</th>
                                        <th>Descripcion</th>
                                        <th>&nbsp</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $product)
                                        <tr>
                                            <td >{{ $product->id }}</td>
                                            <td>{{ $product->name }}</td>
                                            <td>{{$product->description}}</td>
                                            @can('products.show')
                                                <td width="10px">
                                                    <a href="{{ route('products.show', $product->id) }}"
                                                       class="btn btn-sm btn-info float-sm-right">
                                                        Ver registro
                                                    </a>
                                                </td>
                                            @endcan
                                            @can('products.edit')
                                                <td width="10px">
                                                    <a href="{{ route('products.edit', $product->id) }}"
                                                       class="btn btn-sm btn-success">
                                                        Editar registro
                                                    </a>
                                                </td>
                                            @endcan
                                            @can('products.destroy')
                                                <td width="10px">
                                                    {!! Form::open(['route' => ['products.destroy', $product->id],
                                                    'method' => 'DELETE']) !!}
                                                    <button class="btn btn-sm btn-danger">
                                                        Eliminar registro
                                                    </button>
                                                    {!! Form::close() !!}
                                                </td>
                                            @endcan
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $products->render() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
