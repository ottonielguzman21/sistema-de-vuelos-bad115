@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Destinos</h3> </div>
                    <div class="panel-body">
                        {{ Form::open(['route' => 'destino.store'])}}

                        <div class="form-group">
                            {{ Form::label('airport_origin_id', 'Origen') }}
                            <select id="airport_origin_id" name="airport_origin_id" class="form-group ">
                                <option value="">Seleccione Origen</option>
                                @foreach ($aeropuertos as $aeropuerto)
                                    <option value="{{ $aeropuerto->codigo }}">{{ $aeropuerto->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            {{ Form::label('airport_destination_id', 'Destino') }}
                            <select id="airport_destination_id" name="airport_destination_id" class="form-group ">
                                <option value="">Seleccione Destino</option>
                                @foreach ($aeropuertos as $aeropuerto)
                                    <option value="{{ $aeropuerto->codigo }}">{{ $aeropuerto->nombre }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            {{ Form::label('fecha_ida', 'Fecha hida') }}
                            {{ Form::text('fecha_ida', null, ['class' => 'form-group']) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('fecha_retorno', 'Fecha retorno') }}
                            {{ Form::text('fecha_retorno', null, ['class' => 'form-group']) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('seat_id', 'Asiento') }}
                            <select id="seat_id" name="seat_id" class="form-group ">
                                <option value="">Asiento Disponible</option>
                                @foreach ($asientos as $asiento)
                                    <option value="{{ $asiento->id }}">{{ $asiento->asiento }}</option>
                                @endforeach
                            </select>

                        </div>


                        <div class="form-group">
                            {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
                        </div>

                        {{ Form::close() }}
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
