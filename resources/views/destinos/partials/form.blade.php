<div class="form-group">
    {{ Form::label('nombre_clase', 'Nombre de la clase') }}
    {{ Form::text('nombre_clase', null, ['class' => 'form-control', 'name'=>'nombre_clase','id' => 'nombre_clase']) }}

</div>
<div class="form-group">
    {{ Form::label('cantidad_asientos', 'Cantidad Asientos') }}
    {{ Form::text('cantidad_asientos', null, ['class' => 'form-control']) }}
</div>
<div class="form-group">
    {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
</div>
