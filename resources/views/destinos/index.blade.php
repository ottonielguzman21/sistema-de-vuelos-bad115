@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="<?php echo asset('css/style.css')?>" type="text/css">
    <div class="container modal-dialog-centered">
        <div class="col">
            <div class="col-md-10 panel float-sm-right">
                <div class="abs-center">
                    <div class="card">
                        <div class="panel float-sm-right">
                            <div class="card-header ">
                                Gestion de destinos
                                @can('destino.create')
                                    <a href="{{route('destino.create')}}"
                                       class="btn btn-sm btn-primary col-sm-3 float-sm-right">
                                        Crear nuevo
                                    </a>
                                @endcan
                            </div>
                            <div class="card-body">
                                <table class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th width="10px">ID</th>
                                        <th>Origen</th>
                                        <th >Destino</th>
                                        <th >Fecha Ida</th>
                                        <th >Fecha Regreso</th>
                                        <th>&nbsp</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($destinos as $destino)
                                        <tr>
                                            <td >{{ $destino->id }}</td>
                                            <td>{{$destino->origen }}</td>
                                            <td>{{$destino->destino}}</td>
                                            <td>{{$destino->fecha_ida}}</td>
                                            <td>{{$destino->fecha_regreso}}</td>
                                            @can('destino.show')
                                                <td width="10px">
                                                    <a href="{{ route('destino.show', $destino->id) }}"
                                                       class="btn btn-sm btn-info float-sm-right">
                                                        Ver registro
                                                    </a>
                                                </td>
                                            @endcan
                                            @can('destino.edit')
                                                <td width="10px">
                                                    <a href="{{ route('destino.edit', $destino->id) }}"
                                                       class="btn btn-sm btn-success">
                                                        Editar registro
                                                    </a>
                                                </td>
                                            @endcan
                                            @can('destino.destroy')
                                                <td width="10px">
                                                    <button class="btn btn-sm btn-danger">
                                                        Eliminar registro
                                                    </button>

                                                </td>
                                            @endcan
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


