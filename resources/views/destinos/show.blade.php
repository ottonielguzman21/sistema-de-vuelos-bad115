@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Detalle Clase
                        <div class="pull-right">

                            <a class="btn btn-primary btn-sm" href="{{ route('clase.index') }}"> Regresar</a>

                        </div>
                    </div>

                    <div class="panel-body">
                        <p><strong>Nombre Clase</strong>  {{ $clase->nombre_clase }}</p>
                        <p><strong>Cantidad Asientos</strong>  {{ $clase->cantidad_asientos }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
