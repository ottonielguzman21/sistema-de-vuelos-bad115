@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Editar Clase
                        <div class="pull-right">

                            <a class="btn btn-primary btn-sm" href="{{ route('clase.index') }}"> Regresar</a>

                        </div>
                    </div>

                    <div class="panel-body">
                        {!! Form::model($clase, ['route' => ['clase.update', $clase->id],
                        'method' => 'PUT']) !!}

                        @include('clase.partials.form')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
