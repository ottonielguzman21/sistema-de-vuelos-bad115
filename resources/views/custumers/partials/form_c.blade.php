<div class="row">
   <div class="col-sm-2">
      {!! form::label('Nombre Usuario') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('user_id') ? 'has-error' : "" }}">
      <i>
         <select name="user_id" class="form-control">
            <option disabled selected>Seleccione Usuario</option>
            @foreach($users as $user)
            <option value="{{$user->id}}">{{$user->id}}. {{$user->lname }} {{$user->name }}</option>
            @endforeach
         </select>
      </i>
      <div class="help-block"> 
         <strong>{{ $errors->first('user_id', 'Seleccione uno') }}</strong>
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('Tipo Documento') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('tipo_documento') ? 'has-error' : "" }}">
      <i>
         <select name="tipo_documento" class="form-control">
            <option disabled selected>Seleccione Tipo Documento</option>
            <option value="Juridico">Juridico</option>
            <option value="Natural">Natural</option>
         </select>
      </i>
      <div class="help-block"> 
         <strong>{{ $errors->first('tipo_documento', 'Seleccione uno') }}</strong>
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('Tipo Cliente') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('tipo_cliente') ? 'has-error' : "" }}">
      <i>
         <select readonly name="tipo_cliente" class="form-control">
            <option selected value="2">Empresa</option>
         </select>
      </i>
      <div class="help-block"> 
         <strong>{{ $errors->first('tipo_cliente', 'Seleccione uno') }}</strong>
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('numero_viajero_frecuente', 'N° Viajero Frecuente') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('numero_viajero_frecuente') ? 'has-error' : "" }}">
      <i>{{ Form::text('numero_viajero_frecuente',NULL, ['class'=>'form-control', 'id'=>'numero_viajero_frecuente', 'placeholder'=>'N° Viajero']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('numero_viajero_frecuente', 'Ingrese: A-Z a-z 0-9') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('direccion', 'Dirección') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('direccion') ? 'has-error' : "" }}">
      <i>{{ Form::text('direccion',NULL, ['class'=>'form-control', 'id'=>'direccion', 'placeholder'=>'Dirección']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('direccion', 'Ingrese: A-Z a-z 0-9') }}</strong> 
      </div>
   </div>
</div>
</div>
   <div class="row">
   <div class="col-sm-2">
      {!! form::label('telefono_fijo', 'Telefono Fijo') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('telefono_fijo') ? 'has-error' : "" }}">
      <i>{{ Form::text('telefono_fijo',NULL, ['class'=>'form-control', 'id'=>'telefono_fijo', 'placeholder'=>'20000000','pattern'=>'^2\d{7}$' ]) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('telefono_fijo', 'Ingrese: 0-9 Formato: 20000000') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('nombre_empresa', 'Nombre Empresa') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('nombre_empresa') ? 'has-error' : "" }}">
      <i>{{ Form::text('nombre_empresa',NULL, ['class'=>'form-control', 'id'=>'nombre_empresa', 'placeholder'=>'Empresa 123']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('nombre_empresa', 'Ingrese: A-Z a-z 0-9') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('nombre_contacto', 'Nombre Contacto') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('nombre_contacto') ? 'has-error' : "" }}">
      <i>{{ Form::text('nombre_contacto',NULL, ['class'=>'form-control', 'id'=>'nombre_contacto', 'placeholder'=>'Contacto']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('nombre_contacto', 'Ingrese: A-Z a-z') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('NIT', 'NIT') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('NIT') ? 'has-error' : "" }}">
      <i>{{ Form::text('NIT',NULL, ['class'=>'form-control', 'id'=>'NIT', 'placeholder'=>'10101010101010']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('NIT', 'Formato:10101010101010') }}</strong> 
      </div>
   </div>
</div>
</div>

<div class="row">
   <div class="col-sm-2">
      {!! form::label('NIC', 'NIC') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('NIC') ? 'has-error' : "" }}">
      <i>{{ Form::text('NIC',NULL, ['class'=>'form-control', 'id'=>'NIC', 'placeholder'=>'1234567']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('NIC', 'Formato:1234567') }}</strong> 
      </div>
   </div>
</div>
</div>

<div class="form-group text-center">
   {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
   <a class="btn btn-sm btn-danger" href="{{ route('custumers.index') }}">Cancelar</a>
</div>