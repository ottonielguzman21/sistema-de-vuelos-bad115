   <div class="row">
         <div class="col-sm-2">
            {!! form::label('Nombre Usuario') !!}
         </div>
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('user_id') ? 'has-error' : "" }}">
            <i>
               <select name="user_id" class="form-control">
                  <option disabled selected>Seleccione Usuario</option>
                  @foreach($users as $user)
                  <option value="{{$user->id}}">{{$user->id}}. {{$user->lname }} {{$user->name }}</option>
                  @endforeach
               </select>
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('user_id', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
         <div class="col-sm-2">
            {!! form::label('Tipo Documento') !!}
         </div>
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('tipo_documento') ? 'has-error' : "" }}">
            <i>
               <select name="tipo_documento" class="form-control">
                  <option disabled selected>Seleccione Tipo Documento</option>
                  <option value="Juridico">Juridico</option>
                  <option value="Natural">Natural</option>
               </select>
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('tipo_documento', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
         <div class="col-sm-2">
            {!! form::label('Tipo Cliente') !!}
         </div>
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('tipo_cliente') ? 'has-error' : "" }}">
            <i>
               <select readonly name="tipo_cliente" class="form-control">
                  <option selected value="1">Persona Natural</option>
               </select>
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('tipo_cliente', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
   <div class="col-sm-2">
      {!! form::label('numero_viajero_frecuente', 'N° Viajero Frecuente') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('numero_viajero_frecuente') ? 'has-error' : "" }}">
      <i>{{ Form::text('numero_viajero_frecuente',NULL, ['class'=>'form-control', 'id'=>'numero_viajero_frecuente', 'placeholder'=>'1234567']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('numero_viajero_frecuente', 'Ingrese: A-Z a-z 0-9') }}</strong> 
      </div>
   </div>
</div>
</div>
   <div class="row">
   <div class="col-sm-2">
      {!! form::label('direccion', 'Dirección') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('direccion') ? 'has-error' : "" }}">
      <i>{{ Form::text('direccion',NULL, ['class'=>'form-control', 'id'=>'direccion', 'placeholder'=>'Dirección']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('direccion', 'Ingrese: A-Z a-z 0-9') }}</strong> 
      </div>
   </div>
</div>
</div>
   <div class="row">
   <div class="col-sm-2">
      {!! form::label('telefono_fijo', 'Telefono Fijo') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('telefono_fijo') ? 'has-error' : "" }}">
      <i>{{ Form::text('telefono_fijo',NULL, ['class'=>'form-control', 'id'=>'telefono_fijo', 'placeholder'=>'20000000','pattern'=>'^2\d{7}$' ]) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('telefono_fijo', 'Ingrese: 0-9 Formato: 20000000') }}</strong> 
      </div>
   </div>
</div>
</div>
   <div class="row">
   <div class="col-sm-2">
      {!! form::label('telefono_movil', 'Telefono Movil') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('telefono_movil') ? 'has-error' : "" }}">
      <i>{{ Form::text('telefono_movil',NULL, ['class'=>'form-control', 'id'=>'telefono_movil', 'placeholder'=>'70000000','pattern'=>'^7\d{7}$||^6\d{7}$']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('telefono_movil', 'Ingrese: 0-9 Formato: 70000000') }}</strong> 
      </div>
   </div>
</div>
</div>
   <div class="row">
   <div class="col-sm-2">
      {!! form::label('numero_documento', 'N° de Documento') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('numero_documento') ? 'has-error' : "" }}">
      <i>{{ Form::number('numero_documento',NULL, ['class'=>'form-control', 'id'=>'numero_documento', 'placeholder'=>'1234567']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('numero_documento', 'Ingrese: 0-9') }}</strong> 
      </div>
   </div>
</div>
</div>
   <div class="row">
         <div class="col-sm-2">
            {!! form::label('Género') !!}
         </div>
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('genero') ? 'has-error' : "" }}">
            <i>
               <select name="genero" class="form-control">
                  <option disabled selected>Seleccione Género</option>
                  <option value="Masculino">Masculino</option>
                  <option value="Femenino">Femenino</option>
                  <option value="Otro">Otro</option>
               </select>
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('genero', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
         <div class="col-sm-2">
            {!! form::label('Estado Civil') !!}
         </div>
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('estado_civil') ? 'has-error' : "" }}">
            <i>
               <select name="estado_civil" class="form-control">
                  <option disabled selected>Seleccione Estado Civil</option>
                  <option value="Comprometido/a">Comprometido/a</option>
                  <option value="Soltero/a">Soltero/a</option>
                  <option value="Casado/a">Casado/a</option>
                  <option value="Separado/a">Separado/a</option>
                  <option value="Divorciado/a">Divorciado/a</option>
                  <option value="Viudo/a">Viudo/a</option>
               </select>
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('estado_civil', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
   </div>


<div class="form-group text-center">
   {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
   <a class="btn btn-sm btn-danger" href="{{ route('custumers.index') }}">Cancelar</a>
</div>