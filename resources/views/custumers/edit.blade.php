@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row col-md-10">
      <div class="col-md-12 col-md-offset-2 ">
         <div class="panel panel-default">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mt-sm-0">
                  <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                  <li class="breadcrumb-item active"><a href="{{route('custumers.index')}}"><i class="fas fa-sitemap"></i> Clientes</a></li>
                  @if($custumer->tipo_cliente==1)
                  <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-edit"></i> Editar Cliente {{$custumer->User->lname}}</a></li>
                  @endif
                  @if($custumer->tipo_cliente==2)
                  <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-edit"></i> Editar Cliente: Empresa {{$custumer->compania->nombre_empresa}}</a></li>
                  @endif
                  <li class="breadcrumb-item"><a href="#"></a></li>
               </ol>
            </nav>
            <div class="row">
               <div class="col-lg-12 margin-tb">
                  <div class="pull-left">
                     @if($custumer->tipo_cliente==1)
                     <h2><i class="fas fa-edit"></i> Editar Cliente {{$custumer->User->lname}}</h2>
                     @endif
                     @if($custumer->tipo_cliente==2)
                     <h2><i class="fas fa-edit"></i> Editar Cliente: Empresa {{$custumer->compania->nombre_empresa}}</h2>
                     @endif
                  </div>
               </div>
            </div>
            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
               <p>{{ $message }}</p>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
               <strong>Whoops!</strong> Hubo algunos problemas con su entrada!<br><br>
               <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
               </ul>
            </div>
            @endif
            <form action="{{ route('custumers.update',$custumer->id) }}" method="POST">
               @csrf
               @method('PUT')
               <div class="row">
                  <div class="col-sm-10">
                     <div class="form-group {{ $errors->has('tipo_documento') ? 'has-error' : "" }}">
                     <strong>Tipo Documento:</strong>
                     <i>
                     <select name="tipo_documento" class="form-control">
                           <option selected value="{{ $custumer->tipo_documento }}">{{ $custumer->tipo_documento }}</option>
                           <option value="Juridico">Juridico</option>
                           <option value="Natural">Natural</option>
                     </select>
                     </i>
                     <div class="help-block"> 
                        <strong>{{ $errors->first('tipo_documento', 'Seleccione uno') }}</strong>
                     </div>
                  </div>
               </div>
               <div class="col-sm-10">
                  <div class="form-group {{ $errors->has('numero_viajero_frecuente') ? 'has-error' : "" }}">
                     <strong>N° Viajero Frecuente:</strong>
                        <i >
                        <input type="number" name="numero_viajero_frecuente" value="{{ $custumer->numero_viajero_frecuente }}" class="form-control" placeholder="123456" min="0" >
                        </i>
                     <div class="help-block"> 
                        <strong>{{ $errors->first('numero_viajero_frecuente', 'Ingrese 1-9') }}</strong>
                     </div>
                  </div>
               </div>
               <div class="col-sm-10">
                  <div class="form-group {{ $errors->has('direccion') ? 'has-error' : "" }}">
                     <strong>Direccion:</strong>
                        <i >
                        <input type="text" name="direccion" value="{{ $custumer->direccion }}" class="form-control" placeholder="direccion">
                        </i>
                     <div class="help-block"> 
                        <strong>{{ $errors->first('direccion', 'Ingrese A-Z a-z 1-9') }}</strong>
                     </div>
                  </div>
               </div>
            <div class="col-sm-10">
               <div class="form-group {{ $errors->has('telefono_fijo') ? 'has-error' : "" }}">
                  <strong>Telefono Fijo:</strong>
                     <i >
                     <input type="text" name="telefono_fijo" value="{{ $custumer->telefono_fijo }}" class="form-control" placeholder="20000000" pattern="^2\d{7}$">
                     </i>
                  <div class="help-block"> 
                     <strong>{{ $errors->first('telefono_fijo', 'Ingrese 1-9') }}</strong>
                  </div>
               </div>
            </div>
            <div class="col-sm-10">
               <div class="form-group {{ $errors->has('estado') ? 'has-error' : "" }}">
                  <strong>Estado:</strong>
                  <i>
                     <select name="estado" class="form-control">
                        @if( $custumer->estado==1)
                           <option selected value="{{ $custumer->estado }}">{{ $custumer->estado }}. Activo</option>
                        @endif
                        @if( $custumer->estado==0)
                           <option selected value="{{ $custumer->estado }}">{{ $custumer->estado }}. Desactivo</option>
                        @endif
                           <option value="1">Activo</option>
                           <option value="0">Desactivo</option>
                     </select>
                  </i>
                  <div class="help-block"> 
                     <strong>{{ $errors->first('estado', 'Seleccione uno') }}</strong>
                  </div>
               </div>
            </div>
         @if($custumer->tipo_cliente==1)
         <div class="col-sm-10">
            <div class="form-group {{ $errors->has('telefono_movil') ? 'has-error' : "" }}">
               <strong>Telefono Fijo:</strong>
            <i >
               <input type="text" name="telefono_movil" value="{{ $custumer->persona->telefono_movil }}" class="form-control" placeholder="70000000" pattern="^7\d{7}$||^6\d{7}$">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('telefono_movil', 'Ingrese 1-9 Formato: 70000000') }}</strong>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
         <div class="form-group {{ $errors->has('numero_documento') ? 'has-error' : "" }}">
            <strong>N° De Docuemento:</strong>
            <i >
               <input type="number" name="numero_documento" value="{{ $custumer->persona->numero_documento }}" class="form-control" placeholder="123456" min="0" >
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('numero_documento', 'Ingrese 1-9') }}</strong>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
         <div class="form-group {{ $errors->has('genero') ? 'has-error' : "" }}">
            <strong>Genero:</strong>
         <i>
            <select name="genero" class="form-control">
               <option selected value="{{ $custumer->persona->genero }}">{{ $custumer->persona->genero }}</option>
               <option value="Masculino">Masculino</option>
               <option value="Femenino">Femenino</option>
               <option value="Otro">Otro</option>
            </select>
         </i>
         <div class="help-block"> 
            <strong>{{ $errors->first('genero', 'Seleccione uno') }}</strong>
         </div>
         </div>
      </div>
      <div class="col-sm-10">
            <div class="form-group {{ $errors->has('estado_civil') ? 'has-error' : "" }}">
            <strong>Estado Civil:</strong>
            <i>
         <select name="estado_civil" class="form-control">
               <option selected value="{{ $custumer->persona->estado_civil }}">{{ $custumer->persona->estado_civil }}</option>
               <option value="Comprometido/a">Comprometido/a</option>
               <option value="Soltero/a">Soltero/a</option>
               <option value="Casado/a">Casado/a</option>
               <option value="Separado/a">Separado/a</option>
               <option value="Divorciado/a">Divorciado/a</option>
               <option value="Viudo/a">Viudo/a</option>
         </select>
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('estado_civil', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
       @endif

       @if($custumer->tipo_cliente==2)
          <div class="col-sm-10">
            <div class="form-group {{ $errors->has('nombre_empresa') ? 'has-error' : "" }}">
            <strong>Nombre Empresa:</strong>
            <i >
            <input type="text" name="nombre_empresa" value="{{ $custumer->compania->nombre_empresa }}" class="form-control">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('nombre_empresa', 'Ingrese A-Z a-z') }}</strong>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
            <div class="form-group {{ $errors->has('nombre_contacto') ? 'has-error' : "" }}">
            <strong>Nombre De Contacto:</strong>
            <i >
            <input type="text" name="nombre_contacto" value="{{ $custumer->compania->nombre_contacto }}" class="form-control">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('nombre_contacto', 'Ingrese A-Z a-z') }}</strong>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
            <div class="form-group {{ $errors->has('NIT') ? 'has-error' : "" }}">
            <strong>NIT:</strong>
            <i >
            <input type="text" name="NIT" value="{{ $custumer->compania->NIT }}" class="form-control">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('NIT', 'Ingrese 1-9 Formato:10101010101010') }}</strong>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
            <div class="form-group {{ $errors->has('NIC') ? 'has-error' : "" }}">
            <strong>NIC:</strong>
            <i >
            <input type="number" name="NIC" value="{{ $custumer->compania->NIC }}" class="form-control">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('NIC', '1-9 Formato:1010101') }}</strong>
            </div>
         </div>
      </div>
       @endif
         <div class="col-xs-12 col-sm-12 col-md-12 text-center">
         <button type="submit" class="btn btn-sm btn-primary"><i class="fas fa-save"></i> Actualizar</button>
         <a class="btn btn-sm btn-danger" href="{{ route('custumers.index') }}">Cancelar</a>
         </div>
      </div>
      </form>
   </div>
</div>
</div>
</div>
@endsection