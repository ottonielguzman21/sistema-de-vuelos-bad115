@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row">
      <div class="col-md-8">
         <div class="panel panel-default">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mt-sm-0">
                  <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                  <li class="breadcrumb-item active"><a href="{{route('custumers.index')}}"><i class="fas fa-sitemap"></i> Clientes</a></li>
                  <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-edit"></i> Ingresar Cliente tipo Empresa</a></li>
                  <li class="breadcrumb-item"><a href="#"></a></li>
               </ol>
            </nav>
            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
               <p>{{ $message }}</p>
            </div>
            @endif
            <div class="panel-heading">
               <h3>
                  <b>Ingresar Nuevo Cliente (Empresa)</b>
               </h3>
            </div>
            <div class="panel-body">
               {{ Form::open(['route' => 'custumers.store_c']) }}
               @include('custumers.partials.form_c')
               {{ Form::close() }}
            </div>
         </div>
      </div>
   </div>
</div>
@endsection