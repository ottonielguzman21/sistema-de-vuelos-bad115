@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row col-md-10">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">

                    <nav aria-label="breadcrumb">
                       <ol class="breadcrumb mt-sm-0">
                          <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                          <li class="breadcrumb-item active"><a href="{{route('custumers.index')}}"><i class="fas fa-sitemap"></i> Modelos</a></li>
                          <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-edit"></i> Ver Cliente {{$custumer->User->lname}}</a></li>          
                          <li class="breadcrumb-item"><a href="#"></a></li>
                       </ol>
                    </nav>
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h2><i class="fas fa-info-circle"></i> Cliente Persona Natural {{$custumer->User->lname}}</h2>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-primary" href="{{ route('custumers.index') }}"><i class="fas fa-arrow-left"></i> </a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Apellidos:</strong>
                                {{ $custumer->User->lname}}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Nombre:</strong>
                                {{ $custumer->User->name }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Tipo Documento:</strong>
                                {{ $custumer->tipo_documento }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>N° Viajero frecuente:</strong>
                                {{ $custumer->numero_viajero_frecuente}}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Direccion:</strong>
                                {{ $custumer->direccion }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Telefono Fijo:</strong>
                                {{ $custumer->telefono_fijo }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>N° de Documento:</strong>
                                {{ $custumer->persona->numero_documento }}
                            </div>
                        </div><div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>N° Viajero Frecuente:</strong>
                                {{ $custumer->numero_viajero_frecuente  }}
                            </div>
                        </div><div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Género:</strong>
                                {{ $custumer->persona->genero }}
                            </div>
                        </div><div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Estado Civil:</strong>
                                {{ $custumer->persona->estado_civil }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
