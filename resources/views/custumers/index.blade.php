@extends('layouts.app_a')
@section('content')
<div class="row">
   <div class ="col-sm-12 ">
      <div class="full.left container">
         <nav aria-label="breadcrumb">
            <ol class="breadcrumb mt-sm-0">
               <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"> </i>Inicio</a></li>
               <li class="breadcrumb-item active"><a href="{{url('custumers')}}"><i class="fas fa-sitemap"></i> Clientes</a></li>
               <li class="breadcrumb-item"><a href="#"></a></li>
            </ol>
         </nav>
         @if ($custumers == null)
         <h3>Gestion de Asignacion de Clientes.</h3>
         <h3>No hay Clientes en el Sistema. </h3>
         <h4>Para poder gestionar sus Clientes, Ingrese Clientes Naturales, o Empresas</h4>
         @endif
         @if ($custumers !== null)
         <div class="panel-heading">
            <h3><b><i class="fas fa-sitemap"></i> Gestion de Clientes</b></h3>
         </div>
         <br>
         @endif
         <div class="col-sm-12">
            <div class="col-sm-3">
               @if ($custumers !== null)
               <a class="btn btn-success" href="{{ route('custumers.create') }}"><i class="fas fa-plus"></i> Nuevo Cliente -Persona Natural</a><br><br>
               @endif
            </div>
            <div class ="col-sm-3 inline">
               @if ($custumers !== null)
               <a class="btn btn-success" href="{{ route('custumers.create_1') }}"><i class="fas fa-plus"></i> Nuevo Cliente Compañia</a>
               @endif
               <br><br>
            </div>
            {!! Form::open(['route'=>'custumers.index','method'=>'GET', 'class'=>'form-inline float-right']) !!}
            <div class="form-group">
               {!! Form::text('numero_viajero_frecuente', null, ['class' => 'form-control','placeholder'=>'N° Cliente']) !!}
            </div>
            <button type="submit" class="btn btn-info "><i class="fas fa-search"></i> Buscar</button>
            {!! Form::close() !!}
         </div>
         @if ($custumers != null)
         <h4>Seleccione la pestaña adecuada para gestionar las asignaciones de alumnos.</h4>
         @endif
      </div>
   </div>
</div>
@if ($message = Session::get('success'))
<div class="alert alert-success">
   <p>{{ $message }}</p>
</div>
@endif
@if ($message = Session::get('error'))
<div class="alert alert-danger">
   <p>{{ $message }}</p>
</div>
@endif
 @if ($custumers != null)
<div class="container">
   <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home">Todos los Clientes</a></li>
      <li><a data-toggle="tab" href="#menu1">Personas Naturales</a></li>
      <li><a data-toggle="tab" href="#menu2">Empresas.</a></li>
   </ul>
   <div class="tab-content">
      <div id="home" class="tab-pane fade in active">
         <table class="table table-striped table-responsive" style="text-align:center" >
            <thead>
               <tr>
                  <th with="80px">No</th>
                  <th style="text-align:center">Apellidos</th>
                  <th style="text-align:center">Nombres</th>
                  <th style="text-align:center">Tipo de Documento</th>
                  <th style="text-align:center">Tipo de Cliente</th>
                  <th style="text-align:center">N° Viajero Frecuente</th>
                  <th style="text-align:center">Direccion</th>
                  <th style="text-align:center">Tel Fijo</th>
               </tr>
               <?php $i=1; ?>
               @foreach ($custumers as $key => $custumer)
               <tr>
                  <td>{{ ++$i }}</td>
                  <td>{{ $custumer->User->lname }}</td>
                  <td>{{ $custumer->User->name }}</td>
                  <td>{{ $custumer->tipo_documento }}</td>
                  @if ($custumer->tipo_cliente == 1)
                  <td> Persona Natural </td>
                  @endif
                  @if ($custumer->tipo_cliente == 2)
                  <td> Empresa </td>
                  @endif
                  <td>{{ $custumer->numero_viajero_frecuente }}</td>
                  <td>{{ $custumer->direccion }}</td>
                  <td>{{ $custumer->telefono_fijo }}</td>
               </tr>
               @endforeach
            </thead>
         </table>
      </div>
      <div id="menu1" class="tab-pane fade">
         <table class="table table-striped table-responsive" style="text-align:center" >
            <thead>
               <tr>
                  <th with="80px">No</th>
                  <th style="text-align:center">Apellidos</th>
                  <th style="text-align:center">Nombres</th>
                  <th style="text-align:center">Tipo de Documento</th>
                  <th style="text-align:center">N° Viajero Frecuente</th>
                  <th style="text-align:center">Direccion</th>
                  <th style="text-align:center">Tel Fijo</th>
                  <th style="text-align:center">N° Documento</th>
                  <th style="text-align:center">N° Viajero Frecuente</th>
                  <th style="text-align:center">Genero</th>
                  <th style="text-align:center">Estado Civil</th>
                  <th style="text-align:center">Tel Movil</th>
                  <th style="text-align:center">Accion</th>
               </tr>
               <?php $i=1; ?>
               @foreach ($custumers_p as $key => $custumer)
               <tr>
                  <td>{{ ++$i }}</td>
                  <td>{{ $custumer->User->lname }}</td>
                  <td>{{ $custumer->User->name }}</td>
                  <td>{{ $custumer->tipo_documento }}</td>
                  <td>{{ $custumer->numero_viajero_frecuente }}</td>
                  <td>{{ $custumer->direccion }}</td>
                  <td>{{ $custumer->telefono_fijo }}</td>
                  <td>{{ $custumer->persona->numero_documento }}</td>
                  <td>{{ $custumer->numero_viajero_frecuente }}</td>
                  <td>{{ $custumer->persona->genero }}</td>
                  <td>{{ $custumer->persona->estado_civil }}</td>
                  <td>{{ $custumer->persona->telefono_movil }}</td>
                  <td width="350px">
                     <form action="{{  route('custumers.destroy',$custumer->id) }}" method="POST">
                        <a class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{ route('custumers.show',$custumer->id) }}"><i class="fas fa-eye"></i></a>
                        <a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Editar" href="{{ route('custumers.edit',$custumer->id) }}"><i class="fas fa-edit"></i></a>
                        @csrf
                     </form>
                  </td>
               </tr>
               @endforeach
            </thead>
         </table>
      </div>
      <div id="menu2" class="tab-pane fade">
         <table class="table table-striped table-responsive" style="text-align:center" >
            <thead>
               <tr>
                  <th with="80px">No</th>
                  <th style="text-align:center">Apellidos</th>
                  <th style="text-align:center">Nombres</th>
                  <th style="text-align:center">Tipo de Documento</th>
                  <th style="text-align:center">Tipo de Cliente</th>
                  <th style="text-align:center">N° Viajero Frecuente</th>
                  <th style="text-align:center">Direccion</th>
                  <th style="text-align:center">Tel Fijo</th>
                  <th style="text-align:center">Empresa</th>
                  <th style="text-align:center">Nom Contacto</th>
                  <th style="text-align:center">NIT</th>
                  <th style="text-align:center">NIC</th>
                  <th style="text-align:center">Accion</th>
               </tr>
               <?php $i=1; ?>
               @foreach ($custumers_c as $key => $custumer)
               <tr>
                  <td>{{ ++$i }}</td>
                  <td>{{ $custumer->User->lname }}</td>
                  <td>{{ $custumer->User->name }}</td>
                  <td>{{ $custumer->tipo_documento }}</td>
                  <td>{{ $custumer->tipo_cliente }}</td>
                  <td>{{ $custumer->numero_viajero_frecuente }}</td>
                  <td>{{ $custumer->direccion }}</td>
                  <td>{{ $custumer->telefono_fijo }}</td>
                  <td>{{ $custumer->compania->nombre_empresa }}</td>
                  <td>{{ $custumer->compania->nombre_contacto }}</td>
                  <td>{{ $custumer->compania->NIT }}</td>
                  <td>{{ $custumer->compania->NIC}}</td>
                  <td width="350px">
               <form action="{{  route('custumers.destroy',$custumer->id) }}" method="POST">
                  <a class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{ route('custumers.show',$custumer->id) }}"><i class="fas fa-eye"></i></a>
                  <a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Editar" href="{{ route('custumers.edit',$custumer->id) }}"><i class="fas fa-edit"></i></a>
                  @csrf
               </form>
                  </td>
               </tr>
               @endforeach
            </thead>
         </table>
      </div>
   </div>
   {!! $custumers->links() !!}
</div>
@endif
@endsection
