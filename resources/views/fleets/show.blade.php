@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row col-md-10">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">
                    <nav aria-label="breadcrumb">
                       <ol class="breadcrumb mt-sm-0">
                          <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                          <li class="breadcrumb-item active"><a href="{{route('fleets.index')}}"><i class="fas fa-sitemap"></i> Clientes</a></li>
                          <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-edit"></i> Ver Asignacion a Flota: {{ $fleets->airline->nombre_oficial}}, {{ $fleets->airline->pais_origen }}</a></li>
                          <li class="breadcrumb-item"><a href="#"></a></li>
                       </ol>
                    </nav>
                    <div class="row">
                       <div class="col-lg-12 margin-tb">
                          <div class="pull-left">
                             <h2><i class="fas fa-edit"></i> Editar Flota: {{ $fleets->airline->nombre_oficial}}, {{ $fleets->airline->pais_origen }}</h2>
                          </div>
                       </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Aerolinea:</strong>
                                {{ $fleets->airline->id }}. {{ $fleets->airline->nombre_oficial }}, {{ $fleets->airline->pais_origen }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Aeroplano:</strong>
                                {{ $fleets->airplane->id }}. {{ $fleets->airplane->modelo->nombre_modelo }}, {{ $fleets->airplane->tipo_avion }}, {{ $fleets->airplane->capacidad }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
