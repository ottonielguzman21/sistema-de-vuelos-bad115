@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row col-md-10">
      <div class="col-md-12 col-md-offset-2 ">
         <div class="panel panel-default">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mt-sm-0">
                  <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                  <li class="breadcrumb-item active"><a href="{{route('fleets.index')}}"><i class="fas fa-sitemap"></i> Clientes</a></li>
                  <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-edit"></i> Editar Flota: {{ $fleets->airline->nombre_oficial}}, {{ $fleets->airline->pais_origen }}</a></li>
                  <li class="breadcrumb-item"><a href="#"></a></li>
               </ol>
            </nav>
            <div class="row">
               <div class="col-lg-12 margin-tb">
                  <div class="pull-left">
                     <h2><i class="fas fa-edit"></i> Editar Flota: {{ $fleets->airline->nombre_oficial}}, {{ $fleets->airline->pais_origen }}</h2>
                  </div>
               </div>
            </div>
            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
               <p>{{ $message }}</p>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
               <strong>Whoops!</strong> Hubo algunos problemas con su entrada!<br><br>
               <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
               </ul>
            </div>
            @endif
            <form action="{{ route('fleets.update',$fleets->id) }}" method="POST">
               @csrf
               @method('PUT')
               <div class="row">
      <div class="col-sm-10">
         <div class="form-group {{ $errors->has('airline_id') ? 'has-error' : "" }}">
            <strong>Aerolinea:</strong>
         <i>
            <select name="airline_id" class="form-control">
               <option selected value="{{ $fleets->airline->id }}">{{ $fleets->airline->id }}. {{ $fleets->airline->nombre_oficial }}, {{ $fleets->airline->pais_origen }}</option>
               @foreach($airlines as $airline)
               <option value="{{$airline->id}}">{{$airline->id}}. {{$airline->nombre_oficial }} {{$airline->pais_origen }}</option>
               @endforeach
            </select>
         </i>
         <div class="help-block"> 
            <strong>{{ $errors->first('airline_id', 'Seleccione uno') }}</strong>
         </div>
         </div>
      </div>
      <div class="col-sm-10">
         <div class="form-group {{ $errors->has('airplane_id') ? 'has-error' : "" }}">
            <strong>Aeroplano:</strong>
         <i>
            <select name="airplane_id" class="form-control">
               <option selected value="{{ $fleets->airplane->id }}">{{ $fleets->airplane->id }}. {{$airplane->nombre_aeroplano }}, {{ $fleets->airplane->modelo->nombre_modelo }}, {{ $fleets->airplane->tipo_avion }}, {{ $fleets->airplane->capacidad }}</option>
                  @foreach($airplanes as $airplane)
                  <option value="{{$airplane->id}}">{{$airplane->id}}. {{$airplane->nombre_aeroplano }}, {{$airplane->modelo->nombre_modelo }}, {{$airplane->tipo_avion }}, {{ $airplane->capacidad }}</option>
                  @endforeach
               </select>
         </i>
         <div class="help-block"> 
            <strong>{{ $errors->first('airplane_id', 'Seleccione uno') }}</strong>
         </div>
         </div>
      </div>
         <div class="col-xs-12 col-sm-12 col-md-12 text-center">
         <button type="submit" class="btn btn-sm btn-primary"><i class="fas fa-save"></i> Actualizar</button>
         <a class="btn btn-sm btn-danger" href="{{ route('fleets.index') }}">Cancelar</a>
         </div>
      </div>
      </form>
   </div>
</div>
</div>
</div>
@endsection