@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row">
      <div class="col-md-12 col-md-offset-2">
         <div class="panel panel-default">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mt-sm-0">
                  <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"> </i>Inicio</a></li>
                  <li class="breadcrumb-item active"><a href="{{url('fleets')}}"><i class="fas fa-sitemap"></i> Flotas</a></li>
                  <li class="breadcrumb-item"><a href="#"></a></li>
               </ol>
            </nav>
            <div class="panel-heading">
               <h3><b><i class="fas fa-sitemap"></i> Gestión de Asignacion Avion-Aerolinea (Flota)</b></h3>
            </div>
            {!! Form::open(['route'=>'fleets.index','method'=>'GET', 'class'=>'form-inline float-right']) !!}
            <div class="form-group">
               {!! Form::text('airline_id', null, ['class' => 'form-control','placeholder'=>'Aerolinea']) !!}
            </div>
            <button type="submit" class="btn btn-info "><i class="fas fa-search"></i> Buscar</button>
            {!! Form::close() !!}
            <div class="pull-right">
               <a class="btn btn-success" href="{{ route('fleets.create') }}"><i class="fas fa-plus"></i> Agregar Asignacion Avion-Aerolinea (Flota)</a>
            </div>
            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
               <p>{{ $message }}</p>
            </div>
            @endif
            <br>
            <table class="table table-bordered table-striped">
               <tr>
                  <th>No</th>
                  <th>Nombre Aerolinea</th>
                  <th>Pais de Origen</th>
                  <th>Nombre de Aeroplano</th>
                  <th>Modelo</th>
                  <th>Tipo Avión</th>
                  <th>Capacidad</th>
                  <th>Estado del Aeroplano</th>
                  <th width="280px">Accion</th>
               </tr>
               @foreach ($fleets as $fleet)
               <tr>
                  <td>{{ ++$i }}</td>
                  <td>{{ $fleet->airline->nombre_oficial }}</td>
                  <td>{{ $fleet->airline->pais_origen }}</td>
                  <td>{{ $fleet->airplane->nombre_aeroplano }}</td>
                  <td>{{ $fleet->airplane->modelo->nombre_modelo }}</td>
                  <td>{{ $fleet->airplane->tipo_avion }}</td>
                  <td>{{ $fleet->airplane->capacidad }}</td>
                  @if($fleet->airplane->estado==1)
                  <td>Activo</td>
                  @endif
                  @if($fleet->airplane->estado==0)
                  <td>Inactivo</td>
                  @endif
                  <td width="350px">
                     <form action="{{ route('fleets.destroy',$fleet->id) }}" method="POST">
                        <a class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{ route('fleets.show',$fleet->id) }}"><i class="fas fa-eye"></i>Mostrar</a>
                        <a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Editar" href="{{ route('fleets.edit',$fleet->id) }}"><i class="fas fa-edit"></i>Editar</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i>Eliminar</button>
                     </form>
                  </td>
               </tr>
               @endforeach
            </table>
            {!! $fleets->links() !!}
         </div>
      </div>
   </div>
</div>
@endsection