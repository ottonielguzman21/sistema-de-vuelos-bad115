   <div class="row">
         <div class="col-sm-2">
            {!! form::label('Aerolinea') !!}
         </div>
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('airline_id') ? 'has-error' : "" }}">
            <i>
               <select name="airline_id" class="form-control">
                  <option disabled selected>Seleccione Aerolinea</option>
                  @foreach($airlines as $airline)
                  <option value="{{$airline->id}}">{{$airline->id}}. {{$airline->nombre_oficial }} {{$airline->pais_origen }}</option>
                  @endforeach
               </select>
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('airline_id', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
         <div class="col-sm-2">
            {!! form::label('Aeroplano') !!}
         </div>
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('airplane_id') ? 'has-error' : "" }}">
            <i>
               <select name="airplane_id" class="form-control">
                  <option disabled selected>Seleccione Aeroplano</option>
                  @foreach($airplanes as $airplane)
                  <option value="{{$airplane->id}}">{{$airplane->id}}. {{$airplane->nombre_aeroplano }}, {{$airplane->tipo_avion }}, {{ $airplane->capacidad }}</option>
                  @endforeach
               </select>
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('airplane_id', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
   </div>
   <div class="form-group text-center">
   {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
   <a class="btn btn-sm btn-danger" href="{{ route('fleets.index') }}">Cancelar</a>
</div>