@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row">
      <div class="col-md-12 ">
         <div class="panel panel-default col-md-offset-2">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mt-sm-0">
                  <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"> </i>Inicio</a></li>
                  <li class="breadcrumb-item active"><a href="{{url('brands')}}"><i class="fas fa-sitemap"></i> Marcas</a></li>
                  <li class="breadcrumb-item"><a href="#"></a></li>
               </ol>
            </nav>
            <div class="panel-heading">
               <h3><b><i class="fas fa-sitemap"></i> Gestion de Marcas</b></h3>
            </div>
            {!! Form::open(['route'=>'brands.index','method'=>'GET', 'class'=>'form-inline float-right']) !!}
            <div class="form-group">
               {!! Form::text('nombre_marca', null, ['class' => 'form-control','placeholder'=>'Marca']) !!}
            </div>
            <div class="form-group">
               {!! Form::text('fabricante', null, ['class' => 'form-control','placeholder'=>'Fabricante']) !!}
            </div>
            <button type="submit" class="btn btn-info "><i class="fas fa-search"></i> Buscar</button>
            {!! Form::close() !!}
            <div class="pull-right">
               <a class="btn btn-success" href="{{ route('brands.create') }}"><i class="fas fa-plus"></i>  Crear nueva Marca</a>
            </div>
            @if ($message = Session::get('info'))
            <div class="alert alert-success">
               <p>{{ $message }}</p>
            </div>
            @endif
            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
               <p>{{ $message }}</p>
            </div>
            @endif
            <br>
            <table class="table table-bordered table-striped">
               <tr>
                  <th>No</th>
                  <th>Marca</th>
                  <th>Fabricante</th>
                  <th width="280px">Accion</th>
               </tr>
               @foreach ($brands as $brand)
               <tr>
                  <td>{{ ++$i }}</td>
                  <td>{{ $brand->nombre_marca }}</td>
                  <td>{{ $brand->fabricante }}</td>
                  <td width="350px">
                     <form action="{{ route('brands.destroy',$brand->id) }}" method="POST">
                        <a class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{ route('brands.show',$brand->id) }}"><i class="fas fa-eye"></i>Mostrar</a>
                        <a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Editar" href="{{ route('brands.edit',$brand->id) }}"><i class="fas fa-edit"></i>Editar</a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i>Eliminar</button>
                     </form>
                  </td>
               </tr>
               @endforeach
            </table>
            {!! $brands->links() !!}
         </div>
      </div>
   </div>
</div>
@endsection