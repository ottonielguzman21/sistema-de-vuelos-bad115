@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row col-md-10">
      <div class="col-md-12 ">
         <div class="panel panel-default">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mt-sm-0">
                  <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                  <li class="breadcrumb-item active"><a href="{{route('brands.index')}}"><i class="fas fa-sitemap"></i> Marcas</a></li>
                  <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-edit"></i> Editar Marca</a></li>
                  <li class="breadcrumb-item active"><a href="{{url('#')}}">{{$brand->id}}</a></li>
                  <li class="breadcrumb-item"><a href="#"></a></li>
               </ol>
            </nav>
            <div class="row">
               <div class="col-lg-12 margin-tb">
                  <div class="pull-left">
                     <h2><i class="fas fa-edit"></i> Editar Marca {{$brand->id}}</h2>
                  </div>
               </div>
            </div>
            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
               <p>{{ $message }}</p>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
               <strong>Whoops!</strong> Hubo algunos problemas con su entrada!<br><br>
               <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
               </ul>
            </div>
            @endif
            <form action="{{ route('brands.update',$brand->id) }}" method="POST">
               @csrf
               @method('PUT')
               <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12">
                     <div class="form-group">
                        <strong>Marca:</strong>
                        <input type="text" name="nombre_marca" value="{{ $brand->nombre_marca }}" class="form-control" placeholder="Nombre de la Marca">
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12">
                     <div class="form-group">
                        <strong>Fabricante:</strong>
                        <input type="text" name="fabricante" value="{{ $brand->fabricante }}" class="form-control" placeholder="Fabricante">
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12">
                     <div class="form-group">
                        <strong>Estado:</strong>
                        <select name="estado" class="form-control">
                           <?php if($brand->estado=='1'): ?>
                           <option value="{{$brand->estado}}"  >Activa</option>
                           <?php endif; ?>
                           <?php if($brand->estado=='0'): ?>
                           <option value="{{$brand->estado}}"  >Inactiva</option>
                           <?php endif; ?>
                           <option value='1'>Activa</option>
                           <option value='0'>Inactiva</option>
                        </select>
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                     <button type="submit" class="btn btn-sm btn-primary"><i class="fas fa-save"></i> Guardar</button>
                     <a class="btn btn-sm btn-danger" href="{{ route('modelos.index') }}">Cancelar</a>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
