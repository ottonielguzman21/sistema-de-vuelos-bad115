@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row col-md-10">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">

                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mt-sm-0">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                            <li class="breadcrumb-item active"><a href="{{ route('brands.index') }}"><i class="fas fa-sitemap"></i> Marcas</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-eye"></i> Mostrar Marca</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('#')}}">{{$brand->id}}</a></li>
                            <li class="breadcrumb-item"><a href="#"></a></li>
                        </ol>
                    </nav>
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h2><i class="fas fa-info-circle"></i> Detalle de la Marca</h2>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-primary" href="{{ route('brands.index') }}"><i class="fas fa-arrow-left"></i> </a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Marca:</strong>
                                {{ $brand->nombre_marca }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Fabricante:</strong>
                                {{ $brand->fabricante }}
                            </div>
                        </div>
                        <?php if($brand->estado=='1'): ?>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Estado:</strong>
                                Activa
                            </div>
                        </div> 
                        <?php endif; ?>
                        <?php if($brand->estado=='0'): ?>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Estado:</strong>
                                Inactiva
                            </div>
                        </div> 
                        <?php endif; ?>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Fecha de creacion:</strong>
                                {{ $brand->created_at }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Fecha de modificacion:</strong>
                                {{ $brand->updated_at }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
