<div class="row">
   <div class="col-sm-2">
      {!! form::label('nombre_marca', 'Nombre de la Marca') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('nombre_marca') ? 'has-error' : "" }}">
      <i>{{ Form::text('nombre_marca',NULL, ['class'=>'form-control', 'id'=>'nombre_marca', 'placeholder'=>'Marca']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('nombre_marca', 'Ingrese: A-Z a-z') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('fabricante', 'Fabricante') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('fabricante') ? 'has-error' : "" }}">
      <i>{{ Form::text('fabricante',NULL, ['class'=>'form-control', 'id'=>'fabricante', 'placeholder'=>'Fabricante']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('fabricante', 'Ingrese: A-Z a-z') }}</strong> 
      </div>
   </div>
</div>
</div>

<div class="form-group text-center">
   {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
   <a class="btn btn-sm btn-danger" href="{{ route('brands.index') }}">Cancelar</a>
</div>