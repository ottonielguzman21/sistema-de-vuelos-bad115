<div class="row">
   <div class="col-sm-2">
      {!! form::label('nombre_modelo', 'Modelo') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('nombre_modelo') ? 'has-error' : "" }}">
      <i>{{ Form::text('nombre_modelo',NULL, ['class'=>'form-control', 'id'=>'nombre_modelo', 'placeholder'=>'Modelo']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('nombre_modelo', 'Ingrese: A-Z a-z 0-9 y espacios') }}</strong> 
      </div>
   </div>
</div>
</div>
      <div class="row">
         <div class="col-sm-2">
            {!! form::label('Marca') !!}
         </div>
         <div class="col-sm-6">
            <div class="form-group {{ $errors->has('brand_id') ? 'has-error' : "" }}">
            <i>
               <select name="brand_id" class="form-control">
                  <option disabled selected>Seleccione Marca</option>
                  @foreach($brands as $brand)
                  <option value="{{$brand->id}}">{{$brand->id}}. {{$brand->nombre_marca }}</option>
                  @endforeach
               </select>
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('brand_id', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
   </div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('envergadura', 'Envergadura') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('envergadura') ? 'has-error' : "" }}">
      <i>{{ Form::number('envergadura',NULL, ['class'=>'form-control', 'id'=>'envergadura', 'placeholder'=>'Envergadura', 'min'=>'0', 'step'=>'0.01',]) }}</i>
      <div class="help-block required" >
         <strong>{{ $errors->first('envergadura', 'Ingrese numeros enteros o Decimales mayores a 0') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('longitud', 'Longitud') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('longitud') ? 'has-error' : "" }}">
      <i>{{ Form::number('longitud',NULL, ['class'=>'form-control', 'id'=>'longitud', 'placeholder'=>'Longitud', 'min'=>'0', 'step'=>'0.01',]) }}</i>
      <div class="help-block required" >
         <strong>{{ $errors->first('longitud', 'Ingrese numeros Enteros o Decimales mayores a 0') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('year', 'Año') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('year') ? 'has-error' : "" }}">
      <i>{{ Form::number('year',NULL, ['class'=>'form-control', 'id'=>'year', 'placeholder'=>'Año', 'min'=>'1950', 'max'=>'2050', 'step'=>'1',]) }}</i>
      <div class="help-block required" >
         <strong>{{ $errors->first('year', 'Ingrese numeros, formato: 1999') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('peso_vacio', 'Peso Vacio') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('peso_vacio') ? 'has-error' : "" }}">
      <i>{{ Form::number('peso_vacio',NULL, ['class'=>'form-control', 'id'=>'peso_vacio', 'placeholder'=>'Peso Vacio', 'min'=>'0', 'step'=>'1',]) }}</i>
      <div class="help-block required" >
         <strong>{{ $errors->first('peso_vacio', 'Ingrese numeros Enteros') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('peso_max_despegue', 'Peso Max Despegue') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('peso_max_despegue') ? 'has-error' : "" }}">
      <i>{{ Form::number('peso_max_despegue',NULL, ['class'=>'form-control', 'id'=>'peso_max_despegue', 'placeholder'=>'Peso Max Despegue', 'min'=>'0', 'step'=>'1',]) }}</i>
      <div class="help-block required" >
         <strong>{{ $errors->first('peso_max_despegue', 'Ingrese numeros Enteros') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('carga_util', 'Carga Útil') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('carga_util') ? 'has-error' : "" }}">
      <i>{{ Form::number('carga_util',NULL, ['class'=>'form-control', 'id'=>'carga_util', 'placeholder'=>'Carga Útil', 'min'=>'0', 'step'=>'1',]) }}</i>
      <div class="help-block required" >
         <strong>{{ $errors->first('carga_util', 'Ingrese numeros Enteros') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('factor_carga', 'Factor de Carga') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('factor_carga') ? 'has-error' : "" }}">
      <i>{{ Form::number('factor_carga',NULL, ['class'=>'form-control', 'id'=>'factor_carga', 'placeholder'=>'Factor de Carga', 'min'=>'0', 'step'=>'0.01',]) }}</i>
      <div class="help-block required" >
         <strong>{{ $errors->first('factor_carga', 'Ingrese numeros enteros o Decimales mayores a 0') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('velocidad_maxima', 'Velocidad Máxima') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('velocidad_maxima') ? 'has-error' : "" }}">
      <i>{{ Form::number('velocidad_maxima',NULL, ['class'=>'form-control', 'id'=>'velocidad_maxima', 'placeholder'=>'Velocidad Máxima', 'min'=>'0', 'step'=>'1',]) }}</i>
      <div class="help-block required" >
         <strong>{{ $errors->first('velocidad_maxima', 'Ingrese numeros Enteros') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('motor', 'Motor') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('motor') ? 'has-error' : "" }}">
      <i>{{ Form::text('motor',NULL, ['class'=>'form-control', 'id'=>'motor', 'placeholder'=>'Motor']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('motor', 'Ingrese: A-Z a-z y espacios') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="row">
   <div class="col-sm-2">
      {!! form::label('autonomia', 'Autonomia') !!}
   </div>
   <div class="col-sm-6">
      <div class="form-group {{ $errors->has('autonomia') ? 'has-error' : "" }}">
      <i>{{ Form::text('autonomia',NULL, ['class'=>'form-control', 'id'=>'autonomia', 'placeholder'=>'Autonomia']) }}</i>
      <div class="help-block" >
         <strong>{{ $errors->first('autonomia', 'Ingrese: A-Z a-z y espacios') }}</strong> 
      </div>
   </div>
</div>
</div>
<div class="form-group text-center">
   {{ Form::submit('Guardar', ['class' => 'btn btn-sm btn-primary']) }}
   <a class="btn btn-sm btn-danger" href="{{ route('modelos.index') }}">Cancelar</a>
</div>