@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row">
      <div class="col-md-12 col-md-offset-2 ">
         <div class="panel panel-default">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mt-sm-0">
                  <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"> </i>Inicio</a></li>
                  <li class="breadcrumb-item active"><a href="{{url('modelos')}}"><i class="fas fa-sitemap"></i> Modelo</a></li>
                  <li class="breadcrumb-item"><a href="#"></a></li>
               </ol>
            </nav>
            <div class="panel-heading">
               <h3><b><i class="fas fa-sitemap"></i> Gestion de Modelos de Aviones</b></h3>
            </div>
            {!! Form::open(['route'=>'brands.index','method'=>'GET', 'class'=>'form-inline float-right']) !!}
            <div class="form-group">
               {!! Form::text('nombre_marca', null, ['class' => 'form-control','placeholder'=>'Marca']) !!}
            </div>
            <div class="form-group">
               {!! Form::text('fabricante', null, ['class' => 'form-control','placeholder'=>'Fabricante']) !!}
            </div>
            <button type="submit" class="btn btn-info "><i class="fas fa-search"></i> Buscar</button>
            {!! Form::close() !!}
            <div class="pull-right">
               <a class="btn btn-success" href="{{ route('modelos.create') }}"><i class="fas fa-plus"></i>  Crear nuevo Modelo</a>
            </div>
            @if ($message = Session::get('info'))
            <div class="alert alert-success">
               <p>{{ $message }}</p>
            </div>
            @endif
            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
               <p>{{ $message }}</p>
            </div>
            @endif
            <br>
            <table class="table table-bordered table-striped">
               <tr>
                  <th with="80px">No</th>
                  <th style="text-align:center">Modelo</th>
                  <th style="text-align:center">Marca</th>
                  <th style="text-align:center">Envergadura</th>
                  <th style="text-align:center">Longitud</th>
                  <th style="text-align:center">Año</th>
                  <th style="text-align:center">Peso vacio</th>
                  <th style="text-align:center">Peso Max Despegue</th>
                  <th style="text-align:center">Carga Util</th>
                  <th style="text-align:center">Factor Carga</th>
                  <th style="text-align:center">Velocidad Maxima</th>
                  <th style="text-align:center">Motor</th>
                  <th style="text-align:center">Autonomia</th>
                  <th style="text-align:center">Accion</th>
               </tr>
               <?php $i=1; ?>
                @foreach ($modelos as $key => $model)
               
               <tr>

                  <td>{{ ++$i }}</td>

                  <td>{{ $model->nombre_modelo }}</td>
                  <td> {{$model->brand->nombre_marca}}</td>                  
                  <td>{{ $model->envergadura }}</td>
                  <td>{{ $model->longitud }}</td>
                  <td>{{ $model->year }}</td>
                  <td>{{ $model->peso_vacio }}</td>
                  <td>{{ $model->peso_max_despegue }}</td>
                  <td>{{ $model->carga_util }}</td>
                  <td>{{ $model->factor_carga }}</td>
                  <td>{{ $model->velocidad_maxima }}</td>
                  <td>{{ $model->motor }}</td>
                  <td>{{ $model->autonomia }}</td>
                  
                  <td width="350px">
                     <form action="{{  route('modelos.destroy',$model->id) }}" method="POST">
                        <a class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detalles" href="{{ route('modelos.show',$model->id) }}"><i class="fas fa-eye"></i></a>
                        <a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Editar" href="{{ route('modelos.edit',$model->id) }}"><i class="fas fa-edit"></i></a>
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash"></i></button>
                     </form>
                  </td>
               </tr>
               @endforeach
            </table>
            {!! $modelos->links() !!}
         </div>
      </div>
   </div>
</div>
@endsection