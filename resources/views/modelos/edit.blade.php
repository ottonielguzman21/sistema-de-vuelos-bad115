@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row col-md-10">
      <div class="col-md-12 col-md-offset-2 ">
         <div class="panel panel-default">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb mt-sm-0">
                  <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                  <li class="breadcrumb-item active"><a href="{{route('modelos.index')}}"><i class="fas fa-sitemap"></i> Modelos</a></li>
                  <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-edit"></i> Editar Modelo</a></li>
                  <li class="breadcrumb-item active"><a href="{{url('#')}}">{{$modelo->nombre_modelo}}</a></li>
                  <li class="breadcrumb-item"><a href="#"></a></li>
               </ol>
            </nav>
            <div class="row">
               <div class="col-lg-12 margin-tb">
                  <div class="pull-left">
                     <h2><i class="fas fa-edit"></i> Editar Modelo {{$modelo->nombre_modelo}}</h2>
                  </div>
               </div>
            </div>
            @if ($message = Session::get('error'))
            <div class="alert alert-danger">
               <p>{{ $message }}</p>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
               <strong>Whoops!</strong> Hubo algunos problemas con su entrada!<br><br>
               <ul>
                  @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
               </ul>
            </div>
            @endif
            <form action="{{ route('modelos.update',$modelo->id) }}" method="POST">
               @csrf
               @method('PUT')
               <div class="row">
                  <div class="col-sm-10">
                     <div class="form-group {{ $errors->has('nombre_modelo') ? 'has-error' : "" }}">
                     <strong>Modelo:</strong>
                     <i aaaaa>
                     <input type="text" name="nombre_modelo" value="{{ $modelo->nombre_modelo }}" class="form-control" placeholder="Modelo">
                     </i>
                     <div class="help-block"> 
                        <strong>{{ $errors->first('nombre_modelo', 'Seleccione uno') }}</strong>
                     </div>
                  </div>
               </div>
               <div class="col-sm-10">
                  <div class="form-group {{ $errors->has('brand_id') ? 'has-error' : "" }}">
                     <strong>Marca:</strong>
                  <i>
                     <select name="brand_id" class="form-control">
                        <option value="{{$modelo->brand->id}}" selected>{{$modelo->brand->id}}. {{$modelo->brand->nombre_marca}}</option>
                        @foreach($brands as $brand)
                        <option value="{{$brand->id}}">{{$brand->id}}. {{$brand->nombre_marca}}</option>
                        @endforeach
                     </select>
                  </i>
                  <div class="help-block"> 
                     <strong>{{ $errors->first('brand_id', 'Seleccione uno') }}</strong>
                  </div>
               </div>
         </div>
         <div class="col-sm-10">
            <div class="form-group {{ $errors->has('envergadura') ? 'has-error' : "" }}">
            <strong>Envergadura:</strong>
            <i >
            <input type="number" name="envergadura" value="{{ $modelo->envergadura }}" class="form-control" placeholder="Modelo" min="0" step="0.01">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('envergadura', 'Ingrese solo numeros decimales') }}</strong>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
            <div class="form-group {{ $errors->has('longitud') ? 'has-error' : "" }}">
            <strong>Longitud:</strong>
            <i >
            <input type="number" name="longitud" value="{{ $modelo->longitud }}" class="form-control" placeholder="Longitud" min="0" step="0.01">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('longitud', 'Ingrese solo numeros decimales') }}</strong>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
            <div class="form-group {{ $errors->has('year') ? 'has-error' : "" }}">
            <strong>Año:</strong>
            <i >
            <input type="number" name="year" value="{{ $modelo->year }}" class="form-control" placeholder="Año" min="1950" step="1">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('year', 'Ingrese solo numeros Enteros > 1950') }}</strong>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
            <div class="form-group {{ $errors->has('peso_vacio') ? 'has-error' : "" }}">
            <strong>Peso Vacio:</strong>
            <i >
            <input type="number" name="peso_vacio" value="{{ $modelo->peso_vacio }}" class="form-control" placeholder="Peso Vacio" min="0" step="1">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('peso_vacio', 'Ingrese solo numeros Enteros > 1950') }}</strong>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
            <div class="form-group {{ $errors->has('peso_max_despegue') ? 'has-error' : "" }}">
            <strong>Peso Max Despegue:</strong>
            <i >
            <input type="number" name="peso_max_despegue" value="{{ $modelo->peso_max_despegue }}" class="form-control" placeholder="Peso Max Despegue" min="0" step="1">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('peso_max_despegue', 'Ingrese solo numeros Enteros > 1950') }}</strong>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
            <div class="form-group {{ $errors->has('carga_util') ? 'has-error' : "" }}">
            <strong>Carga Útil:</strong>
            <i >
            <input type="number" name="carga_util" value="{{ $modelo->carga_util }}" class="form-control" placeholder="Carga Útil" min="0" step="1">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('carga_util', 'Ingrese solo numeros Enteros > 1950') }}</strong>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
            <div class="form-group {{ $errors->has('factor_carga') ? 'has-error' : "" }}">
            <strong>Factor de carga:</strong>
            <i >
            <input type="number" name="factor_carga" value="{{ $modelo->factor_carga }}" class="form-control" placeholder="Factor de carga" min="0" step="0.01">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('factor_carga', 'Ingrese solo numeros decimales') }}</strong>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
            <div class="form-group {{ $errors->has('velocidad_maxima') ? 'has-error' : "" }}">
            <strong>Velocidad Maxima:</strong>
            <i >
            <input type="number" name="velocidad_maxima" value="{{ $modelo->velocidad_maxima }}" class="form-control" placeholder="Velocidad Maxima" min="0" step="1">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('velocidad_maxima', 'Ingrese solo numeros Enteros > 1950') }}</strong>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
            <div class="form-group {{ $errors->has('motor') ? 'has-error' : "" }}">
            <strong>Motor:</strong>
            <i aaaaa>
            <input type="text" name="motor" value="{{ $modelo->motor }}" class="form-control" placeholder="Motor">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('motor', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
      <div class="col-sm-10">
            <div class="form-group {{ $errors->has('autonomia') ? 'has-error' : "" }}">
            <strong>Autonomia:</strong>
            <i aaaaa>
            <input type="text" name="autonomia" value="{{ $modelo->autonomia }}" class="form-control" placeholder="Autonomia">
            </i>
            <div class="help-block"> 
               <strong>{{ $errors->first('autonomia', 'Seleccione uno') }}</strong>
            </div>
         </div>
      </div>
         <div class="col-xs-12 col-sm-12 col-md-12 text-center">
         <button type="submit" class="btn btn-sm btn-primary"><i class="fas fa-save"></i> Guardar</button>
         <a class="btn btn-sm btn-danger" href="{{ route('modelos.index') }}">Cancelar</a>
         </div>
      </div>
      </form>
   </div>
</div>
</div>
</div>
@endsection