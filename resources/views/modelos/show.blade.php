@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row col-md-10">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">

                    <nav aria-label="breadcrumb">
                       <ol class="breadcrumb mt-sm-0">
                          <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i> Inicio</a></li>
                          <li class="breadcrumb-item active"><a href="{{route('modelos.index')}}"><i class="fas fa-sitemap"></i> Modelos</a></li>
                          <li class="breadcrumb-item active"><a href="{{url('#')}}"><i class="fas fa-edit"></i> Ver Modelo</a></li>
                          <li class="breadcrumb-item active"><a href="{{url('#')}}">{{$modelos->nombre_modelo}}</a></li>
                          <li class="breadcrumb-item"><a href="#"></a></li>
                       </ol>
                    </nav>
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h2><i class="fas fa-info-circle"></i> Detalle del Modelo</h2>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-primary" href="{{ route('modelos.index') }}"><i class="fas fa-arrow-left"></i> </a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Modelo:</strong>
                                {{ $modelos->nombre_modelo }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Marca:</strong>
                                {{ $modelos->brand->nombre_marca }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Envergadura:</strong>
                                {{ $modelos->envergadura }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Longitud:</strong>
                                {{ $modelos->longitud}}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Año:</strong>
                                {{ $modelos->year }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Peso vacio:</strong>
                                {{ $modelos->peso_vacio }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Peso Max de Despegue:</strong>
                                {{ $modelos->peso_max_despegue }}
                            </div>
                        </div><div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Carga Útil:</strong>
                                {{ $modelos->carga_util }}
                            </div>
                        </div><div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Factor de Carga:</strong>
                                {{ $modelos->factor_carga }}
                            </div>
                        </div><div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Velocidad Maxima:</strong>
                                {{ $modelos->velocidad_maxima }}
                            </div>
                        </div><div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Motor:</strong>
                                {{ $modelos->motor }}
                            </div>
                        </div><div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Autonomia:</strong>
                                {{ $modelos->autonomia }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
