@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row float-sm-right">
            <div class="col-md-12 col-md-offset-2 ">
                <div class="panel panel-default">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mt-sm-0">
                            <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="fas fa-home"></i>Inicio</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('permissions')}}"><i class="fas fa-tags"></i>Permisos</a></li>
                            <li class="breadcrumb-item"><a href="#"></a></li>
                        </ol>
                    </nav>
                    <div class="panel-heading">
                        <h3><i class="fas fa-user-tag"></i>Gestion de Permisos del sistema</h3>
                    </div>
                    {!! Form::open(['route'=>'permission.index','method'=>'GET', 'class'=>'form-inline float-right']) !!}
                        <div class="form-group">
                            {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Nombre']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('slug', null, ['class' => 'form-control','placeholder'=>'Slug']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::text('description', null, ['class' => 'form-control','placeholder'=>'Descripcion']) !!}
                        </div>
                        <button type="submit" class="btn btn-info "><i class="fas fa-search"></i> Buscar</button>
                    {!! Form::close() !!}
                    <div class="pull-right">
                        <a class="btn btn-success" href="{{ route('permission.create') }}"> <i class="fas fa-plus"></i> Crear nuevo permiso</a>
                    </div>
                    @if ($message = Session::get('mensaje'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Nombre</th>
                            <th>Slug</th>
                            <th>Descripcion</th>
                            <th width="320px">Action</th>
                        </tr>
                        @foreach ($permissions as $permission)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $permission->name }}</td>
                                <td>{{ $permission->slug }}</td>
                                <td>{{ $permission->description }}</td>
                                <td>
                                    <form action="{{ route('permission.destroy',$permission->id) }}" method="POST">

                                        <a class="btn btn-info" href="{{ route('permission.show',$permission->id) }}"><i class="fa fa-eye"></i> Mostrar</a>

                                        <a class="btn btn-primary" href="{{ route('permission.edit',$permission->id) }}"><i class="fas fa-edit"></i> Editar</a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i>Eliminar</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>

                    {!! $permissions->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
