@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row float-sm-right">
            <div class="col-md-12 col-md-offset-2 ">
                <div class="panel panel-default">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mt-sm-0">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Inicio</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('permissions')}}">Permisos</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('#')}}">Mostrar Permiso</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('#')}}">{{$permission->id}}</a></li>
                            <li class="breadcrumb-item"><a href="#"></a></li>
                        </ol>
                    </nav>
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                            <div class="pull-left">
                                <h2><i class="fas fa-eye"></i> Detalle del permiso:</h2>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-primary" href="{{ route('permission.index') }}"><i class="fas fa-arrow-left"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Nombre:</strong>
                                {{ $permission->name }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Slug:</strong>
                                {{ $permission->slug }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Descripcion:</strong>
                                {{ $permission->description }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Fecha de creacion:</strong>
                                {{ $permission->created_at }}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <strong>Fecha de modificacion:</strong>
                                {{ $permission->updated_at }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
