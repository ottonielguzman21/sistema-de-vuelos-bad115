<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Flight Systems &copy; </title>

    <link rel="stylesheet" href="css/reset.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/layout.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/t_style.css" type="text/css" media="all">
    <script type="text/javascript" src="js/jquery-1.4.2.js" ></script>
    <script type="text/javascript" src="js/cufon-yui.js"></script>
    <script type="text/javascript" src="js/cufon-replace.js"></script>
    <script type="text/javascript" src="js/Myriad_Pro_italic_600.font.js"></script>
    <script type="text/javascript" src="js/Myriad_Pro_italic_400.font.js"></script>
    <script type="text/javascript" src="js/Myriad_Pro_400.font.js"></script>
    <!--[if lt IE 9]>
    <script type="text/javascript" src="js/ie6_script_other.js"></script>
    <script type="text/javascript" src="js/html5.js"></script>
    <![endif]-->

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="icon" type="image/gif" href="{{url('images/flight-systems.gif')}}" sizes="32x32">
    <style>
        .dropbtn {
            background-color: #33393f;
            color: white;
            padding: 16px;
            font-size: 16px;
            border: none;
            cursor: pointer;
        }

        .dropdown {
            position: relative;
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #33393f;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
        }

        .dropdown-content a {
            color: #33393f;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {background-color: black;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }

        .dropdown:hover .dropbtn {
            background-color: #33393f;
        }
        </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-dark shadow-sm">
            <div class="container">
                <a class="navbar-brand text-white font-weight-bold" href="{{ url('/') }}">
                    FLIGHT SYSTEMS &copy;
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse list-group-flush" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        <li class="nav-item">
                            @can('system')
                            <div class="dropdown">
                                <button class="dropbtn">Administracion Usuarios</button>
                                <div class="dropdown-content">
                                @can('users.index')
                                     <a href="{{ route('users.index') }}" class="text-white nav-link">Usuarios</a>
                                @endcan
                                @can('roles.index')
                                    <a href="{{ route('roles.index') }}" class="text-white nav-link">Roles</a>
                                @endcan
                                @can('permissions.index')
                                    <a href="{{ route('permission.index') }}" class="text-white nav-link">Permisos</a>
                                @endcan
                                @can('modelos.index')
                                    <a href="{{ route('custumers.index') }}" class="text-white nav-link">Clientes</a>
                                @endcan
                                </div>
                            </div>
                            @endcan
                        </li>
                        <li class="nav-item">
                            @can('system')
                            <div class="dropdown">
                                <button class="dropbtn">Mantenimientos</button>
                                <div class="dropdown-content">
                                    @can('clase.index')
                                        <a href="{{ route('clase.index') }}" class="text-white nav-link">Clases</a>
                                    @endcan
                                    @can('brands.index')
                                        <a href="{{ route('brands.index') }}" class="text-white nav-link">Marcas</a>
                                    @endcan
                                    @can('modelos.index')
                                        <a href="{{ route('modelos.index') }}" class="text-white nav-link">Modelos</a>
                                    @endcan
                                    @can('modelos.index')
                                        <a href="{{ route('airplanes.index') }}" class="text-white nav-link">Aeroplanos</a>
                                    @endcan
                                    @can('modelos.index')
                                        <a href="{{ route('fleets.index') }}" class="text-white nav-link">Flotas</a>
                                    @endcan
                                </div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <div class="dropdown">
                                <button class="dropbtn">Gestion de Destinos</button>
                                <div class="dropdown-content">
                                    @can('city_countrys.index')
                                        <a href="{{ route('city_countrys.index') }}" class="text-white nav-link">Asignacion Ciudades-Pais</a>
                                    @endcan
                                </div>
                            </div>
                                @endcan
                        </li>
                        @can('aerolineas.index')
                            <li class="nav-item">
                               <a href="{{ url('Gestion-Aerolineas') }}" class="text-white nav-link">Gestion de aerolineas</a>
                            </li>
                         @endcan
                            @can('mi-aerolinea.index')
                                <li class="nav-item">
                                    <a href="{{ url('mi-aerolinea') }}" class="text-white nav-link">Gestion de vuelos</a>
                                </li>
                            @endcan

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link text-white font-weight-bold" href="{{ route('login') }}">{{ __('Iniciar Sesion') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link text-white font-weight-bold" href="{{ route('register') }}">{{ __('Registrarse') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="fa fa-user"></i>  {{Auth::user()->name }}<br>
                                    <i class="fa fa-clock"> {{(\Carbon\Carbon::now('America/El_Salvador'))->isoFormat('D MMMM YYYY, h:mm:ss a')}}</i><span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out"></i>
                                          {{ __('Salir') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        @if (session('info'))
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-md-offset-2">
                        <div class="alert alert-success">
                            {{ session('info') }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <script type="text/javascript"> Cufon.now(); </script>
    <!-- END PAGE SOURCE -->
</body>
</html>

