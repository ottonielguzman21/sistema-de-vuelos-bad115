@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-12">
         <div class="card">
            <div class="card-header bg-primary text-white text-center"><h3>Bienvenido al sistema</h3></div>
            <div class="card-body">
               @if (session('status'))
               <div class="alert alert-success" role="alert">
                  {{ session('status') }}
               </div>
               @endif
               <div>
                  <h5>¡Acabas de Iniciar Sesion!</h5>
                  Bienvenido: {{Auth::user()->name }} {{Auth::user()->lname }}
                  <br>
               </div>
               <div>
                  <h5>Tu Correo:</h5>
                  {{Auth::user()->email }}
               </div>
                   <div>
                       <br>
                       <h4 class="h4 font-weight-bold">SISTEMA DE GESTION FLIGHT AIRLINES</h4>
                       <br>
                       <div>
                           <img src="{{URL::asset('/images/Flight.png')}}" alt="profile Pic" height="200" width="200"></img>
                       </div>
                       <br><br><br><br><br><br>
                       <div>
                           <h4>
                               <i>
                                   Calle Alberto Masferrer, Contiguo a Iglesia parroquial, Santo Tomás
                               </i>
                           </h4>
                       </div>
                       <div>
                           <h4>
                               <i>
                                   Teléfono: 2220-9942, correo eléctronico: flight_airlines@fa.com
                               </i>
                           </h4>
                       </div>
                       <br>
                   </div>
            </div>
         </div>

      </div>
   </div>
</div>
@endsection
