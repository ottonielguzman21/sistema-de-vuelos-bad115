<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\User;

Route::get('/', function () {
    return view('welcome');
});
Route::get('aircrafts', function () {
    return view('aircrafts');
});
Route::get('safety', function () {
    return view('safety');
});
Route::get('contact', function () {
    return view('contact');
});
Route::get('/nombre/{id}','UserController@nombre');
Route::get('/miaerolinea/{id}','AirlineController@obtenerPorSP');
Route::get('/aerolineas/activas','AirlineController@aerolineasActivas');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth'])->group(function (){

    //Custumers

        Route::post('custumers/store','CustumersController@store')->name('custumers.store')
            ->middleware('permission:custumers.create');

        Route::post('custumers/store_c','CustumersController@store_c')->name('custumers.store_c')
            ->middleware('permission:custumers.create_1');

        Route::get('custumers','CustumersController@index')->name('custumers.index')
            ->middleware('permission:custumers.index');

        Route::get('custumers/create','CustumersController@create')->name('custumers.create')
            ->middleware('permission:custumers.create');

        Route::get('custumers/create_1','CustumersController@create_1')->name('custumers.create_1')
            ->middleware('permission:custumers.create_1');

        Route::put('custumers/{product}','CustumersController@update')->name('custumers.update')
            ->middleware('permission:custumers.edit');

        Route::get('custumers/{product}','CustumersController@show')->name('custumers.show')
            ->middleware('permission:custumers.show');

        Route::delete('custumers/{product}','CustumersController@destroy')->name('custumers.destroy')
            ->middleware('permission:custumers.destroy');

        Route::get('custumers/{product}/edit','CustumersController@edit')->name('custumers.edit')
            ->middleware('permission:custumers.edit');

    //Brands

        Route::post('brands/store','BrandsController@store')->name('brands.store')
            ->middleware('permission:brands.create');

        Route::get('brands','BrandsController@index')->name('brands.index')
            ->middleware('permission:brands.index');

        Route::get('brands/create','BrandsController@create')->name('brands.create')
            ->middleware('permission:brands.create');

        Route::put('brands/{product}','BrandsController@update')->name('brands.update')
            ->middleware('permission:brands.edit');

        Route::get('brands/{product}','BrandsController@show')->name('brands.show')
            ->middleware('permission:brands.show');

        Route::delete('brands/{product}','BrandsController@destroy')->name('brands.destroy')
            ->middleware('permission:brands.destroy');

        Route::get('brands/{product}/edit','BrandsController@edit')->name('brands.edit')
            ->middleware('permission:brands.edit');

        //Fleets
        Route::post('fleets/store','FleetsController@store')->name('fleets.store')
            ->middleware('permission:fleets.create');

        Route::get('fleets','FleetsController@index')->name('fleets.index')
            ->middleware('permission:fleets.index');

        Route::get('fleets/create','FleetsController@create')->name('fleets.create')
            ->middleware('permission:fleets.create');

        Route::put('fleets/{product}','FleetsController@update')->name('fleets.update')
            ->middleware('permission:fleets.edit');

        Route::get('fleets/{product}','FleetsController@show')->name('fleets.show')
            ->middleware('permission:fleets.show');

        Route::delete('fleets/{product}','FleetsController@destroy')->name('fleets.destroy')
            ->middleware('permission:fleets.destroy');

        Route::get('fleets/{product}/edit','FleetsController@edit')->name('fleets.edit')
            ->middleware('permission:fleets.edit');

        //City_countrys
        Route::post('city_countrys/store','City_countrysController@store')->name('city_countrys.store')
            ->middleware('permission:city_countrys.create');

        Route::get('city_countrys','City_countrysController@index')->name('city_countrys.index')
            ->middleware('permission:city_countrys.index');

        Route::get('city_countrys/create','City_countrysController@create')->name('city_countrys.create')
            ->middleware('permission:city_countrys.create');

        Route::put('city_countrys/{product}','City_countrysController@update')->name('city_countrys.update')
            ->middleware('permission:city_countrys.edit');

        Route::get('city_countrys/{product}','City_countrysController@show')->name('city_countrys.show')
            ->middleware('permission:city_countrys.show');

        Route::delete('city_countrys/{product}','City_countrysController@destroy')->name('city_countrys.destroy')
            ->middleware('permission:city_countrys.destroy');

        Route::get('city_countrys/{product}/edit','City_countrysController@edit')->name('city_countrys.edit')->middleware('permission:city_countrys.edit');

            //Modelos
        Route::post('modelos/store','ModelosController@store')->name('modelos.store')
            ->middleware('permission:modelos.create');

        Route::get('modelos','ModelosController@index')->name('modelos.index')
            ->middleware('permission:modelos.index');

        Route::get('modelos/create','ModelosController@create')->name('modelos.create')
            ->middleware('permission:modelos.create');

        Route::put('modelos/{product}','ModelosController@update')->name('modelos.update')
            ->middleware('permission:modelos.edit');

        Route::get('modelos/{product}','ModelosController@show')->name('modelos.show')
            ->middleware('permission:modelos.show');

        Route::delete('modelos/{product}','ModelosController@destroy')->name('modelos.destroy')
            ->middleware('permission:modelos.destroy');

        Route::get('modelos/{product}/edit','ModelosController@edit')->name('modelos.edit')
            ->middleware('permission:modelos.edit');

        //Airplanes
        Route::post('airplanes/store','AirplanesController@store')->name('airplanes.store')
            ->middleware('permission:airplanes.create');

        Route::get('airplanes','AirplanesController@index')->name('airplanes.index')
            ->middleware('permission:airplanes.index');

        Route::get('airplanes/create','AirplanesController@create')->name('airplanes.create')
            ->middleware('permission:airplanes.create');

        Route::put('airplanes/{product}','AirplanesController@update')->name('airplanes.update')
            ->middleware('permission:airplanes.edit');

        Route::get('airplanes/{product}','AirplanesController@show')->name('airplanes.show')
            ->middleware('permission:airplanes.show');

        Route::delete('airplanes/{product}','AirplanesController@destroy')->name('airplanes.destroy')
            ->middleware('permission:airplanes.destroy');

        Route::get('airplanes/{product}/edit','AirplanesController@edit')->name('airplanes.edit')
            ->middleware('permission:airplanes.edit');

    //Roles
        Route::post('roles/store','RoleController@store')->name('roles.store')
                    ->middleware('permission:roles.create');

        Route::get('roles','RoleController@index')->name('roles.index')
            ->middleware('permission:roles.index');

        Route::get('roles/create','RoleController@create')->name('roles.create')
            ->middleware('permission:roles.create');

        Route::put('roles/{role}','RoleController@update')->name('roles.update')
            ->middleware('permission:roles.edit');

        Route::get('roles/{role}','RoleController@show')->name('roles.show')
            ->middleware('permission:roles.show');

        Route::delete('roles/{role}','RoleController@destroy')->name('roles.destroy')
            ->middleware('permission:roles.destroy');

        Route::get('roles/{role}/edit','RoleController@edit')->name('roles.edit')
            ->middleware('permission:roles.edit');

    //Productos
        Route::post('products/store','ProductController@store')->name('products.store')
            ->middleware('permission:products.create');

        Route::get('products','ProductController@index')->name('products.index')
            ->middleware('permission:products.index');

        Route::get('products/create','ProductController@create')->name('products.create')
            ->middleware('permission:products.create');

        Route::put('products/{product}','ProductController@update')->name('products.update')
            ->middleware('permission:products.edit');

        Route::get('products/{product}','ProductController@show')->name('products.show')
            ->middleware('permission:products.show');

        Route::delete('products/{product}','ProductController@destroy')->name('products.destroy')
            ->middleware('permission:products.destroy');

        Route::get('products/{product}/edit','ProductController@edit')->name('products.edit')
            ->middleware('permission:products.edit');

        //Usuarios
        Route::get('users','UserController@index')->name('users.index')
            ->middleware('permission:users.index');

        Route::put('users/{product}','UserController@update')->name('users.update')
            ->middleware('permission:users.edit');

        Route::get('users/{user}','UserController@show')->name('users.show')
            ->middleware('permission:users.show');

        Route::delete('users/{user}','UserController@destroy')->name('users.destroy')
            ->middleware('permission:users.destroy');

        Route::get('users/{user}/edit','UserController@edit')->name('users.edit')
            ->middleware('permission:users.edit');

        //Empresa
        Route::get('empresas','EmpresaController@index')->name('empresa.index')
            ->middleware('permission:empresa.index');

        //Gestion de permisos
        Route::post('permissions/store','PermissionController@store')->name('permission.store')
            ->middleware('permission:permission.create');

        Route::get('permissions','PermissionController@index')->name('permission.index')
            ->middleware('permission:permission.index');

        Route::get('permissions/create','PermissionController@create')->name('permission.create')
            ->middleware('permission:permission.create');

        Route::put('permissions/{permission}','PermissionController@update')->name('permission.update')
            ->middleware('permission:permission.edit');

        Route::get('permissions/{permission}','PermissionController@show')->name('permission.show')
            ->middleware('permission:permission.show');

        Route::delete('permissions/{permission}','PermissionController@destroy')->name('permission.destroy')
            ->middleware('permission:permission.destroy');

        Route::get('permissions/{permission}/edit','PermissionController@edit')->name('permission.edit')
            ->middleware('permission:permission.edit');

        //Classes
        Route::get('clases','ClassesController@index')->name('clase.index')
            ->middleware('permission:clases.index');

        Route::get('clases/create','ClassesController@create')->name('clase.create')
            ->middleware('permission:clases.create');

        Route::get('clases/{clase}/edit','ClassesController@edit')->name('clase.edit')
            ->middleware('permission:clases.edit');

        Route::put('clases/{clase}','ClassesController@update')->name('clase.update')
            ->middleware('permission:clases.edit');

        Route::get('clases/{clase}','ClassesController@show')->name('clase.show')
            ->middleware('permission:clases.show');

        Route::delete('clases/{clase}','ClassesController@destroy')->name('clase.destroy')
            ->middleware('permission:clases.destroy');

        Route::post('clases/store','ClassesController@store')->name('clase.store')
            ->middleware('permission:clases.create');

        //rutas destinos
        Route::get('destinos','DestinoController@index')->name('destino.index')
            ->middleware('permission:destino.index');

       Route::post('destinos/store','DestinoController@store')->name('destino.store')
            ->middleware('permission:destino.create');

        Route::get('destinos/create','DestinoController@create')->name('destino.create')
            ->middleware('permission:destino.create');

        Route::put('destinos/{destino}','DestinoController@update')->name('destino.update')
            ->middleware('permission:destino.edit');

        Route::get('destinos/{destino}','DestinoController@show')->name('destino.show')
            ->middleware('permission:destino.show');

        Route::delete('destinos/{destino}','DestinoController@destroy')->name('destino.destroy')
            ->middleware('permission:destino.destroy');

        Route::get('destinos/{destino}/edit','DestinoController@edit')->name('destino.edit')
            ->middleware('permission:destino.edit');

        //Gestion de Aerolineas
        Route::get('/Gestion-Aerolineas', function () {
            return view('aerolineas.index');
        });
        Route::resource('/aerolineas', 'AirlineController', ['except'=>'show','create','edit'])
            ->middleware('permission:aerolineas.index');
        //Usuario: Administrador de aerolinea
        Route::resource('/mi-aerolinea', 'miAerolineaController', ['except'=>'show','create','edit'])
            ->middleware('permission:mi-aerolinea.index');

        //ruta de prueba
        Route::get('/funcion','AirlineController@ver');

        Route::get('/vuelos','miAerolineaController@vuelos')
            ->middleware('permission:mi-aerolinea.index');
        Route::get('/paises','miAerolineaController@paises')
        ->middleware('permission:mi-aerolinea.index');
        Route::get('/aeropuertosDisponibles', 'AirlineController@aeropuertos')
        ->middleware('permission:mi-aerolinea.index');
        Route::post('/mi-aerolinea/crearVuelo', 'miAerolineaController@crearVuelo')
            ->middleware('permission:mi-aerolinea.index');
        Route::get('/vuelo/{idVuelo}/{idEstado}', 'miAerolineaController@darBajaVuelo')
            ->middleware('permission:mi-aerolinea.index');
        Route::get('/detalleVuelo/{idVuelo}', 'miAerolineaController@detalleVuelo')
            ->middleware('permission:mi-aerolinea.index');
        Route::post('/actualizarVuelo', 'miAerolineaController@actualizarVuelo')
             ->middleware('permission:mi-aerolinea.index');
        Route::get('/escalas/{idVuelo}/{idAerolinea}', 'miAerolineaController@vuelosParaEscala')
            ->middleware('permission:mi-aerolinea.index');
        Route::post('/asignarEscala', 'miAerolineaController@asignarEscala')
            ->middleware('permission:mi-aerolinea.index');
});
