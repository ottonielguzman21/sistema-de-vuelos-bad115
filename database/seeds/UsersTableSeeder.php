<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Role;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        factory(App\User::class,20)->create();
        Role::create([
            'id'=>'1',
            'name'=> 'Admin',
            'slug'=> 'admin',
            'description'=>'Este rol posee todos los permisos en el sistema, unicamente si cuenta con all-access',
            'special'=> 'all-access'
        ]);
        \App\User::create([  //Aqui estoy haciendo mi user
            'name'=>'Edwin Otoniel',
            'lname'=>'Guzman',
            'email'=> 'gm12006@ues.edu.sv', //Password:fantasma
            'password'=> '$2y$10$CozKB4JPQeGyw.EVW3QOvO9pWuviS68cdElLLnV86kd0XDHuHXy6y' //Esta contraseña esta encriptada en AES 256 BCD
        ]);
        \App\RolUser::create([  //Aqui me estoy dando todos los privilegios: all-access
            'role_id'=>'1',
            'user_id'=> '21'
        ]);
    }
}
