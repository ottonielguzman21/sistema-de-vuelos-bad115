<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Users
        Permission::create([
            'name'  => 'Navegar usuarios',
            'slug'  =>  'users.index',
            'description' => 'Lista y navega todos los usuarios del sistema',
        ]);
        Permission::create([
            'name'  => 'Crear usuario',
            'slug'  =>  'users.create',
            'description' => 'Permite crear usuarios en el sistema',
        ]);
        Permission::create([
            'name'  => 'Ver detalle de usuarios',
            'slug'  =>  'users.show',
            'description' => 'Ver en detalle cada rol en el sistema',
        ]);
        Permission::create([
            'name'  => 'Edicion de usuarios',
            'slug'  =>  'users.edit',
            'description' => 'Editar Cualquier dato de un usuario del sistema',
        ]);
        Permission::create([
            'name'  => 'Eliminar Usuario',
            'slug'  =>  'users.destroy',
            'description' => 'Eliminar cualquier usuario del sistema',
        ]);

        //Role
        Permission::create([
            'name'  => 'Navegar roles',
            'slug'  =>  'roles.index',
            'description' => 'Lista y navega todos los roles del sistema',
        ]);
        Permission::create([
            'name'  => 'Crear rol',
            'slug'  =>  'roles.create',
            'description' => 'Permite crear roles en el sistema',
        ]);
        Permission::create([
            'name'  => 'Ver detalle de los roles',
            'slug'  =>  'roles.show',
            'description' => 'Ver en detalle cada rol en el sistema',
        ]);
        Permission::create([
            'name'  => 'Edicion de roles',
            'slug'  =>  'roles.edit',
            'description' => 'Editar Cualquier dato de un rol del sistema',
        ]);
        Permission::create([
            'name'  => 'Eliminar rol',
            'slug'  =>  'roles.destroy',
            'description' => 'Eliminar cualquier rol del sistema',
        ]);

        //Products
        Permission::create([
            'name'  => 'Navegar productos',
            'slug'  =>  'products.index',
            'description' => 'Permite crear produtos en el sistema',
        ]);
        Permission::create([
            'name'  => 'Crear producto',
            'slug'  =>  'products.create',
            'description' => 'Lista y navega todos los productos del sistema',
        ]);
        Permission::create([
            'name'  => 'Ver detalle de los productos',
            'slug'  =>  'products.show',
            'description' => 'Ver en detalle cada productos en el sistema',
        ]);
        Permission::create([
            'name'  => 'Edicion de productos',
            'slug'  =>  'products.edit',
            'description' => 'Editar Cualquier dato de un productos',
        ]);
        Permission::create([
            'name'  => 'Eliminar producto',
            'slug'  =>  'products.destroy',
            'description' => 'Eliminar cualquier producto del sistema',
        ]);
        //Permisos para empresa
        Permission::create([
            'name'  => 'Navegar por la vista empresa',
            'slug'  =>  'empresa.index',
            'description' => 'sirve para consultar la empresa',
        ]);

        //Gestion de Permisos
        Permission::create([
            'name'  => 'Navegar permisos',
            'slug'  =>  'permission.index',
            'description' => 'Permite crear permisos en el sistema',
        ]);
        Permission::create([
            'name'  => 'Crear permisos',
            'slug'  =>  'permission.create',
            'description' => 'permite la creacion de un permiso',
        ]);
        Permission::create([
            'name'  => 'Ver detalle de los permisos',
            'slug'  =>  'permission.show',
            'description' => 'Ver en detalle cada permiso en el sistema',
        ]);
        Permission::create([
            'name'  => 'Edicion de permiso',
            'slug'  =>  'permission.edit',
            'description' => 'Editar Cualquier dato de un permiso',
        ]);
        Permission::create([
            'name'  => 'Eliminar permiso',
            'slug'  =>  'permission.destroy',
            'description' => 'Eliminar cualquier permiso del sistema',
        ]);

        //Permisos para clase
        Permission::create([
            'name'  => 'Navegar por la vista Clase',
            'slug'  =>  'clases.index',
            'description' => 'sirve para consultar las clases',
        ]);

        Permission::create([
            'name'  => 'Crear Clase',
            'slug'  =>  'clases.create',
            'description' => 'Crea una clase',
        ]);
        Permission::create([
            'name'  => 'Ver detalle de la clase',
            'slug'  =>  'clases.show',
            'description' => 'Ver en detalle cada clase',
        ]);
        Permission::create([
            'name'  => 'Edicion de clase',
            'slug'  =>  'clases.edit',
            'description' => 'Editar Cualquier dato de un clase',
        ]);
        Permission::create([
            'name'  => 'Eliminar clase',
            'slug'  =>  'clases.destroy',
            'description' => 'Eliminar cualquier clase del sistema',
        ]);

        //Permisos para destinos
        Permission::create([
            'name'  => 'Navegar por la vista Destino',
            'slug'  =>  'destino.index',
            'description' => 'sirve para consultar los destinos',
        ]);

        Permission::create([
            'name'  => 'Crear Destino',
            'slug'  =>  'destino.create',
            'description' => 'Crea un destino',
        ]);
        Permission::create([
            'name'  => 'Ver detalle del destino',
            'slug'  =>  'destino.show',
            'description' => 'Ver en detalle cada clase',
        ]);
        Permission::create([
            'name'  => 'Edicion de destino',
            'slug'  =>  'destino.edit',
            'description' => 'Editar Cualquier dato de un clase',
        ]);
        Permission::create([
            'name'  => 'Eliminar destino',
            'slug'  =>  'destino.destroy',
            'description' => 'Eliminar cualquier clase del destino',
        ]);

                //Permisos para Marcas
        Permission::create([
            'name'  => 'Navegar por la vista Marcas',
            'slug'  =>  'brands.index',
            'description' => 'sirve para consultar las Marcas',
        ]);

        Permission::create([
            'name'  => 'Crear Marca',
            'slug'  =>  'brands.create',
            'description' => 'Crea una Marca',
        ]);
        Permission::create([
            'name'  => 'Ver detalle de Marca',
            'slug'  =>  'brands.show',
            'description' => 'Ver en detalle cada Marca',
        ]);
        Permission::create([
            'name'  => 'Edicion de Marca',
            'slug'  =>  'brands.edit',
            'description' => 'Editar Cualquier dato de una Marca',
        ]);
        Permission::create([
            'name'  => 'Eliminar Marca',
            'slug'  =>  'brands.destroy',
            'description' => 'Eliminar cualquier Marca de la lista',
        ]);
    }
}
