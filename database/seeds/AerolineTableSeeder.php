<?php

use Illuminate\Database\Seeder;
use Caffeinated\Shinobi\Models\Permission;

class AerolineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::create([
            'name'  => 'Gestion de Aerolineas',
            'slug'  =>  'aerolineas.index',
            'description' => 'Permite Gestionar las aerolineas del sistema',
        ]);
    }
}
