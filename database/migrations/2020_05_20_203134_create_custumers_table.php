<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustumersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custumers', function (Blueprint $table) {
            $table->increments('id');//ID_CLIENTE
            $table->integer('user_id')->unsigned();//USER_ID
            $table->string('tipo_documento', 25)->nullable(false);//TIPO_DOCUMENTO
            $table->integer('tipo_cliente')->unsigned()->nullable(false);//TIPO_CLIENTE
            $table->string('numero_viajero_frecuente',25)->nullable(false);//NUMERO_VIAJERO_FRECUENTE
            $table->string('direccion', 30)->nullable(true);//DIRECCION
            $table->string('telefono_fijo', 20)->nullable(true);//TELEFONO_FIJO
            $table->boolean('estado')->default(true);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custumers');
    }
}
