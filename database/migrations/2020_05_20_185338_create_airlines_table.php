<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirlinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airlines', function (Blueprint $table) {
            $table->increments('id');//ID_AEROLINEA

            $table->integer('user_id')->unsigned();

            $table->string('nombre_oficial',25)->nullable(false);//NOMBRE_OFICIAL
            $table->string('pais_origen', 25)->nullable(false);//PAIS_ORIGEN
            $table->string('nombre_corto',15)->nullable(true);//NOMBRE_CORTO
            $table->string('correo_electronico',25)->nullable(false);//CORREO_ELECTRONICO
            $table->date('fecha_fundacion',25)->nullable(false);//FECHA_FUNDACION
            $table->string('twitter',25)->nullable(true);//TWITTER
            $table->string('facebook',25)->nullable(true);//FACEBOOK
            $table->unsignedInteger('airplane_id');//ID_AVION
            $table->string('sitio_web',25)->nullable(true);//SITIO_WEB
            $table->string('cod_aeroline',5)->nullable(false);//COD_AEROLINEA
            $table->string('url_imagen',30)->nullable(false);//URL_IMAGEN
            $table->boolean('estado')->default(true);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airlines');
    }
}
