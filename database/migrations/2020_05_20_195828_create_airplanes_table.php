<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirplanesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airplanes', function (Blueprint $table) {
            $table->increments('id');//ID_AVION
            $table->unsignedInteger('modelo_id');//ID_MODELO
            $table->string('nombre_aeroplano',25)->nullable(false);//TIPO_AVION
            $table->integer('capacidad')->unsigned()->nullable(false);//CAPACIDAD
            $table->string('tipo_avion',25)->nullable(false);//TIPO_AVION
            $table->timestamps();
            $table->boolean('estado')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airplanes');
    }
}
