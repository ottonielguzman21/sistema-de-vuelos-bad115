<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservationHistories', function (Blueprint $table) {
            $table->increments('id');//ID_HISTORIALRESERVA
            $table->unsignedInteger('reservation_id');//ID_RESERVA
            $table->unsignedInteger('custumer_id');//ID_RESERVA
            $table->integer('estado_pago')->unsigned()->nullable(false);//ESTADO_PAGO
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservationHistories');
    }
}
