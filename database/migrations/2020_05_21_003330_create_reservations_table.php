<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('payment_id');//ID_PAGO
            $table->unsignedInteger('flight_id');//COD_VUELO
            $table->unsignedInteger('custumer_id');//ID_CLIENTE
            $table->timestamps();

            $table->foreign('payment_id')->references('id')->on('payments');
            $table->foreign('custumer_id')->references('id')->on('custumers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
