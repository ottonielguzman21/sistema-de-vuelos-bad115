<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airports', function (Blueprint $table) {
            $table->increments('id');//ID_AEROPUERTO
            $table->string('nombre_aeropuerto',25)->nullable(true);//NOMBRE_AEROPUERTO
            $table->string('responsable',25)->nullable(false);//RESPONSABLE
            $table->integer('estaciones')->unsigned()->nullable(false);//ESTACIONES
            $table->string('telefono',25)->nullable(false);//TELEFONO
            $table->boolean('estado')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airports');
    }
}
