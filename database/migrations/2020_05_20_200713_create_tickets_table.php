<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('reservation_id');//ID_RESERVA
            $table->boolean('estado_pago')->default(false);//ESTADO_PAGO
            $table->unsignedInteger('passenger_id');//ID_PASAJERO
            $table->string('tipo_boleto',15)->nullable(false);//TIPO_BOLETO
            $table->string('serial',15)->unique()->nullable(false);//SERIAL
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
