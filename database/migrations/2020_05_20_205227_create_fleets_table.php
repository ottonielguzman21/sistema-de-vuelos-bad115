<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFleetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleets', function (Blueprint $table) {
            $table->increments('id');//ID_FLOTA
            $table->unsignedInteger('airplane_id');//ID_AVION
            $table->unsignedInteger('airline_id');//ID_AEROLINEA
            $table->timestamps();

            $table->foreign('airplane_id')->references('id')->on('airplanes');
            $table->foreign('airline_id')->references('id')->on('airlines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fleets');
    }
}
