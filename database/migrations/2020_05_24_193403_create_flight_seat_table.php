<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightSeatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flight_seat', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('flight_id');//ID_CIUDAD
            $table->unsignedInteger('seat_id');//ID_PAIS
            $table->boolean('reservado')->default(false);
            $table->timestamps();

            $table->foreign('flight_id')->references('id')->on('flights');
            $table->foreign('seat_id')->references('id')->on('seats');
        });
        Schema::table('seats', function (Blueprint $table) {
            $table->string('fila',4)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flight_seat');
    }
}
