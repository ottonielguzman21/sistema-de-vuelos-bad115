<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassengersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passengers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('primer_nombre', 15)->nullable(false);//PRIMER_NOMBRE
            $table->string('segundo_nombre', 15)->nullable(false);//SEGUNDO_NOMBRE
            $table->string('primer_apellido', 15)->nullable(false);//PRIMER_APELLIDO
            $table->string('segundo_apellido', 15)->nullable(false);//SEGUNDO_APELLIDO
            $table->char('genero',1)->nullable(false);//GENERO
            $table->string('numero_telefono', 20)->nullable(true);//NUMERO_TELEFONO
            $table->string('pasaporte', 15)->nullable(false);//PASAPORTE
            $table->date('fecha_nacimiento')->nullable(false);//FECHA_NACIMIENTO
            $table->string('nacionalidad', 15)->nullable(false);//NACIONALIDAD
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passengers');
    }
}
