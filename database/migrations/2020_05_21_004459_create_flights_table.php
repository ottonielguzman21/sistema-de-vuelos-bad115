<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cod_vuelo',6);
            $table->unsignedInteger('airplane_id');//ID_AVION
            $table->unsignedInteger('airport_origin_id');//ID_AEROPUERTO_ORIGEN
            $table->unsignedInteger('airport_destination_id');//ID_AEROPUERTO_DESTINO
            $table->unsignedInteger('flight_scale');//UN VUELO TIENE POR ESCALA OTRO VUELO
            $table->unsignedInteger('seat_id');//ID_ASIENTO
            $table->unsignedInteger('rate_id');//ID_TARIFA

            $table->integer('millas_distancia')->unsigned()->nullable(false);//MILLAS
            $table->dateTime('fecha_ida');
            $table->dateTime('fecha_regreso');
            $table->decimal('precio_escala',5,2)->unsigned()->nullable(true);//PRECIO_ESCALA
            $table->integer('duracion_horas')->unsigned()->nullable(false);//DURACION_HORAS
            $table->integer('duracion_dias')->unsigned()->nullable(false);//DURACION_DIAS

            $table->foreign('airplane_id')->references('id')->on('airplanes');
            $table->foreign('airport_origin_id')->references('id')->on('airports');
            $table->foreign('airport_destination_id')->references('id')->on('airports');
            $table->foreign('flight_scale')->references('id')->on('flights');
            $table->foreign('seat_id')->references('id')->on('seats');
            $table->boolean('estado')->default(true);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flights');
    }
}
