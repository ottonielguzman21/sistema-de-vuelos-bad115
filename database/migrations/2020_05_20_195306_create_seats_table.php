<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seats', function (Blueprint $table) {//ASIENTOS
            $table->increments('id');//ID_ASIENTO
            $table->string('fila',2)->nullable(false);//FILA
            $table->string('letra', 2)->nullable(false);//LETRA
            $table->boolean('reservado')->default(false);//RESERVADO
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seats');
    }
}
