<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConstraintOnReservationHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //reservationHistories
        Schema::table('reservationHistories', function (Blueprint $table) {
            $table->foreign('reservation_id')->references('id')->on('reservations');
            $table->foreign('custumer_id')->references('id')->on('custumers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
