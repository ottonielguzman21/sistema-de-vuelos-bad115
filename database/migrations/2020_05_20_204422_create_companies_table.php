<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');//ID_EMPRESA
               $table->unsignedInteger('custumer_id');//ID_CLIENTE
               $table->string('nombre_empresa', 25)->nullable(false);//NOMBRE_EMPRESA
               $table->string('nombre_contacto', 25)->nullable(false);//NOMBRE_CONTACTO
               $table->string('NIT', 25)->unique()->nullable(false);//NIT
               $table->string('NIC', 25)->unique()->nullable(false);//NIC
               //$table->primary(array('id','empresa_id','NIT','NIC'));//CLAVE COMPUESTA::ADMITIDA EN ELOQUENT PERO NO POR MARIADB
            $table->timestamps();
            $table->foreign('custumer_id')->references('id')->on('custumers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
