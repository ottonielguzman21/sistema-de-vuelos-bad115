<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirportCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airport_country', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('airport_id');//ID_AEROLINEA
            $table->unsignedInteger('country_id');//ID_PAIS
            $table->timestamps();

            $table->foreign('airport_id')->references('id')->on('airports');
            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airport_country');
    }
}
