<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modelos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('brand_id');//ID_MARCA
            $table->string('nombre_modelo', 25)->nullable(false);//NOMBRE_MODELO
            $table->decimal('envergadura', 5,2)->unsigned()->nullable(true);//ENVERGADURA
            $table->decimal('longitud', 5, 2)->unsigned()->nullable(false);//LONGITUD
            $table->integer('year')->unsigned()->nullable(true);//YEAR
            $table->integer('peso_vacio')->unsigned()->nullable(true);//PESO_VACIO
            $table->integer('peso_max_despegue')->unsigned()->nullable(true);//PESO_MAXDESPEGUE
            $table->integer('carga_util')->unsigned()->nullable(true);//CARGA_UTIL
            $table->decimal('factor_carga', 5,2)->unsigned()->nullable(true);//FACTOR_CARGA
            $table->integer('velocidad_maxima')->unsigned()->nullable(false);//VELOCIDAD_MAXIMA
            $table->string('motor', 25)->nullable(true);//MOTOR
            $table->string('autonomia', 25)->nullable(true);//AUTONOMIA
            $table->timestamps();

            $table->foreign('brand_id')->references('id')->on('brands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modelos');
    }
}
