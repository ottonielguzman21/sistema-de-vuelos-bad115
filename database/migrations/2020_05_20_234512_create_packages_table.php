<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('airline_id');//ID_AEROLINEA
            $table->string('nombre_paquete', 20)->nullable(false);//NOMBRE_CLASE
            $table->decimal('porcentaje_descuento',2,2)->unsigned()->nullable(false);//PORCENTAJE_DESCUENTO
            $table->unsignedInteger('country_id');//ID_PAIS
            $table->unsignedInteger('city_id');//ID_CIUDAD
            $table->integer('cantidad_pasajeros')->unsigned()->nullable(false);//CANTIDAD_PASAJEROS
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('city_id')->references('id')->on('cities');
            $table->foreign('airline_id')->references('id')->on('airlines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
