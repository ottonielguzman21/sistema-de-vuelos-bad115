<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('custumer_id');
            $table->string('numero_documento',20)->nullable(true);//NUMERO_DOCUMENTO
            $table->string('genero',15)->nullable(true);//GENERO
            $table->string('estado_civil',15)->nullable(true);//ESTADO_CIVIL
            $table->string('telefono_movil',15)->nullable(true);//TELEFONO_MOVIL
            $table->timestamps();
            $table->foreign('custumer_id')->references('id')->on('custumers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
