<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAirlineFlightTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airline_flight', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('flight_id');//COD_VUELO
            $table->unsignedInteger('airline_id');//ID_AEROLINEA
            $table->timestamps();

            $table->foreign('airline_id')->references('id')->on('airlines');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airline_flight');
    }
}
