<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable=['nombre_marca','fabricante','estado'];


    public function scopeNombre_marca($query, $nombre_marca)
	{
		return $query->where('nombre_marca', 'LIKE', "%$nombre_marca%");
	}

	public function modelo()
    {
        return $this->belongsTo('App\Modelo', 'id');
    }

}
