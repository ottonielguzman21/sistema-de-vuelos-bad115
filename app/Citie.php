<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citie extends Model
{
    protected $fillable=['id','nombre_ciudad'];

    public function scopeNombre($query, $nombre_ciudad)
	{
		return $query->where('nombre_ciudad', 'LIKE', "%$nombre_ciudad%");
	}
}
