<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fleet extends Model
{
    protected $fillable = ['id', 'airplane_id', 'airline_id'];
    protected $dates = ['created_at','updated_at'];

    public function scopeAerolinea($query, $airline_id)
    {
        return $query->where('airline_id', 'LIKE', "%$airline_id%");
    }

    public function airplane(){
    return $this->belongsTo('App\Airplane', 'airplane_id');
 	}

 	public function airline(){
    return $this->belongsTo('App\Airline', 'airline_id');
 	}
}
