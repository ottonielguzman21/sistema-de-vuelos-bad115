<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Modelo;
use App\Brand;

class ModelosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $nombr =$request->get('id');
        $mode = Modelo::with('brand')->get();
        $brand = Brand::orderBy('id','DESC')->Nombre_marca($nombr)->paginate(5);
        $nombre =$request->get('nombre_modelo');
        $modelos= Modelo::orderBy('id','DESC')->nombre($nombre)->paginate(5);
        return view('modelos.index',compact('modelos'));  
          }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::all();
        return view('modelos.create', compact('brands'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
          'nombre_modelo'=>'required|alpha_num_spaces',
          'brand_id'=>'required|numeric',
          'envergadura'=>'required|numeric',
          'longitud'=>'required|numeric',
          'year'=>'required|numeric',
          'peso_vacio'=>'required|integer',
          'peso_max_despegue'=>'required|integer',
          'carga_util'=>'required|integer',
          'factor_carga'=>'required|numeric',
          'velocidad_maxima'=>'required|integer',
          'motor'=>'required|alpha_spaces',
          'autonomia'=>'required|alpha_spaces',
        ]);

        $model = Modelo::where('nombre_modelo', $request->nombre_modelo)
        ->where('envergadura', $request->envergadura)
        ->where('longitud', $request->longitud)
        ->where('year', $request->year)
        ->where('peso_vacio', $request->peso_vacio)
        ->where('peso_max_despegue', $request->peso_max_despegue)
        ->where('carga_util', $request->carga_util)
        ->where('factor_carga', $request->factor_carga)
        ->where('velocidad_maxima', $request->velocidad_maxima)
        ->where('motor', $request->motor)
        ->where('autonomia', $request->autonomia)
        ->exists(); //true or false
        if($model)
        {
          return redirect()->back()
          ->with('error','¡ERROR!. El Modelo ya se encuentra en el sistema, ¡Registro repetido!');
        }

        $model = Modelo::create($request->all());
        return redirect()->route('modelos.index')->with('info', '¡Modelo guardado con éxito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modelos = Modelo::find($id);
         return view('modelos.show', compact('modelos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $brands = Brand::all();
         $modelo = Modelo::find($id);
         return view('modelos.edit', compact('brands','modelo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
          'nombre_modelo'=>'required|alpha_num_spaces',
          'brand_id'=>'required|numeric',
          'envergadura'=>'required|numeric',
          'longitud'=>'required|numeric',
          'year'=>'required|numeric',
          'peso_vacio'=>'required|integer',
          'peso_max_despegue'=>'required|integer',
          'carga_util'=>'required|integer',
          'factor_carga'=>'required|numeric',
          'velocidad_maxima'=>'required|integer',
          'motor'=>'required|alpha_spaces',
          'autonomia'=>'required|alpha_spaces',
        ]);

        $model = Modelo::where('nombre_modelo', $request->nombre_modelo)
        ->where('envergadura', $request->envergadura)
        ->where('longitud', $request->longitud)
        ->where('year', $request->year)
        ->where('peso_vacio', $request->peso_vacio)
        ->where('peso_max_despegue', $request->peso_max_despegue)
        ->where('carga_util', $request->carga_util)
        ->where('factor_carga', $request->factor_carga)
        ->where('velocidad_maxima', $request->velocidad_maxima)
        ->where('motor', $request->motor)
        ->where('autonomia', $request->autonomia)
        ->exists(); //true or false
        if($model)
        {
          return redirect()->back()
          ->with('error','¡ERROR!. El Modelo ya se encuentra en el sistema, ¡Registro repetido!');
        }

        $model = Modelo::find($id)->update($request->all());
        return redirect()->route('modelos.index')->with('info', '¡Modelo Actualizado con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
        Modelo::find($id)->delete();
        return redirect()->route('modelos.index')->with('info','Modelo eliminado con exito');
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->route('modelos.index')->with('error','¡ERROR! El modelo ha sido asignada a un Avion, no se puede borrar!! Borre la asignacion Marca a Avion e intentelo de nuevo.');
        }
    }
}
