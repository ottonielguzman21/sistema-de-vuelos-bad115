<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Brand;

class BrandsController extends Controller
{

    public function index(Request $request)
    {
        $nombre =$request->get('nombre_marca');
        $brands= Brand::orderBy('id','DESC')->nombre_marca($nombre)->paginate(5);
        return view('brands.index',compact('brands'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
          'nombre_marca'=>'required|alpha_spaces',
          'fabricante'=>'required|alpha_spaces',
        ]);

        $brand = Brand::where('nombre_marca', $request->nombre_marca)
        ->where('fabricante', $request->fabricante)
        ->exists(); //true or false
        if($brand)
        {
          return redirect()->back()
          ->with('error','¡ERROR!. La marca ya se encuentra en el sistema, ¡Registro repetido!');
        }
        $brand = Brand::create($request->all());
        return redirect()->route('brands.index')->with('info', 'Marca guardada con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = Brand::find($id);
        return view('brands.show',compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $brand = Brand::find($id);
         return view('brands.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
          'nombre_marca'=>'required|alpha_spaces',
          'fabricante'=>'required|alpha_spaces',
          'estado'=>'required|numeric',
        ]);
         $brand = Brand::where('nombre_marca', $request->nombre_marca)
        ->where('fabricante', $request->fabricante)
        ->where('estado', $request->estado)
        ->exists(); //true or false
        if($brand)
        {
          return redirect()->back()
          ->with('error','¡ERROR! No se pudo actualizar, la Marca ya se encuentra en el sistema, ¡Registro repetido!');
        }
        Brand::find($id)->update($request->all());
        return redirect()->route('brands.index')->with('info', 'Marca Actualizada con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
        Brand::find($id)->delete();
        return redirect()->route('brands.index')->with('info','Marca eliminada con exito');
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->route('brands.index')->with('error','¡ERROR! La marca ha sido asignada a un Avion, no se puede borrar!! Borre la asignacion Marca a Avion e intentelo de nuevo.');
        }
    }
}
