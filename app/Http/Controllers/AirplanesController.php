<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Airplane;
use App\Modelo;

class AirplanesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tipo =$request->get('tipo_avion');
        $airplanes= Airplane::orderBy('id','DESC')->tipo($tipo)->paginate(5);
        return view('airplanes.index',compact('airplanes'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modelos = Modelo::all();
        return view('airplanes.create', compact('modelos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
          'modelo_id'=>'required|integer',
          'nombre_aeroplano'=>'required|alpha_spaces',
          'capacidad'=>'required|integer',
          'tipo_avion'=>'required|alpha_spaces',
        ]);

        $airplan = Airplane::where('modelo_id', $request->modelo_id)
        ->where('nombre_aeroplano', $request->nombre_aeroplano)
        ->where('capacidad', $request->capacidad)
        ->where('tipo_avion', $request->tipo_avion)
        ->exists(); //true or false
        $aero = Airplane::where('nombre_aeroplano', $request->nombre_aeroplano)
        ->exists(); //true or false
        if($airplan)
        {
          return redirect()->back()
          ->with('error','¡ERROR!. El Aeroplano ya se encuentra en el sistema, ¡Registro repetido!');
        }elseif ($aero) {
            return redirect()->back()
          ->with('error','¡ERROR!. El Nombre para el Aeroplano ya se encuentra en el sistema, ¡El Nombre debe ser Único!');
        }
        Airplane::create($request->all());
        return redirect()->route('airplanes.index')->with('info', 'Aeroplano guardado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $airplane = Airplane::find($id);
        return view('airplanes.show',compact('airplane'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $airplane = Airplane::find($id);
        $modelos = Modelo::all();
        return view('airplanes.edit',compact('modelos','airplane'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
          'modelo_id'=>'required|integer',
          'nombre_aeroplano'=>'required|alpha_spaces',
          'capacidad'=>'required|integer',
          'tipo_avion'=>'required|alpha_spaces',
          'estado'=>'required|integer',
        ]);
        Airplane::find($id)->update($request->all());
        return redirect()->route('airplanes.index')->with('info', 'Aeroplano Actualizado con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
        Airplane::find($id)->delete();
        return redirect()->route('airplanes.index')->with('info','Aeroplano eliminado con exito');
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->route('airplanes.index')->with('error','¡ERROR! El Aeroplano, no se puede borrar! Borre la asignacion a Flota e Intentelo de Nuevo.');
        }
    }
}
