<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Flight;

class DestinoController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  $destinos = Destinos::latest()->paginate(5);
        // return view('countries.index',compact('countries'))
        //      ->with('i', (request()->input('page', 1) - 1) * 5); */

        $destinos=DB::table('flights as f')
                    ->leftJoin('airports as aio', 'aio.id','=','f.airport_origin_id')
                    ->leftJoin('airport_country as aco','aco.airport_id','=','aio.id')
                    ->leftJoin('countries as coo' , 'coo.id','=','aco.country_id')
                    ->leftJoin('city_country as cco' ,'cco.country_id','=','coo.id')
                    ->leftJoin('cities as co', 'co.id','=','cco.city_id')
                    ->leftJoin('airports as aid','aid.id','=','f.airport_destination_id')
                    ->leftJoin('airport_country as acd','acd.country_id','=','aid.id')
                    ->leftJoin('countries as cod','cod.id','=','acd.country_id')
                    ->leftJoin('city_country as ccd','ccd.country_id','=','cod.id')
                    ->leftJoin('cities as cd','cd.id','=','ccd.city_id')
                    ->select(DB::raw('concat(aio.nombre_aeropuerto,"-",co.nombre_ciudad,"-",coo.nombre_pais) as origen,
                    concat(aid.nombre_aeropuerto,"-",cd.nombre_ciudad,"-",cod.nombre_pais) as destino,
                    f.fecha_ida,f.fecha_regreso,f.id'))
                    ->get();



        return view('destinos.index',compact('destinos'))
                    ->with('i', (request()->input('page', 1) - 1) * 5);


    }





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $aeropuertos = self::getAeropuertos();
        $asientos    = self::getAsientosDisponibles();

        //array("origenes" => $aeropuertos);
        return view('destinos.create', $aeropuertos,$asientos);//compact('aeropuertos'))
                //->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $flight= new Flight;
        $codVuelo="AV003";//self::getCodigoVuelo();

        $fh_ida=date('Y-m-d',strtotime($request->fecha_ida));
        $fh_retorno=date('Y-m-d',strtotime($request->fecha_retorno));
        //unos datos van quemados por que no se las relciones con otra tablas por ejm aviones
        // que implica la tabla paqueetes y aerolienas no se por que ......
        $millas=200;
        $duracion_horas=3;
        $duracion_dias=1;
        $airplane=2;
        $rate_id=0;
        $flight->airplane_id=$airplane;
        $flight->cod_vuelo=$codVuelo;
        $flight->airport_origin_id=$request->airport_origin_id;
        $flight->airport_destination_id=$request->airport_destination_id;
        $flight->rate_id=$rate_id;
        $flight->seat_id=$request->seat_id;
        $flight->millas_distancia=$millas;
        $flight->fecha_ida=$fh_ida;
        $flight->fecha_regreso=$fh_retorno;
        $flight->duracion_horas=$duracion_horas;
        $flight->duracion_dias=$duracion_dias;
        $flight->save();

        return redirect()->route('destino.index')
               ->with('success','Destino creado correctmente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /* $countries = countries::find($id);
        return view('countries.show',compact('countries')); */
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /* $country = countries::find($id);
        return view('countries.edit', compact('country')); */
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
/*         $pais=countries::find($request->id);
        $pais->nombre_pais=$request->nombre_pais;
        $pais->cod_area=$request->cod_area;

        $country = $pais->update();//countries::updated($pais);

        return redirect()->route('countries.index')
            ->with('info', 'Destino guardado con éxito'); */

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\countries  $countries
     * @return \Illuminate\Http\Response
     */





    public function getAeropuertos()
    {
        $aeropuertos=DB::table('airports as aio')
        ->leftJoin('airport_country as aco','aco.airport_id','=','aio.id')
        ->leftJoin('countries as coo' , 'coo.id','=','aco.country_id')
        ->leftJoin('city_country as cco' ,'cco.country_id','=','coo.id')
        ->leftJoin('cities as co', 'co.id','=','cco.city_id')
        ->select(DB::raw('concat(aio.nombre_aeropuerto,"-",co.nombre_ciudad,"-",coo.nombre_pais) as nombre,aio.id as codigo'))
        ->get();
        return compact('aeropuertos');

    }

    public function getAsientosDisponibles()
    {

        $reservado=0;
        $asientos=DB::table('seats as s')
        ->select(DB::raw('CONCAT(s.fila,"-",s.letra) as asiento,s.id'))
        ->where('s.reservado', '=',$reservado)
        ->get();
        return compact('asientos');

    }

    public function getCodigoVuelo()
    {

    //no me funciono talves el le entiende mejor y lo arregla es para generar el codigo_vuelo tabla flight
    //query funciona pero pasarlo a laravel no le halle...
    $sql = "SELECT left(cod_vuelo,2),RIGHT(cod_vuelo,3) +1 FROM flights
        WHERE left(cod_vuelo,2)=(SELECT MAX(left(cod_vuelo,2)) FROM flights)
        AND RIGHT(cod_vuelo,3)=(SELECT MAX(right(cod_vuelo,3)) FROM flights)";

        $datos=DB::select($sql)->get();
        return compact('datos');

    }



}
