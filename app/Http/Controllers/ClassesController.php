<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Http\Request;
use App\Classes;

class ClassesController extends Controller
{
    public function index(Request $request)
    {
        $name=$request->get('nombre_clase');
        $asiento=$request->get('cantidad_asiento');
        $classes= Classes::orderBy('id','DESC')
            ->name($name)
            ->asiento($asiento)
            ->paginate(5);

        return view('clase.index',compact('classes'))
            ->with('i',(\request()->input('page',1)-1)*5);
    }

        /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clase.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clase=Classes::create($request->all());

        return redirect()->route('clase.index')
            ->with('info', 'Clase guardada con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Classes $clase)
    {
        return view('clase.show',compact('clase'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit( Classes $clase)
    {

        return view('clase.edit', compact('clase'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Classes $clase)
    {
        $request->validate([
            'nombre_clase' => 'required',
            'cantidad_asientos' => 'required',
        ]);
        $clase->update($request->all());

        return redirect()->route('clase.index')
            ->with('info', 'Clase actualizada con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Classes $clase)
    {
        $clase->delete();
        return redirect()->route('clase.index')
            ->with('info','La clase ha sido eliminado exitosamente!');
    }


}
