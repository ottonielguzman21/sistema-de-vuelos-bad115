<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Citie;
use App\Countrie;
use App\City_country;
use DB;

class City_countrysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $paices =$request->get('country_id');
        $city_countrys = City_country::orderBy('id','DESC')->pais($paices)->paginate(5);
        return view('city_countrys.index',compact('city_countrys'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = DB::table('cities')
            ->whereNotExists(function ($query)
                {
                    $query->select(DB::raw(1))
                    ->from('city_country')
                    ->whereRaw('city_country.city_id = cities.id');
                })->get();
        $countries = Countrie::all();
        return view('city_countrys.create', compact('countries','cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
          'city_id'=>'required',
          'country_id'=>'required',
        ]);

        $city_country = City_country::where('city_id', $request->city_id)->where('country_id', $request->country_id)->exists();
        if ($city_country) {
            return redirect()->back()
          ->with('error','¡ERROR!. ¡La ciudad ya a sido agregado a esa Ciudad! Registro repetido.');
        }
        City_country::create($request->all());
        return redirect()->route('city_countrys.index')->with('info', '¡Ciudad agregada a País con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $city_countrys = City_country::find($id);
        return view('city_countrys.show',compact('city_countrys'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city_countrys = City_country::find($id);
        $countrys = Countrie::all();
        $cities = Citie::all();
        return view('city_countrys.edit', compact('city_countrys','cities','countrys'));    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
          'city_id'=>'required',
          'country_id'=>'required',
        ]);
        City_country::find($id)->update($request->all());
        return redirect()->route('city_countrys.index')->with('info', '¡Asignacion Ciudad a País Actualizada con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
        City_country::find($id)->delete();
        return redirect()->route('city_countrys.index')->with('info','Asignacion Ciudad a País eliminada con exito');
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->route('city_countrys.index')->with('error','¡ERROR! La Asignacion Ciudad a País no se puede borrar!!');
        }
    }
}
