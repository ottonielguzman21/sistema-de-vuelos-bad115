<?php

namespace App\Http\Controllers;

use App\Classes;
use Illuminate\Http\Request;
use App\Airline;
use Illuminate\Support\Facades\DB;
use PhpParser\JsonDecoder;
use PhpParser\Node\Expr\Array_;
use Psy\Util\Json;

class AirlineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            return Airline::orderBy('id','DESC')
                ->get();
        }else{
            return view('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function obtenerPorSP($id){
        // Assume #id = 1;
        $model = new Airline();
        $user = $model->hydrate(
            DB::select(
                'call select_by_aerolinea_id(?)',[$id]
            )
        );
        return $user;
    }
    public function aeropuertos(Request $request){
        $aeropuertos=(DB::select('call usp_aeropuertos()'));
        return $aeropuertos;
    }
    public function aerolineasActivas(Request $request){
        if($request->ajax()){
            $aerolineas=(DB::select('call usp_aerolineas_activas()'));
            $clases= Classes::all();
            $clave= array($clases);
            return [
                    'aerolineas'=>$aerolineas,
                    'clases'=>$clases
                   ];
        }else{
            return view('home');
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'user_id' => 'required|int',
            'nombre_oficial' => 'required|string|max:25',
            'pais_origen' => 'required|string|max:25',
            'nombre_corto' => 'required|string|max:15',
            'fecha_fundacion' => 'required|date',
            'correo_electronico' => 'required|string|email|max:25',
        ]);
        $url= 'air-canada.jpg';
        DB::select('SELECT crearAerolinea(?,?,?,?,?,?,?,?,?,?)',[
              $request->user_id,
              $request->nombre_oficial,
              $request->pais_origen,
              $request->nombre_corto,
              $request->correo_electronico,
              $request->fecha_fundacion,
              $request->twitter,
              $request->facebook,
              $request->sitio_web,
              $url
        ]);
        DB::select('call usp_asignarRolAdministrador(?)',[$request->user_id]);
        $aero=Airline::get()->last();
        return compact('aero',$aero);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function ver(){
        //$array=array("35","Italian Airlines","Italia","IA","italia-airlines@it.com", "2020-05-26 19:11:19", "italian","italian", "www.it-airlines.com","air-canada.jpg");
        //$nb = collect(DB::raw('SELECT crearAerolinea(?,?,?,?,?,?,?,?,?,?) AS nb',$array));
        $sql= 'SELECT crearAerolinea(12,\'Italian Airlines\',\'Italia\',\'IA\',\'italia-airlines@it.com\', \'2020-05-26 19:11:19\', \'italian\',\'italian\', \'www.it-airlines.com\',\'air-canada.png\') AS nb';
        $nb = collect(DB::select($sql))->first()->nb;
        //var_dump($nb);
        return $nb;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aerolinea= Airline::find($id);
        $aerolinea->nombre_oficial=$request->get('nombre_oficial');
        $aerolinea->user_id=$request->get('user_id');
        $aerolinea->pais_origen=$request->get('pais_origen');
        $aerolinea->correo_electronico=$request->get('correo_electronico');
        $aerolinea->fecha_fundacion=$request->get('fecha_fundacion');
        $aerolinea->twitter=$request->get('twitter');
        $aerolinea->facebook=$request->get('facebook');
        $aerolinea->sitio_web=$request->get('sitio_web');
        $aerolinea->estado=$request->get('estado');
        $aerolinea->save();
        return $aerolinea;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aerolinea= Airline::find($id);
        $aerolinea->delete();
    }
}
