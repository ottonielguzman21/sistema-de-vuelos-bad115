<?php

namespace App\Http\Controllers;

use App\Classes;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Airline;
use Illuminate\Support\Facades\DB;

class miAerolineaController extends Controller
{
    public function index()
    {
        $user= auth()->user();
        $miAerolinea=Airline::where('user_id',$user->id)->first();
        return view('mi-aerolinea.mi-aerolinea')
            ->with('usuario',$user)
            ->with('aerolinea',$miAerolinea);
    }
    public function vuelos(Request $request){
        $user= auth()->user();
        $miAerolinea=Airline::where('user_id',$user->id)->first();
        $paises=DB::select('call usp_obtenerPaises()');
        $vuelos=(DB::select('call usp_datosVuelosAerolinea(?)',[$miAerolinea->id]));
        $aviones=(DB::select('call usp_avionesAaerolinea(?)',[$miAerolinea->id]));
        $aeropuertos=(DB::select('call usp_aeropuertos()'));
        return ['vuelos'=>$vuelos,
                'idAerolinea'=>$miAerolinea->id,
                'aviones'=>$aviones,
                'aeropuertos'=>$aeropuertos
        ];
    }
    public function darBajaVuelo(Request $respuesta){
        $estado=$respuesta->idEstado;
        $vuelo=$respuesta->idVuelo;
        (DB::select('call usp_darDeBajaVuelo(?,?)',[$vuelo,$estado]));
    }
    public function detalleVuelo(Request $respuesta){
        $id=$respuesta->idVuelo;
        $vuelo=DB::select('call usp_detalleVuelo(?)',[$id]);
        return $vuelo;
    }
    public function actualizarVuelo(Request $respuesta){

        DB::select('call usp_actualizarVuelo(?,?,?,?,?,?,?)',
            [
              $respuesta->idVuelo,
              $respuesta->airplane_id,
              $respuesta->idAeropuertoOrigen,
              $respuesta->idAeropuertoDestino,
              $respuesta->millas,
              $respuesta->fechaIda,
              $respuesta->fechaRegreso
            ]);
    }
    public function crearVuelo(Request $respuesta){
        $user= auth()->user();
        $miAerolinea=Airline::where('user_id',$user->id)->first();
        $idAirplane=$respuesta->airplane_id;
        $aeropuertoOrigen=$respuesta->idAeropuertoOrigen;
        $idAeropuertoDestino=$respuesta->idAeropuertoDestino;
        $millas=$respuesta->millas;
        $fechaIda=$respuesta->fechaIda;
        $fechaRegreso=$respuesta->fechaRegreso;
        DB::select('call usp_crearVuelo(?,?,?,?,?,?,?)',
            [$miAerolinea->id,$idAirplane,$aeropuertoOrigen,$idAeropuertoDestino,$millas,$fechaIda,$fechaRegreso]);
    }
    public function paises(Request $request){
        $paises=DB::select('call usp_obtenerPaises()');
        return $paises;
    }
    public function vuelosParaEscala(Request $respuesta){
        $escalas=DB::select('call usp_vuelosParaEscala(?,?)',
            [
                $respuesta->idVuelo,
                $respuesta->idAerolinea
            ]);
        return $escalas;
    }
    public function asignarEscala(Request $respuesta){
        $resultado=DB::select('call usp_asignarEscala(?,?)',
            [
                $respuesta->idVuelo,
                $respuesta->idEscala
            ]);
        return $resultado;
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
