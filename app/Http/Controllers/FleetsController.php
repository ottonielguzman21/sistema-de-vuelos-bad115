<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Airplane;
use App\Airline;
use App\Fleet;
use DB;

class FleetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $aerolinea =$request->get('airline_id');
        $fleets = Fleet::orderBy('id','DESC')->aerolinea($aerolinea)->paginate(5);
        return view('fleets.index',compact('fleets'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $airplanes = DB::table('airplanes')
            ->whereNotExists(function ($query)
                {
                    $query->select(DB::raw(1))
                    ->from('fleets')
                    ->whereRaw('fleets.airplane_id = airplanes.id');
                })->get();
        $airlines = Airline::all();
        return view('fleets.create', compact('airlines','airplanes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
          'airplane_id'=>'required',
          'airline_id'=>'required',
        ]);

        $flota = Fleet::where('airplane_id', $request->airplane_id)->where('airline_id', $request->airline_id)->exists();
        if ($flota) {
            return redirect()->back()
          ->with('error','¡ERROR!. ¡El avion ya a sido agregado a esa Flota! Registro repetido.');
        }
        $flota = Fleet::create($request->all());
        return redirect()->route('fleets.index')->with('info', '¡Avión agregado a Flora con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fleets = Fleet::find($id);
        return view('fleets.show',compact('fleets'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fleets = Fleet::find($id);
        $airlines = Airline::all();
        $airplanes = Airplane::all();
        return view('fleets.edit', compact('fleets','airplanes','airlines'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
          'airplane_id'=>'required',
          'airline_id'=>'required',
        ]);
        Fleet::find($id)->update($request->all());
        return redirect()->route('fleets.index')->with('info', '¡Flora Actualizada con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
        Fleet::find($id)->delete();
        return redirect()->route('fleets.index')->with('info','Asignacion a Flota eliminada con exito');
        } catch (\Illuminate\Database\QueryException $e) {
            return redirect()->route('fleets.index')->with('error','¡ERROR! La asignacion a flota no se puede borrar!!');
        }
    }
}
