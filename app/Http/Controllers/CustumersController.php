<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Custumer;
use App\Persona;
use App\Companie;
use App\User;
use DB;


class CustumersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {        
        $users = User::all();
        $clientes= Custumer::all();
        $viajero =$request->get('numero_viajero_frecuente');
        $custumers_p = $clientes->where('tipo_cliente', '1');
        $custumers_c = $clientes->where('tipo_cliente', '2');         
        $custumers= Custumer::orderBy('id','DESC')->viajero($viajero)->paginate(10);
        return view('custumers.index',compact('custumers', 'users', 'custumers_p','custumers_c'));
          
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = DB::table('users')
            ->whereNotExists(function ($query)
                {
                    $query->select(DB::raw(1))
                    ->from('custumers')
                    ->whereRaw('custumers.user_id = users.id');
                })->get();
        $persons = Persona::all();
        return view('custumers.create', compact('users','persons'));

    }
    public function create_1()
    {
        $users = DB::table('users')
            ->whereNotExists(function ($query)
                {
                    $query->select(DB::raw(1))
                    ->from('custumers')
                    ->whereRaw('custumers.user_id = users.id');
                })->get();
        return view('custumers.create_1', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
       $this->validate($request,[
          'user_id'=>'required',
          'tipo_documento'=>'required|alpha',
          'tipo_cliente'=>'required|integer',
          'numero_viajero_frecuente'=>'required|alpha_num_spaces',
          'direccion'=>'required|alpha_num_spaces',
          'telefono_fijo'=>'required|integer',
        
          'numero_documento'=>'required|alpha_num_spaces',
          'numero_viajero_frecuente'=>'required|integer',
          'genero'=>'required|alpha',
          'estado_civil'=>'required|string',
          'telefono_movil'=>'required|integer',
        ]);

       $person = Custumer::where('user_id', $request->user_id)->exists();
       $person1 = Persona::where('numero_documento', $request->numero_documento)->exists();
        if($person)
        {
          return redirect()->back()
          ->with('error','¡ERROR!. El Usuario ya se encuentra como Cliente en el Sistema');
        }elseif ($person1) {
            return redirect()->back()
          ->with('error','¡ERROR!. El Numero de Documento Ingresado ya se asigno a un Cliente');
        }

        $custu=Custumer::create([
            'user_id' => $request['user_id'],
            'tipo_documento' => $request['tipo_documento'],
            'tipo_cliente' => $request['tipo_cliente'],
            'numero_viajero_frecuente' => $request['numero_viajero_frecuente'],
            'direccion' => $request['direccion'],
            'telefono_fijo' => $request['telefono_fijo'],
        ]);
        Persona::create([
            'numero_documento' => $request['numero_documento'],
            'genero' => $request['genero'],
            'estado_civil' => $request['estado_civil'],    
            'telefono_movil' => $request['telefono_movil'],
            'custumer_id' => $custu->id,
        ]);
        return redirect()->route('custumers.index')->with('info', 'Cliente Ingresado con éxito');
    }

    public function store_c(Request $request)
    {
       
       $this->validate($request,[
          'user_id'=>'required',
          'tipo_documento'=>'required|alpha',
          'tipo_cliente'=>'required|integer',
          'numero_viajero_frecuente'=>'required|alpha_num_spaces',
          'direccion'=>'required|alpha_num_spaces',
          'telefono_fijo'=>'required|integer',

          'nombre_empresa'=>'required|alpha_num_spaces',
          'nombre_contacto'=>'required|alpha_spaces',
          'NIT'=>'required|integer',
          'NIC'=>'required|integer',
        ]);

       $person = Custumer::where('user_id', $request->user_id)->exists(); //true or false
        if($person)
        {
          return redirect()->back()
          ->with('error','¡ERROR!. El Usuario ya se encuentra como Cliente en el Sistema');
        }
        $comp_nit = Companie::where('NIT', $request->NIT)->exists(); //true or false
        $comp_nic = Companie::where('NIC', $request->NIC)->exists(); //true or false
        if($comp_nit)
        {
          return redirect()->back()
          ->with('error','¡ERROR!. El Cliente con el NIT Ingresado ya se encuentra en el Sistema');
        }elseif ($comp_nic) {
          return redirect()->back()
          ->with('error','¡ERROR!. El Cliente con el NIC Ingresado ya se encuentra en el Sistema');
        }

        $custu=Custumer::create([
            'user_id' => $request['user_id'],
            'tipo_documento' => $request['tipo_documento'],
            'tipo_cliente' => $request['tipo_cliente'],
            'numero_viajero_frecuente' => $request['numero_viajero_frecuente'],
            'direccion' => $request['direccion'],
            'telefono_fijo' => $request['telefono_fijo'],
        ]);
        Companie::create([
            'nombre_empresa' => $request['nombre_empresa'],
            'nombre_contacto' => $request['nombre_contacto'],
            'NIT' => $request['NIT'],    
            'NIC' => $request['NIC'],
            'custumer_id' => $custu->id,
        ]);
        return redirect()->route('custumers.index')->with('info', 'Cliente Ingresado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $custumer = Custumer::find($id);
         if ($custumer->tipo_cliente==1) {
            return view('custumers.show', compact('custumer'));
         }else
         return view('custumers.ver', compact('custumer'));
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $custumer = Custumer::find($id);
        return view('custumers.edit', compact('custumer'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $custumer=Custumer::find($id);

        if ($custumer->tipo_cliente==1) {

             $this->validate($request,[
              'tipo_documento'=>'required|alpha',
              'estado'=>'required|integer',
              'numero_viajero_frecuente'=>'required|alpha_num_spaces',
              'direccion'=>'required|alpha_num_spaces',
              'telefono_fijo'=>'required|integer',

              'numero_documento'=>'required|alpha_num_spaces',
              'genero'=>'required|alpha',
              'estado_civil'=>'required|string',
              'telefono_movil'=>'required|integer',
            ]);

            Custumer::find($id)->update([

                'tipo_documento' => $request['tipo_documento'],
                'estado' => $request['estado'],
                'numero_viajero_frecuente' => $request['numero_viajero_frecuente'],
                'direccion' => $request['direccion'],
                'telefono_fijo' => $request['telefono_fijo'],
            ]);
            Persona::find($custumer->persona->id)->update([
                'numero_documento' => $request['numero_documento'],
                'genero' => $request['genero'],
                'estado_civil' => $request['estado_civil'],    
                'telefono_movil' => $request['telefono_movil'],
            ]);
            return redirect()->route('custumers.index')->with('info', 'Cliente UpdaIngresado con éxito');

         }
         elseif ($custumer->tipo_cliente==2) {

            $this->validate($request,[
            'tipo_documento'=>'required|alpha',
            'estado'=>'required|integer',
            'numero_viajero_frecuente'=>'required|alpha_num_spaces',
            'direccion'=>'required|alpha_num_spaces',
            'telefono_fijo'=>'required|integer',

            'nombre_empresa'=>'required|alpha_num_spaces',
            'nombre_contacto'=>'required|alpha_spaces',
            'NIT'=>'required|integer',
            'NIC'=>'required|integer',
            ]);

        Custumer::find($id)->update([

            'tipo_documento' => $request['tipo_documento'],
            'estado' => $request['estado'],
            'numero_viajero_frecuente' => $request['numero_viajero_frecuente'],
            'direccion' => $request['direccion'],
            'telefono_fijo' => $request['telefono_fijo'],
        ]);
        Companie::find($custumer->compania->id)->update([
            'nombre_empresa' => $request['nombre_empresa'],
            'nombre_contacto' => $request['nombre_contacto'],
            'NIT' => $request['NIT'],    
            'NIC' => $request['NIC'],
        ]);
        return redirect()->route('custumers.index')->with('info', 'Cliente Ingresado con éxito');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
