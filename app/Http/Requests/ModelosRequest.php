<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ModelsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_modelo'=>'required|alpha_num_spaces',
            'brand_id'=>'required|numeric',
            'envergadura'=>'required|numeric',
            'longitud'=>'required|numeric',
            'year'=>'required',
            'peso_vacio'=>'required|integer',
            'peso_max_despegue'=>'required|integer',
            'carga_util'=>'required|integer',
            'factor_carga'=>'required|numeric',
            'velocidad_maxima'=>'required|integer',
            'motor'=>'required|alpha_spaces',
            'autonomia'=>'required|alpha_spaces',
        ];






    }
}
