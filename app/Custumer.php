<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Custumer extends Model
{
    protected $fillable = ['id', 'custumer_id', 'user_id', 'tipo_documento','tipo_cliente','numero_viajero_frecuente','direccion','telefono_fijo','estado'];
    protected $dates = ['created_at','updated_at'];

    public function scopeViajero($query, $viajero)
    {
        return $query->where('numero_viajero_frecuente', 'LIKE', "%$viajero%");
    }

    public function compania(){
        return $this->hasOne('App\Companie', 'custumer_id');
    }

    public function persona(){
        return $this->hasOne('App\Persona', 'custumer_id');
    }

    public function user(){
    return $this->belongsTo('App\User', 'user_id');
 	}

}
