<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airline extends Model
{
    protected $fillable = [
        'user_id',
        'nombre_oficial',
        'pais_origen',
        'nombre_corto',
        'correo_electronico',
        'fecha_fundacion',
        'twitter',
        'facebook',
        'sitio_web',
        'cod_aeroline',
        'url_imagen',
        'estado'
    ];
    public function administrador(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
