<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $fillable = ['id','numero_documento','numero_viajero_frecuente','telefono_fijo','residencia','genero','nombres','apellidos','estado_civil','telefono_movil','custumer_id'];
    protected $dates = ['created_at','updated_at'];

    public function scopeNombre($query, $nombres)
	{
		return $query->where('nombres', 'LIKE', "%$nombres%");
	}
}
