<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $fillable=[
        'name', 'slug', 'description',
    ];
    //query scope
    public function scopeName($query, $name)
    {
        if($name)
            return $query->where('name', 'LIKE', "%$name%");
    }
    public function scopeSlug($query, $slug)
    {
        if($slug)
            return $query->where('slug', 'LIKE', "%$slug%");
    }
    public function scopeDescription($query, $description)
    {
        if($description)
            return $query->where('description', 'LIKE', "%$description%");
    }
}
