<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airplane extends Model
{
    protected $fillable=['modelo_id','capacidad','tipo_avion','estado','nombre_aeroplano'];


    public function scopeTipo($query, $tipo_avion)
	{
		return $query->where('tipo_avion', 'LIKE', "%$tipo_avion%");
	}

	public function modelo()
    {
        return $this->belongsTo('App\Modelo', 'modelo_id');
    }
}
