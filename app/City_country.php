<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City_country extends Model
{
    protected $fillable = ['id', 'city_id', 'country_id'];
    protected $dates = ['created_at','updated_at'];

    public function scopePais($query, $country_id)
    {
        return $query->where('country_id', 'LIKE', "%$country_id%");
    }

    public function city(){
    return $this->belongsTo('App\Citie', 'city_id');
 	}

 	public function country(){
    return $this->belongsTo('App\Countrie', 'country_id');
 	}
 	protected $table = "city_country";
}
