<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder\withPivot;

class Flight extends Model
{
    protected $fillable = [ 'cod_vuelo','airport_origin_id','airport_destination_id','seat_id','millas_distancia', 'fecha_ida','fecha_regreso','duracion_horas','duracion_dias'];
    public $timestamps = false;

}
