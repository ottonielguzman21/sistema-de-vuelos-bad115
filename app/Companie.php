<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companie extends Model
{
    protected $fillable = ['id','custumer_id','telefono_fijo','nombre_empresa','nombre_contacto','NIT','NIC','estado'];
    protected $dates = ['created_at','updated_at'];

    public function scopeNombre($query, $nombre_empresa)
	{
		return $query->where('nombre_empresa', 'LIKE', "%$nombre_empresa%");
	}
}
