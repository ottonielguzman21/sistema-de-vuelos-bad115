<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $fillable = [ 'nombre_clase', 'cantidad_asientos'];
    //query scope
    public function scopeName($query, $name)
    {
        if($name)
            return $query->where('nombre_clase', 'LIKE', "%$name%");
    }
    //query scope
    public function scopeAsiento($query, $asiento)
    {
        if($asiento)
            return $query->where('cantidad_asientos', 'LIKE', "%$asiento%");
    }
}
