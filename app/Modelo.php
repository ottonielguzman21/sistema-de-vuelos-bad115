<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
	    protected $fillable=[
        'brand_id','nombre_modelo', 'envergadura','longitud','year','peso_vacio','peso_max_despegue','carga_util','factor_carga','velocidad_maxima','motor','autonomia'];


     public function scopeNombre($query, $nombre_modelo)
        {
        return $query->where('nombre_modelo', 'LIKE', "%$nombre_modelo%");
        }

	public function brand(){
    return $this->belongsTo('App\Brand', 'brand_id', 'id');
  }
  
}
