<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Countrie extends Model
{
    protected $fillable=['id','nombre_pais','nombre_pais','estado'];


    public function scopeNombre($query, $nombre_pais)
	{
		return $query->where('nombre_pais', 'LIKE', "%$nombre_pais%");
	}
}
