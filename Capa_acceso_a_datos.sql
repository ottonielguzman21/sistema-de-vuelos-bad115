
/*FUNCIONES ALMACENADAS*/

/****************************************************************************************/
/*  Informacion: Esta funcion Permite al administrador de aerolineas poder crear una.   */
/****************************************************************************************/
DELIMITER $$
CREATE OR REPLACE FUNCTION crearAerolinea( user_id int,
                                           nombre_oficial varchar(25),
                                           pais_origen varchar(25),
                                           nombre_corto varchar(15),
                                           correo_electronico varchar(25),
                                           fecha_fundacion date,
                                           twitter varchar(25),
                                           facebook varchar(25),
                                           sitio_web varchar(25),
                                           url_imagen varchar(30)
)
    returns int
BEGIN
    SELECT MAX(id +1) into @siguienteId from vuelosAereos.airlines a;
        -- generacion del codigo de aerolinea
    set @cn = CONCAT(nombre_corto,'%');
    SELECT cod_aeroline into @codigo from vuelosAereos.airlines WHERE cod_aeroline LIKE CAST(@cn AS BINARY) order by id DESC limit 1;
    if @codigo is null then
        set @codGenerado= CONCAT(nombre_corto,'100');
    else
        set @variable=SUBSTRING(@codigo,3,5);
        set @codGenerado= CONCAT(SUBSTRING(nombre_corto,1,2), CAST(@variable as int)+1);
    end if;
    -- inserccion en la tabla
    INSERT into vuelosAereos.airlines (user_id,
                                       nombre_oficial,
                                       pais_origen,
                                       nombre_corto,
                                       correo_electronico,
                                       fecha_fundacion,
                                       twitter,
                                       facebook,
                                       sitio_web,
                                       url_imagen,
                                       cod_aeroline,
                                       updated_at,
                                       created_at)
    values (user_id,
            nombre_oficial,
            pais_origen,
            nombre_corto,
            correo_electronico,
            fecha_fundacion,
            twitter,
            facebook,
            sitio_web,
            url_imagen,
            @codGenerado,
            now(),
            now());
    return  CAST(@siguienteId AS INT);
END
$$
DELIMITER ;
-- SELECT crearAerolinea(6,'Italian Airlines','Italia','IA','italia-airlines@it.com', '2020-05-26 19:11:19', 'italian','italian', 'www.it-airlines.com','air-canada.jpg');

/********************************************************************************************************/
/*  Informacion: Esta funcion permite recuperar el registro de Aeropuertos que cumpla con esos ids      */
/********************************************************************************************************/
CREATE or REPLACE FUNCTION obtAeropuPorPaisCiudad(idPais int, idCiudad int)
RETURNS INT
BEGIN
	SELECT a2.id INTO @_aeropuerto FROM airports a2
	INNER JOIN airport_country ac ON ac.airport_id=a2.id
	INNER JOIN countries cs ON ac.country_id=cs.id
	INNER JOIN city_country co ON co.country_id=cs.id
	INNER JOIN cities cp ON cp.id=co.country_id
	WHERE (cs.id=idPais AND cp.id=idCiudad);

	RETURN CAST(@_aeropuerto AS INT);
END
-- SELECT obtAeropuPorPaisCiudad(1,1);
/*******************************************************************************************************************/
/*  Informacion: Funcion para ver si un vuelo tiene una cantidad de asientos disponibles para realizar una reserva */
/*******************************************************************************************************************/
CREATE or REPLACE FUNCTION function_asientosDisponiblesEnVuelo(cantidad INT, idVuelo INT)
RETURNS INT
BEGIN
	DECLARE _respuesta INT DEFAULT 0;
	  SELECT COUNT(ss.id) INTO @_asientosDisponibles FROM flights f
	  INNER JOIN flight_seat fs ON f.id=fs.flight_id
	  INNER JOIN seats ss ON ss.id=fs.seat_id
	  WHERE f.id=idVuelo AND ss.reservado=0;

	 IF @_asientosDisponibles>=cantidad THEN
	    SET _respuesta :=1;
	 END IF;
	RETURN _respuesta;
END

-- SELECT function_asientosDisponiblesEnVuelo(6,1);
/*FIN FUNCIONES ALMACENADAS*/
/*PROCEDIMIENTOS ALMACENADOS*/

/*******************************************************************************************************************/
/*  Informacion: PROCEDIMIENTO PARA OBTENER TODOS LOS VUELOS QUE LE PERTENEZCAN A UNA AEROLINEA 'X'                */
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_obtenerVuelosPorAerolinea(IN idAerolinea INT)
BEGIN
    SELECT f.cod_vuelo,
           (SELECT m.nombre_modelo from airplanes a2 join modelos m on a2.id=m.id WHERE a2.id=f.airplane_id ) AS Avion,
           (select a3.nombre_aeropuerto from airports a3 where a3.id=f.airport_origin_id) as Origen,
           (select a4.nombre_aeropuerto from airports a4 where a4.id=f.airport_destination_id) as Destino,
           IFNULL((SELECT ai.nombre_aeropuerto from flights f2 join airports ai on f2.airport_origin_id=ai.id where f2.id=f.flight_scale),'Sin Escala') as Escala,
           f.millas_distancia,
           f.fecha_ida, f.fecha_regreso, f.duracion_horas, f.duracion_dias,
           f.estado,
           f.airplane_id, f.airport_destination_id, f.airport_origin_id, f.flight_scale
    FROM airlines a
             JOIN airline_flight e
                  ON a.id=e.airline_id
             JOIN flights f ON f.id=e.flight_id
             JOIN airplanes p ON p.id=f.airplane_id
    WHERE a.id=idAerolinea;
END
-- CALL usp_obtenerVuelosPorAerolinea(1);

/*******************************************************************************************************************/
/*  Informacion: PROCEDIMIENTO PARA OBTENER TODOS LOS VUELOS QUE LE PERTENEZCAN A UN AVION 'x' DE UNA AEROLINEA 'y'*/
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_obtenerVuelosPorAerolineaAvion(IN idAerolinea INT, IN idAvion INT)
BEGIN
SELECT f.cod_vuelo,
       (SELECT m.nombre_modelo from airplanes a2 join modelos m on a2.id=m.id WHERE a2.id=f.airplane_id ) AS Avion,
       (select a3.nombre_aeropuerto from airports a3 where a3.id=f.airport_origin_id) as Origen,
       (select a4.nombre_aeropuerto from airports a4 where a4.id=f.airport_destination_id) as Destino,
       IFNULL((SELECT ai.nombre_aeropuerto from flights f2 join airports ai on f2.airport_origin_id=ai.id where f2.id=f.flight_scale),'Sin Escala') as Escala,
       f.millas_distancia,
       f.fecha_ida, f.fecha_regreso, f.duracion_horas, f.duracion_dias,
       f.estado,
       -- variables de ayuda
       f.airplane_id, f.airport_destination_id, f.airport_origin_id, f.flight_scale
       -- fin de variables de ayuda
FROM airlines a
         JOIN airline_flight e
              ON a.id=e.airline_id
         JOIN flights f ON f.id=e.flight_id
         JOIN airplanes p ON p.id=f.airplane_id
WHERE a.id=idAerolinea AND f.airplane_id=idAvion;
END
-- call usp_obtenerVuelosPorAerolineaAvion(1,1);

/*******************************************************************************************************************/
/*  Informacion: PROCEDIMIENTO PARA AGREGAR ESCALA A UN VUELO YA CREADO                                            */
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_asignarEscala(IN idVuelo INT, IN idEscala INT)
BEGIN
    SELECT f.airport_destination_id INTO @destinoVuelo from vuelosAereos.flights f where id=idVuelo;
    SELECT f2.airport_origin_id INTO @origenEscala from vuelosAereos.flights f2 where id=idEscala;
    IF @destinoVuelo=@origenEscala THEN
        UPDATE vuelosAereos.flights SET flight_scale=idEscala WHERE id=idVuelo;
        SELECT 'Escala asignada con exito!' as Mensaje;
    ELSE
        SELECT 'No es posible agregar la escala, debido a que no coinciden' as Mensaje;

    END IF;
END
-- CALL usp_asignarEscala(1,1);
-- CALL usp_asignarEscala(1,2);

/*******************************************************************************************************************/
/*  Informacion: PROCEDIMIENTO PARA OBTENER TODOS LOS VUELOS QUE TENGAN UN ORIGEN EN ESPECIFICO                    */
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_vuelosEnAeropuerto(IN idAeropuerto INT)
BEGIN
	SELECT * FROM vuelosAereos.flights f WHERE f.airport_origin_id=idAeropuerto;
END
-- call usp_vuelosEnAeropuerto(1);

/*******************************************************************************************************************/
/*  Informacion: ESTE PROCEDIMIENTO ASIGNA UN ROL DE ADMINISTRADOR DE AEROLINEAS A UN USUARIO 'X'                  */
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_asignarRolAdministrador(IN idUsuario INT)
BEGIN
	SELECT COUNT(ru.user_id) INTO @vecesAsignado FROM vuelosAereos.role_user ru WHERE ru.user_id=idUsuario;
	IF @vecesAsignado=0 THEN
	   INSERT INTO vuelosAereos.role_user(role_id,user_id,created_at,updated_at )
	   VALUES (2,idUsuario,now(),now());
	   SELECT 'El rol de administrador de aerolinea ha sido asignado exitosamente!' as Mensaje;
	  ELSE
	    SELECT 'Este usuario ya esta administrando otra aerolinea' as Mensaje;
	   END IF;
END
-- call usp_asignarRolAdministrador(15);
/*******************************************************************************************************************/
/*  Informacion: ESTE PROCEDIMIENTO PERMITE DAR DE BAJA UN VUELO EN ESPECIFICO                                     */
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_darDeBajaVuelo(IN idVuelo INT, IN idEstado INT)
BEGIN
UPDATE flights SET estado=idEstado WHERE id=idVuelo;

END
-- call usp_darDeBajaVuelo(1,0);

/*******************************************************************************************************************/
/*  Informacion: ESTE PROCEDIMIENTO PERMITE OBTENER EL DETALLE DE UN VUELO EN ESPECIFICO                           */
/*******************************************************************************************************************/

CREATE or REPLACE PROCEDURE usp_detalleVuelo(IN idVuelo INT)
begin
    select airplane_id,airport_origin_id as idAeropuertoOrigen,airport_destination_id as idAeropuertoDestino, millas_distancia as millas,
           fecha_ida as fechaIda,fecha_regreso as fechaRegreso
    from flights where id=idVuelo;
end

-- call usp_detalleVuelo(2);

/*******************************************************************************************************************/
/*  Informacion: PROCEDIMIENTO PARA OBTENER TODAS LAS CIUDADES UNICAMENTE SU ID Y NOMBRE                           */
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_obtenerCiudades()
BEGIN
	SELECT c.id,c.nombre_ciudad from cities c;
END

-- call usp_obtenerCiudades();

/*******************************************************************************************************************/
/*  Informacion: PROCEDIMIENTO PARA OBTENER TODOS LOS PAISES POR ID Y NOMBRE                                       */
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_obtenerPaises()
BEGIN
	SELECT p.id, p.nombre_pais from countries p;
END
-- call usp_obtenerPaises();

/*******************************************************************************************************************/
/*  Informacion: PROCEDIMIENTO PARA OBTENER TODOS LOS AVIONES QUE TENGA UNA AEROLINEA REGISTRADA                   */
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_avionesAaerolinea(IN idAerolinea INT)
BEGIN
	select ap.id,m.nombre_modelo FROM airlines a2
	INNER JOIN fleets fl ON a2.id=fl.airline_id
	INNER JOIN airplanes ap ON ap.id=fl.airplane_id
	INNER JOIN modelos m ON m.id=ap.modelo_id
	WHERE (a2.id=idAerolinea AND ap.estado=1);
END
-- call usp_avionesAaerolinea(1);

/*******************************************************************************************************************/
/*  Informacion: PROCEDIMIENTO PARA OBTENER EL ITINERARIO DE UNA AEROLINEA 'X'                                     */
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_itineriarioVuelos(IN idAerolinea int)
BEGIN
	SELECT f.cod_vuelo,
		(SELECT m.nombre_modelo from airplanes a2 join modelos m on a2.id=m.id WHERE a2.id=f.airplane_id ) AS Avion,
		(select a3.nombre_aeropuerto from airports a3 where a3.id=f.airport_origin_id) as Origen,
		(select a4.nombre_aeropuerto from airports a4 where a4.id=f.airport_destination_id) as Destino,
		DATE_FORMAT(f.fecha_ida, "%d/%m/%Y %r") as Fecha_Salida, DATE_FORMAT(f.fecha_regreso, "%d/%m/%Y %r") as Fecha_Llegada,
		IFNULL((SELECT f2.cod_vuelo from flights f2 join airports ai on f2.airport_origin_id=ai.id where f2.id=f.flight_scale),'Sin Escala') as Escala,
		IFNULL((SELECT ai.nombre_aeropuerto from flights f2 join airports ai on f2.airport_destination_id =ai.id where f2.id=f.flight_scale),'Sin Escala') as Destino_Final,
		IFNULL((SELECT DATE_FORMAT(f2.fecha_ida, "%d/%m/%Y %r") from flights f2 join airports ai on f2.airport_origin_id=ai.id where f2.id=f.flight_scale),'Sin Escala') as Fecha_Salida2,
		IFNULL((SELECT DATE_FORMAT(f2.fecha_regreso, "%d/%m/%Y %r") from flights f2 join airports ai on f2.airport_origin_id=ai.id where f2.id=f.flight_scale),'Sin Escala') as Fecha_Llegada2
		FROM airlines a
		JOIN airline_flight e
		ON a.id=e.airline_id
		JOIN flights f ON f.id=e.flight_id
		JOIN airplanes p ON p.id=f.airplane_id
		WHERE (a.id=1 and f.estado=1);
END
-- call usp_itineriarioVuelos(1);

/*******************************************************************************************************************/
/*  Informacion: PROCEDIMIENTO PARA OBTENER TODOS LOS VUELOS SIN ESCALA DE UNA AEROLINEA 'X' QUE CUMPLAN           */
/*               TODOS LOS CRITERIOS DE BUSQUEDA QUE SEAN PROPORCIONADOS POR EL USUARIO.                            */
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_VuelosClienteSinEscala(IN idAerolinea INT,
                                                       IN idClase INT,
                                                       IN fechaInicio DATETIME,
                                                       IN idCountry_origen INT,
                                                       IN idCity_origen INT,
                                                       IN idCountry_destino int,
                                                       IN idCity_destino INT,
                                                       IN cantAdultos INT,
                                                       IN cantAncianos INT)
BEGIN

    DECLARE _fin BOOLEAN DEFAULT 0;
    declare _codigo VARCHAR(6);
    declare _urlImagen VARCHAR(30);
    declare _nombreModelo VARCHAR(25);
    declare _aeropuertoOrigen VARCHAR(25);
    declare _aeropuertoDestino VARCHAR(25);
    declare _nombreClase VARCHAR(15);
    declare _millas INT;
    declare _fechaSalida DATETIME;
    declare _fechaLlegada DATETIME;
    declare _duracionHoras INT;
    declare _duracionDias INT;
    declare _idVuelo INT;
    declare _idAvion INT;
    declare _idAeropuertoOrigen INT;
    declare _idAeropuertoDestino INT;

    DECLARE cur1 CURSOR FOR
        SELECT a.url_imagen,f.cod_vuelo,
               (SELECT m.nombre_modelo FROM airplanes a2 JOIN modelos m ON a2.id=m.id WHERE a2.id=f.airplane_id ) AS Avion,
               (SELECT a3.nombre_aeropuerto FROM airports a3 WHERE a3.id=f.airport_origin_id) AS Origen,
               (SELECT a4.nombre_aeropuerto FROM airports a4 WHERE a4.id=f.airport_destination_id) AS Destino,
               cl.nombre_clase AS Clase,
               f.millas_distancia AS Millas,
               f.fecha_ida AS FechaSalida,
               f.fecha_regreso AS FechaLlegada,
               f.duracion_horas AS HorasVuelo,
               f.duracion_dias AS Dias,
               f.id,
               f.airplane_id,
               f.airport_destination_id,
               f.airport_origin_id
        FROM airlines a
                 JOIN airline_flight e
                      ON a.id=e.airline_id
                 JOIN flights f ON f.id=e.flight_id
                 JOIN airplanes p ON p.id=f.airplane_id
                 JOIN rates rs ON rs.flight_id=f.id
                 JOIN classes cl ON cl.id=rs.class_id
        WHERE (a.id=idAerolinea
            AND f.airport_origin_id=obtAeropuPorPaisCiudad(1,1)
            AND f.airport_destination_id=obtAeropuPorPaisCiudad(idCountry_destino,idCity_destino)
            AND f.fecha_ida>= fechaInicio
            AND cl.id=idClase);

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET _fin :=1;

    DROP TEMPORARY TABLE IF EXISTS VuelosDisponibles;
    CREATE TEMPORARY TABLE VuelosDisponibles(urlImagen VARCHAR(30),codigo VARCHAR(6),nombreModelo VARCHAR(25),aeropuertoOrigen VARCHAR(25),
                                             aeropuertoDestino VARCHAR(25),nombreClase varchar(15), millas INT, fechaSalida DATETIME,fechaLlegada DATETIME, duracionHoras INT,
                                             duracionDias INT,idVuelo INT,idAvion INT,idAeropuertoOrigen INT,idAeropuertoDestino INT);

    OPEN cur1;
    bucle: LOOP
        FETCH cur1 INTO _urlImagen,_codigo,_nombreModelo,
            _aeropuertoOrigen,_aeropuertoDestino,
            _nombreClase,_millas,
            _fechaSalida,_fechaLlegada,
            _duracionHoras,_duracionDias,
            _idVuelo,_idAvion,
            _idAeropuertoOrigen,_idAeropuertoDestino;
        IF _fin THEN
            LEAVE bucle;
        END IF;
        SELECT cantAdultos+cantAncianos INTO @p;
        SELECT function_asientosDisponiblesEnVuelo(@p, _idVuelo) INTO @capacidad;
        IF @capacidad=1 THEN
            INSERT INTO VuelosDisponibles(urlImagen,codigo ,nombreModelo,aeropuertoOrigen,
                                          aeropuertoDestino,nombreClase, millas, fechaSalida,fechaLlegada, duracionHoras,
                                          duracionDias,idVuelo,idAvion,idAeropuertoOrigen,idAeropuertoDestino) VALUES(_urlImagen,_codigo,_nombreModelo,
                                                                                                                      _aeropuertoOrigen,_aeropuertoDestino,
                                                                                                                      _nombreClase,_millas,
                                                                                                                      _fechaSalida,_fechaLlegada,
                                                                                                                      _duracionHoras,_duracionDias,
                                                                                                                      _idVuelo,_idAvion,
                                                                                                                      _idAeropuertoOrigen,_idAeropuertoDestino);
        END IF;
    END LOOP bucle;
    CLOSE cur1;
    SELECT * FROM VuelosDisponibles;
END
-- CALL usp_VuelosClienteSinEscala(1,1,'2020-07-10 10:20:00',1,1,12,12,5,0);

/*******************************************************************************************************************/
/*  Informacion: PROCEDIMIENTO PARA CREAR UN VUELO Y QUE GENERE SUS DATOS                                          */
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_crearVuelo(IN airline_id INT,IN airplane_id INT, IN idAeropuertoOrigen INT,
														IN idAeropuertoDestino INT, IN millas INT,
														IN fechaIda DATETIME, IN fechaRegreso DATETIME)
BEGIN
declare _inicio int DEFAULT 1000;
declare _codVuelo varchar(6);
declare _horas int(10);
declare _dias int(10);
SELECT a.nombre_corto INTO @nombreCorto FROM airlines a WHERE id=airline_id;
SELECT COUNT(fl.cod_vuelo) into @cuenta from airlines a2 inner join airline_flight af on af.airline_id=a2.id inner join flights fl on fl.id=af.flight_id where a2.id=1 order by fl.id desc limit 1;
set _codVuelo := CONCAT(@nombreCorto,(1000+@cuenta));
set _horas :=CAST (HOUR(TIMEDIFF(fechaIda,fechaRegreso)) as int);
set _dias :=_horas/24;
SELECT MAX(id)+1 into @idVuelo from flights;
INSERT INTO flights(cod_vuelo,airplane_id,airport_origin_id,airport_destination_id,
                    millas_distancia,fecha_ida,fecha_regreso,duracion_horas,
                    duracion_dias) VALUES(_codVuelo,airplane_id,idAeropuertoOrigen,idAeropuertoDestino,millas,fechaIda,fechaRegreso,_horas,_dias);

INSERT into airline_flight(airline_id,flight_id,created_at,updated_at)	 values(airline_id,@idVuelo,now(),now());

SELECT * FROM flights ORDER BY id DESC LIMIT 1;
END
-- call usp_crearVuelo(1,1,1,2,150,'2020-07-10 15:20:00','2020-07-15 17:20:00');

/*******************************************************************************************************************/
/*  Informacion: PROCEDIMIENTO PARA ACTUALIZAR LOS DATOS DE UN VUELO                                               */
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_actualizarVuelo(IN idVuelo INT, IN idAvion int, in idAeropuertoOrigen int, in idAeropuertoDestino int, in millas int, in ida datetime, in llegada datetime)
BEGIN
declare _horas int(10);
declare _dias int(10);

set _horas :=CAST (HOUR(TIMEDIFF(ida,llegada)) as int);
set _dias :=_horas/24;

UPDATE flights
set airplane_id=idAvion, airport_origin_id=idAeropuertoOrigen, airport_destination_id=idAeropuertoDestino, millas_distancia=millas,
    fecha_ida=ida, fecha_regreso=llegada,
    duracion_dias= _dias,
    duracion_horas= _horas
where id=idVuelo;
END
-- call usp_actualizarVuelo(2, 1, 3, 3,156,'2020-08-14 10:30:00','2020-08-19 11:30:00')

/*******************************************************************************************************************/
/*  Informacion: PROCEDIMIENTO PARA OBTENER TODOS LOS VUELOS QUE PUEDAN SER ESCALA DE UN VUELO EN ESPECIFICO       */
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_vuelosParaEscala(IN idVuelo INT, IN idAerolinea INT)
BEGIN
    SELECT f1.airport_destination_id, f1.fecha_regreso, ADDDATE(f1.fecha_regreso, INTERVAL 5 hour) into @_destino,@_fechaInicio,@_fechalimite from flights f1 where f1.id=idVuelo;

    SELECT DISTINCT f.id,
                    f.cod_vuelo,
                    (SELECT m.nombre_modelo from airplanes a2 join modelos m on a2.id=m.id WHERE a2.id=f.airplane_id ) as Avion,
                    (SELECT a3.nombre_aeropuerto FROM airports a3 WHERE a3.id=f.airport_origin_id) as Origen,
                    (SELECT a4.nombre_aeropuerto FROM airports a4 WHERE a4.id=f.airport_destination_id ) as Destino,
                    DATE_FORMAT(f.fecha_ida, "%d/%m/%Y %r") as fechaIda,
                    DATE_FORMAT(f.fecha_regreso, "%d/%m/%Y %r") as fechaRegreso from flights f
                                                                                         inner join airline_flight fa on fa.flight_id=f.id
                                                                                         inner join airlines a on a.id=fa.airline_id
    where a.id=idAerolinea and f.estado=1 and f.airport_origin_id=@_destino and f.fecha_ida BETWEEN @_fechaInicio and @_fechalimite;
END
-- call usp_vuelosParaEscala(31,1);

/*******************************************************************************************************************/
/*  Informacion: PROCEDIMIENTO PARA OBTENER TODOS LOS VUELOS: ADMINISTRADOR DE AEROLINEAS                          */
/*******************************************************************************************************************/
CREATE or REPLACE PROCEDURE usp_datosVuelosAerolinea(IN idAerolinea INT)
BEGIN
SELECT f.id,f.cod_vuelo,
       (SELECT m.nombre_modelo FROM airplanes a2 JOIN modelos m ON a2.id=m.id WHERE a2.id=f.airplane_id ) AS Avion,
       (SELECT a3.nombre_aeropuerto FROM airports a3 WHERE a3.id=f.airport_origin_id) AS Origen,
       (SELECT a4.nombre_aeropuerto FROM airports a4 WHERE a4.id=f.airport_destination_id) AS Destino,
       f.duracion_dias,
       f.duracion_horas,
       f.estado,
       DATE_FORMAT(f.fecha_ida, "%d/%m/%Y %r") AS fechaSalida,
       DATE_FORMAT(f.fecha_regreso, "%d/%m/%Y %r") AS fechaRegreso,
       IFNULL((SELECT ai.nombre_aeropuerto from flights f2 JOIN airports ai ON f2.airport_origin_id=ai.id WHERE f2.id=f.flight_scale),'Sin Escala') AS Escala,
       f.millas_distancia,
       f.id AS vueloId,
       idAerolinea as aerolineaId
FROM airlines a2
         INNER JOIN airline_flight ai ON ai.airline_id=a2.id
         INNER JOIN flights f ON f.id=ai.flight_id WHERE a2.id=idAerolinea ORDER BY f.id DESC;
END

/*FIN PROCEDIMIENTOS ALMACENADOS*/
/*INICIO DE TRIGGERS*/

/*******************************************************************************************************************/
/*  Informacion: TRIGGER PARA DAR EL PERMISO DE GESTIONAR VUELOS DE UNA AEROLINEA'                                 */
/*******************************************************************************************************************/
DELIMITER $$
CREATE or REPLACE DEFINER =root@localhost TRIGGER otorgarPermisoGAerolinea AFTER INSERT ON airlines FOR EACH ROW
BEGIN
    SELECT COUNT(a.user_id) INTO @veces FROM vuelosAereos.airlines a WHERE a.user_id=new.user_id LIMIT 1;
    IF @veces=0 THEN
        INSERT INTO vuelosAereos.role_user(role_id,user_id,created_at,updated_at ) VALUES	(2,new.user_id,now(),now());
    end if;
END $$
DELIMITER;
/*FIN DE TRIGGERS*/
