-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: vuelosAereos
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB-1:10.4.13+maria~stretch

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `airline_flight`
--

DROP TABLE IF EXISTS `airline_flight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `airline_flight` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `flight_id` int(10) unsigned NOT NULL,
  `airline_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `airline_flight_airline_id_foreign` (`airline_id`),
  KEY `airline_flight_flight_id_foreign` (`flight_id`),
  CONSTRAINT `airline_flight_airline_id_foreign` FOREIGN KEY (`airline_id`) REFERENCES `airlines` (`id`),
  CONSTRAINT `airline_flight_flight_id_foreign` FOREIGN KEY (`flight_id`) REFERENCES `flights` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airline_flight`
--

LOCK TABLES `airline_flight` WRITE;
/*!40000 ALTER TABLE `airline_flight` DISABLE KEYS */;
INSERT INTO `airline_flight` VALUES (1,1,1,NULL,NULL),(2,4,1,NULL,NULL),(3,3,1,NULL,NULL);
/*!40000 ALTER TABLE `airline_flight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `airlines`
--

DROP TABLE IF EXISTS `airlines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `airlines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `nombre_oficial` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pais_origen` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_corto` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correo_electronico` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_fundacion` date NOT NULL,
  `twitter` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sitio_web` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cod_aeroline` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_imagen` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `airlines_user_id_foreign` (`user_id`),
  CONSTRAINT `airlines_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airlines`
--

LOCK TABLES `airlines` WRITE;
/*!40000 ALTER TABLE `airlines` DISABLE KEYS */;
INSERT INTO `airlines` VALUES (1,21,'Avianca','Mexico','AV','avianca@airlines.com','1999-05-22','avianca','avianca','avianca.com','AV13','avianca.png',1,NULL,NULL),(2,21,'Copa Airlines','Mexico','CA','copa-airlines@support.com','1999-05-04','copa-airlines','copa-airlines','copa-airlines.com','CM211','Copa_Airlines_logo.jpg',0,NULL,'2020-06-30 06:24:38'),(3,21,'Air Canada','Canada','AC','air-canda@help.com','2001-05-15','air-canada','air-canada','air-canada.com','AC21','air-canada.jpg',1,NULL,NULL),(4,21,'Italian Airlines','Italia','IA','italia-airlines@it.com','2020-05-26','italian','italian','www.it-airlines.com','IA100','air-canada.jpg',1,'2020-06-16 23:14:08','2020-06-16 23:14:08'),(5,21,'Italian Airlines','Italia','IA','italia-airlines@it.com','2020-05-26','italian','italian','www.it-airlines.com','IA101','air-canada.jpg',1,'2020-06-16 23:17:06','2020-06-16 23:17:06'),(6,21,'Italian Airlines','Italia','IA','italia-airlines@it.com','2020-05-26','italian','italian','www.it-airlines.com','IA102','air-canada.jpg',1,'2020-06-16 23:28:39','2020-06-16 23:28:39'),(7,21,'Super American','El Salvador','SA','sa@info.com','2020-06-01','super_american','super_americanSV','www.super-american.com','SA100','air-canada.jpg',1,'2020-06-28 05:52:19','2020-06-28 05:52:19'),(8,21,'ssssssssss','El Salvador','UE','italia-airlines@it.com','2020-04-09','italian','ues-sv','www.super-american.com','UE100','air-canada.jpg',1,'2020-06-28 05:56:23','2020-06-28 05:56:23'),(9,4,'Mexico Airline','Italia','MA','italia-airlines@it.com','2020-06-02','super_american','nueva url','sss','MA100','air-canada.jpg',1,'2020-06-28 06:04:48','2020-06-28 06:04:48'),(11,21,'farmacia','El Salvador','UE','sa@info.comss','2020-06-10','ues','ues-sv','www.it-airlines.com','UE101','air-canada.jpg',1,'2020-06-28 06:15:37','2020-06-28 06:15:37'),(12,21,'Mexico Airline','El Salvador','MA','sa@info.com','2020-06-01','ss','ues-sv','www.it-airlines.com','MA101','air-canada.jpg',1,'2020-06-28 06:17:28','2020-06-28 06:17:28'),(13,21,'Italian Airlines','El Salvador','UE','ss@sv.com','2020-03-30','super_american','nueva url','sssss.com','UE102','air-canada.jpg',1,'2020-06-28 06:25:50','2020-06-28 06:25:50'),(14,7,'Italian Airlines','Arabia','SA','sa@info.comss','2020-06-05','ss','nueva url','www.it-airlines.com','SA101','air-canada.jpg',1,'2020-06-28 06:57:39','2020-06-28 06:57:39');
/*!40000 ALTER TABLE `airlines` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER otorgarPermisoGAerolinea AFTER INSERT ON vuelosAereos.airlines FOR EACH ROW
BEGIN 
DECLARE veces INT default 0;
SELECT COUNT(a.user_id) INTO veces FROM vuelosAereos.airlines a WHERE a.user_id=new.user_id;
IF veces=0 THEN
   INSERT INTO vuelosAereos.role_user(role_id,user_id,created_at,updated_at ) VALUES (2,new.user_id,now(),now());
end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `airplanes`
--

DROP TABLE IF EXISTS `airplanes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `airplanes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `modelo_id` int(10) unsigned NOT NULL,
  `capacidad` int(10) unsigned NOT NULL,
  `tipo_avion` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `airplanes_modelo_id_foreign` (`modelo_id`),
  CONSTRAINT `airplanes_modelo_id_foreign` FOREIGN KEY (`modelo_id`) REFERENCES `modelos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airplanes`
--

LOCK TABLES `airplanes` WRITE;
/*!40000 ALTER TABLE `airplanes` DISABLE KEYS */;
INSERT INTO `airplanes` VALUES (1,1,700,'COMERCIAL','2020-05-24 23:31:34',NULL,1),(2,2,700,'COMERCIAL','2020-05-24 23:31:34',NULL,1),(3,3,700,'COMERCIAL','2020-05-24 23:31:34',NULL,1),(4,1,700,'COMERCIAL','2020-05-24 23:31:34',NULL,1),(5,2,700,'COMERCIAL','2020-05-24 23:31:34',NULL,1),(6,3,700,'COMERCIAL','2020-05-24 23:31:34',NULL,1),(7,2,700,'COMERCIAL','2020-05-24 23:31:34',NULL,1),(8,3,700,'COMERCIAL','2020-05-24 23:31:34',NULL,1),(9,3,400,'CARGA','2020-05-26 03:41:52',NULL,1);
/*!40000 ALTER TABLE `airplanes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `airport_country`
--

DROP TABLE IF EXISTS `airport_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `airport_country` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `airport_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `airport_country_airport_id_foreign` (`airport_id`),
  KEY `airport_country_country_id_foreign` (`country_id`),
  CONSTRAINT `airport_country_airport_id_foreign` FOREIGN KEY (`airport_id`) REFERENCES `airports` (`id`),
  CONSTRAINT `airport_country_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airport_country`
--

LOCK TABLES `airport_country` WRITE;
/*!40000 ALTER TABLE `airport_country` DISABLE KEYS */;
INSERT INTO `airport_country` VALUES (1,1,1,NULL,NULL),(2,2,12,NULL,NULL),(3,3,14,NULL,NULL),(4,4,24,NULL,NULL),(5,5,22,NULL,NULL);
/*!40000 ALTER TABLE `airport_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `airports`
--

DROP TABLE IF EXISTS `airports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `airports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_aeropuerto` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `responsable` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estaciones` int(10) unsigned NOT NULL,
  `telefono` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airports`
--

LOCK TABLES `airports` WRITE;
/*!40000 ALTER TABLE `airports` DISABLE KEYS */;
INSERT INTO `airports` VALUES (1,'Monsenor Romero','Ing. Rodolfo Ruiz',25,'22152051',1,NULL,NULL),(2,'Simon Bolivar','Ing. Rodolfo Ruiz',25,'22152051',1,NULL,NULL),(3,'Juan Santa Maria','Pablo Diaz',25,'22152051',1,NULL,NULL),(4,'Charles de Gaulle','Samuel Sanchez',25,'22152051',1,NULL,NULL),(5,'Flughafen Frankfurt','Dr. Armando Castaneda',25,'22152051',1,NULL,NULL);
/*!40000 ALTER TABLE `airports` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booksales`
--

DROP TABLE IF EXISTS `booksales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booksales` (
  `country` varchar(35) DEFAULT NULL,
  `genre` enum('fiction','non-fiction') DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `sales` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booksales`
--

LOCK TABLES `booksales` WRITE;
/*!40000 ALTER TABLE `booksales` DISABLE KEYS */;
INSERT INTO `booksales` VALUES ('Senegal','fiction',2014,12234),('Senegal','fiction',2015,15647),('Senegal','non-fiction',2014,64980),('Senegal','non-fiction',2015,78901),('Paraguay','fiction',2014,87970),('Paraguay','fiction',2015,76940),('Paraguay','non-fiction',2014,8760),('Paraguay','non-fiction',2015,9030);
/*!40000 ALTER TABLE `booksales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_marca` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fabricante` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `brands_nombre_marca_unique` (`nombre_marca`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'Boeing','Wellingtom',1,NULL,NULL),(2,'Quatar','Rockets industries',1,NULL,NULL),(3,'Belmex','Super Lines',1,NULL,NULL);
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_ciudad` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,'San Salvador','2020-05-23 01:55:06','2020-05-23 01:55:06'),(2,'La paz','2020-05-23 01:55:06','2020-05-23 01:55:06'),(3,'Buenos Aires','2020-05-23 01:55:06','2020-05-23 01:55:06'),(4,'Guadalajara','2020-05-23 01:55:06','2020-05-23 01:55:06'),(5,'Río de Janeiro','2020-05-23 01:55:06','2020-05-23 01:55:06'),(6,'Bogotá','2020-05-23 01:55:06','2020-05-23 01:55:06'),(7,'Guayaquil','2020-05-23 01:55:06','2020-05-23 01:55:06'),(8,'Lima','2020-05-23 01:55:06','2020-05-23 01:55:06'),(9,'Montevideo','2020-05-23 01:55:06','2020-05-23 01:55:06'),(10,'Montreal','2020-05-23 01:55:06','2020-05-23 01:55:06'),(11,'Caracas','2020-05-23 01:55:06','2020-05-23 01:55:06'),(12,'Washington D.C.','2020-05-23 01:55:06','2020-05-23 01:55:06'),(13,'San José','2020-05-23 01:55:06','2020-05-23 01:55:06'),(14,'Ciudad de Guatemala','2020-05-23 01:55:06','2020-05-23 01:55:06'),(15,'Kingston','2020-05-23 01:55:06','2020-05-23 01:55:06'),(16,'Managua','2020-05-23 01:55:06','2020-05-23 01:55:06'),(17,'Ciudad de Panama','2020-05-23 01:55:06','2020-05-23 01:55:06'),(18,'San Juan','2020-05-23 01:55:06','2020-05-23 01:55:06'),(19,'Santo Domingo','2020-05-23 01:55:06','2020-05-23 01:55:06'),(20,'Beijin','2020-05-23 01:55:06','2020-05-23 01:55:06'),(21,'Berlin','2020-05-23 01:55:06','2020-05-23 01:55:06'),(22,'Moscu','2020-05-23 01:55:06','2020-05-23 01:55:06'),(23,'Paris','2020-05-23 01:55:06','2020-05-23 01:55:06'),(24,'Tokio','2020-05-23 01:55:06','2020-05-23 01:55:06');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city_country`
--

DROP TABLE IF EXISTS `city_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city_country` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `city_id` int(10) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `city_country_city_id_foreign` (`city_id`),
  KEY `city_country_country_id_foreign` (`country_id`),
  CONSTRAINT `city_country_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `city_country_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city_country`
--

LOCK TABLES `city_country` WRITE;
/*!40000 ALTER TABLE `city_country` DISABLE KEYS */;
INSERT INTO `city_country` VALUES (1,1,1,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(2,2,2,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(3,3,3,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(4,4,4,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(5,5,5,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(6,6,6,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(7,7,7,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(8,8,8,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(9,9,9,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(10,10,10,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(11,11,11,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(12,12,12,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(13,13,13,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(14,14,14,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(15,15,15,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(16,16,16,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(17,17,17,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(18,18,18,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(19,19,19,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(20,20,20,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(21,21,21,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(22,22,22,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(23,23,23,'2020-05-23 01:55:06','2020-05-23 01:55:06'),(24,24,24,'2020-05-23 01:55:06','2020-05-23 01:55:06');
/*!40000 ALTER TABLE `city_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_clase` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cantidad_asientos` int(10) unsigned NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classes`
--

LOCK TABLES `classes` WRITE;
/*!40000 ALTER TABLE `classes` DISABLE KEYS */;
INSERT INTO `classes` VALUES (1,'Primera Clase',25,1,'2020-05-23 23:27:57','2020-05-23 23:27:57'),(2,'Turista',50,1,'2020-05-23 23:29:09','2020-05-23 23:29:11'),(3,'Libre',30,1,'2020-05-25 23:27:53','2020-05-25 23:27:55'),(4,'Sobrecargo',5,1,'2020-05-25 23:29:29','2020-05-25 23:29:31');
/*!40000 ALTER TABLE `classes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `custumer_id` int(10) unsigned NOT NULL,
  `reservationHistory_id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_fijo` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre_empresa` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_contacto` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NIT` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NIC` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `companies_nit_unique` (`NIT`),
  UNIQUE KEY `companies_nic_unique` (`NIC`),
  KEY `companies_custumer_id_foreign` (`custumer_id`),
  KEY `companies_reservationhistory_id_index` (`reservationHistory_id`),
  CONSTRAINT `companies_custumer_id_foreign` FOREIGN KEY (`custumer_id`) REFERENCES `custumers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_pais` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cod_area` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'El Salvador',503,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(2,'Bolivia',591,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(3,'Argentina',503,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(4,'Mexico',54,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(5,'Brasil',55,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(6,'Colombia',57,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(7,'Ecuador',593,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(8,'Perú',51,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(9,'Uruguay',598,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(10,'Canadá',2,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(11,'Venezuela',58,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(12,'Estados Unidos',1,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(13,'Costa Rica',506,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(14,'Guatemala',502,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(15,'Jamaica',876,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(16,'Nicaragua',505,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(17,'Panamá',507,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(18,'Puerto Rico',1787,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(19,'República Dominicana',809,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(20,'China',1025,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(21,'Alemania',756,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(22,'Rusia',653,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(23,'Francia',1001,'2020-05-23 01:55:06','2020-05-23 01:55:06',1),(24,'Japon',125,'2020-05-23 01:55:06','2020-05-23 01:55:06',1);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custumers`
--

DROP TABLE IF EXISTS `custumers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custumers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `tipo_documento` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_cliente` int(10) unsigned NOT NULL,
  `numero_viajero_frecuente` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono_fijo` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `custumers_user_id_foreign` (`user_id`),
  CONSTRAINT `custumers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custumers`
--

LOCK TABLES `custumers` WRITE;
/*!40000 ALTER TABLE `custumers` DISABLE KEYS */;
/*!40000 ALTER TABLE `custumers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fleets`
--

DROP TABLE IF EXISTS `fleets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fleets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `airplane_id` int(10) unsigned NOT NULL,
  `airline_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fleets_airplane_id_foreign` (`airplane_id`),
  KEY `fleets_airline_id_foreign` (`airline_id`),
  CONSTRAINT `fleets_airline_id_foreign` FOREIGN KEY (`airline_id`) REFERENCES `airlines` (`id`),
  CONSTRAINT `fleets_airplane_id_foreign` FOREIGN KEY (`airplane_id`) REFERENCES `airplanes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fleets`
--

LOCK TABLES `fleets` WRITE;
/*!40000 ALTER TABLE `fleets` DISABLE KEYS */;
INSERT INTO `fleets` VALUES (1,1,1,NULL,NULL),(2,3,1,NULL,NULL),(3,8,1,NULL,NULL),(4,1,2,NULL,NULL);
/*!40000 ALTER TABLE `fleets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flight_seat`
--

DROP TABLE IF EXISTS `flight_seat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flight_seat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `flight_id` int(10) unsigned NOT NULL,
  `seat_id` int(10) unsigned NOT NULL,
  `reservado` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `flight_seat_flight_id_foreign` (`flight_id`),
  KEY `flight_seat_seat_id_foreign` (`seat_id`),
  CONSTRAINT `flight_seat_flight_id_foreign` FOREIGN KEY (`flight_id`) REFERENCES `flights` (`id`),
  CONSTRAINT `flight_seat_seat_id_foreign` FOREIGN KEY (`seat_id`) REFERENCES `seats` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flight_seat`
--

LOCK TABLES `flight_seat` WRITE;
/*!40000 ALTER TABLE `flight_seat` DISABLE KEYS */;
INSERT INTO `flight_seat` VALUES (1,1,1,0,NULL,NULL),(2,1,2,0,NULL,NULL),(3,1,3,0,NULL,NULL),(4,1,4,0,NULL,NULL),(5,1,5,0,NULL,NULL),(6,1,6,0,NULL,NULL),(7,3,1,0,NULL,NULL),(8,3,2,0,NULL,NULL),(9,3,3,0,NULL,NULL),(10,3,4,0,NULL,NULL),(11,3,5,0,NULL,NULL);
/*!40000 ALTER TABLE `flight_seat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flights`
--

DROP TABLE IF EXISTS `flights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flights` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cod_vuelo` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `airplane_id` int(10) unsigned NOT NULL,
  `airport_origin_id` int(10) unsigned NOT NULL,
  `airport_destination_id` int(10) unsigned NOT NULL,
  `flight_scale` int(10) unsigned DEFAULT NULL,
  `millas_distancia` int(10) unsigned NOT NULL,
  `fecha_ida` datetime NOT NULL,
  `fecha_regreso` datetime NOT NULL,
  `duracion_horas` int(10) unsigned NOT NULL,
  `duracion_dias` int(10) unsigned NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  KEY `flights_airplane_id_foreign` (`airplane_id`),
  KEY `flights_airport_origin_id_foreign` (`airport_origin_id`),
  KEY `flights_airport_destination_id_foreign` (`airport_destination_id`),
  KEY `flights_flight_scale_foreign` (`flight_scale`),
  CONSTRAINT `flights_airplane_id_foreign` FOREIGN KEY (`airplane_id`) REFERENCES `airplanes` (`id`),
  CONSTRAINT `flights_airport_destination_id_foreign` FOREIGN KEY (`airport_destination_id`) REFERENCES `airports` (`id`),
  CONSTRAINT `flights_airport_origin_id_foreign` FOREIGN KEY (`airport_origin_id`) REFERENCES `airports` (`id`),
  CONSTRAINT `flights_flight_scale_foreign` FOREIGN KEY (`flight_scale`) REFERENCES `flights` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flights`
--

LOCK TABLES `flights` WRITE;
/*!40000 ALTER TABLE `flights` DISABLE KEYS */;
INSERT INTO `flights` VALUES (1,'AV501',1,1,2,2,2100,'2020-07-10 14:20:00','2020-07-18 10:24:34',16,1,1),(2,'AV5001',1,2,5,NULL,1000,'2020-07-01 14:20:00','2020-07-10 14:20:00',25,1,1),(3,'AV5012',1,1,2,NULL,12012,'2020-07-10 15:20:00','2020-07-17 14:20:00',36,1,1),(4,'AP102',1,1,2,NULL,1111,'2020-07-10 10:20:00','2020-07-17 10:20:00',34,2,2),(5,'Av_c',1,1,2,NULL,1000,'2020-07-10 15:20:00','2020-07-12 15:20:00',21,5,1),(6,'AV1003',1,1,2,NULL,125,'2020-07-10 15:20:00','2020-07-12 15:20:00',48,25,1),(7,'AV1003',1,1,2,NULL,125,'2020-07-10 15:20:00','2020-07-12 15:20:00',48,2,1),(8,'AV1003',1,1,2,NULL,125,'2020-07-10 15:20:00','2020-07-14 15:20:00',96,4,1),(9,'AV1003',1,1,2,NULL,125,'2020-07-10 15:20:00','2020-07-15 15:20:00',120,5,1),(10,'AV1003',1,1,2,NULL,125,'2020-07-10 15:20:00','2020-07-15 17:20:00',122,5,1);
/*!40000 ALTER TABLE `flights` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2015_01_20_084450_create_roles_table',1),(4,'2015_01_20_084525_create_role_user_table',1),(5,'2015_01_24_080208_create_permissions_table',1),(6,'2015_01_24_080433_create_permission_role_table',1),(7,'2015_12_04_003040_add_special_role_column',1),(8,'2017_10_17_170735_create_permission_user_table',1),(9,'2020_05_17_235945_create_products_table',1),(10,'2020_05_20_185338_create_airlines_table',1),(11,'2020_05_20_193952_create_airports_table',1),(12,'2020_05_20_195306_create_seats_table',1),(13,'2020_05_20_195828_create_airplanes_table',1),(14,'2020_05_20_200713_create_tickets_table',1),(15,'2020_05_20_202928_create_cities_table',1),(16,'2020_05_20_202928_create_countries_table',1),(17,'2020_05_20_203134_create_custumers_table',1),(18,'2020_05_20_204422_create_companies_table',1),(19,'2020_05_20_205227_create_fleets_table',1),(20,'2020_05_20_224605_create_reservationHistories_table',1),(21,'2020_05_20_230834_create_brands_table',1),(22,'2020_05_20_231403_create_modelos_table',1),(23,'2020_05_20_232823_create_payments_table',1),(24,'2020_05_20_233842_create_classes_table',1),(25,'2020_05_20_234512_create_packages_table',1),(26,'2020_05_20_235213_create_passengers_table',1),(27,'2020_05_21_000013_create_persons_table',1),(28,'2020_05_21_002348_create_city_country_table',1),(29,'2020_05_21_002822_create_airline_flight_table',1),(30,'2020_05_21_003025_create_airport_country_table',1),(31,'2020_05_21_003330_create_reservations_table',1),(32,'2020_05_21_004104_create_rates_table',1),(33,'2020_05_21_004459_create_flights_table',1),(34,'2020_05_21_183603_add_constraint_on_airlines_table',1),(35,'2020_05_21_193538_add_constraint_on_airplanes_table',1),(36,'2020_05_21_200312_add_constraint_on_tickets_table',1),(37,'2020_05_21_202224_add_constraint_on_reservation_histories_table',1),(38,'2020_05_21_203634_add_constraint_on_rates_table',1),(39,'2020_05_21_205606_add_constraint_on_custumers_table',1),(40,'2020_05_21_210453_add_constraint_on_airline_flight_table',1),(41,'2020_05_21_211208_add_constraint_on_airline_reservations_table',1),(42,'2020_05_24_191440_drop__foreing_key_flights',1),(43,'2020_05_24_193403_create_flight_seat_table',1),(44,'2020_05_26_063334_alter_flights_scale_null',1),(45,'2020_05_31_173256_drop_foreing_key_airlines',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelos`
--

DROP TABLE IF EXISTS `modelos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `brand_id` int(10) unsigned NOT NULL,
  `nombre_modelo` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `envergadura` decimal(5,2) unsigned DEFAULT NULL,
  `longitud` decimal(5,2) unsigned NOT NULL,
  `year` int(10) unsigned DEFAULT NULL,
  `peso_vacio` int(10) unsigned DEFAULT NULL,
  `peso_max_despegue` int(10) unsigned DEFAULT NULL,
  `carga_util` int(10) unsigned DEFAULT NULL,
  `factor_carga` decimal(5,2) unsigned DEFAULT NULL,
  `velocidad_maxima` int(10) unsigned NOT NULL,
  `motor` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `autonomia` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `modelos_brand_id_foreign` (`brand_id`),
  CONSTRAINT `modelos_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelos`
--

LOCK TABLES `modelos` WRITE;
/*!40000 ALTER TABLE `modelos` DISABLE KEYS */;
INSERT INTO `modelos` VALUES (1,1,'Boeinging 767',25.60,35.50,2007,525,750,250,25.60,323,'Motor A','Autonomia A','2020-05-23 01:55:06','2020-05-23 01:55:06'),(2,1,'Boeinging 756',25.60,35.50,2007,525,750,250,25.60,323,'Motor B','Autonomia B','2020-05-23 01:55:06','2020-05-23 01:55:06'),(3,1,'Boeinging 985',25.60,35.50,2007,525,750,250,25.60,323,'Motor C','Autonomia Af','2020-05-23 01:55:06','2020-05-23 01:55:06'),(4,2,'Quatar 256',25.60,35.50,2007,525,750,250,25.60,323,'Motor F','Autonomia Ak','2020-05-23 01:55:06','2020-05-23 01:55:06'),(5,2,'Belmex 152',25.60,35.50,2007,525,750,250,25.60,323,'Motor A]','Autonomia AF','2020-05-23 01:55:06','2020-05-23 01:55:06'),(6,1,'Boeinging 201',25.60,35.50,2007,525,750,250,25.60,323,'Motor AF','Autonomia AC','2020-05-23 01:55:06','2020-05-23 01:55:06');
/*!40000 ALTER TABLE `modelos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `packages`
--

DROP TABLE IF EXISTS `packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `airline_id` int(10) unsigned NOT NULL,
  `nombre_paquete` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `porcentaje_descuento` decimal(2,2) unsigned NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `city_id` int(10) unsigned NOT NULL,
  `cantidad_pasajeros` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `packages_country_id_foreign` (`country_id`),
  KEY `packages_city_id_foreign` (`city_id`),
  KEY `packages_airline_id_foreign` (`airline_id`),
  CONSTRAINT `packages_airline_id_foreign` FOREIGN KEY (`airline_id`) REFERENCES `airlines` (`id`),
  CONSTRAINT `packages_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `packages_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `packages`
--

LOCK TABLES `packages` WRITE;
/*!40000 ALTER TABLE `packages` DISABLE KEYS */;
/*!40000 ALTER TABLE `packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `passengers`
--

DROP TABLE IF EXISTS `passengers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `passengers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `primer_nombre` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `segundo_nombre` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primer_apellido` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `segundo_apellido` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `genero` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero_telefono` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pasaporte` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `nacionalidad` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `passengers`
--

LOCK TABLES `passengers` WRITE;
/*!40000 ALTER TABLE `passengers` DISABLE KEYS */;
/*!40000 ALTER TABLE `passengers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  `costo_total` decimal(7,2) unsigned NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,37,2,'2020-06-28 06:15:03','2020-06-28 06:15:03');
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_user`
--

DROP TABLE IF EXISTS `permission_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permission_user_permission_id_index` (`permission_id`),
  KEY `permission_user_user_id_index` (`user_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_user`
--

LOCK TABLES `permission_user` WRITE;
/*!40000 ALTER TABLE `permission_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'Navegar usuarios','users.index','Lista y navega todos los usuarios del sistema','2020-06-27 19:30:38','2020-06-27 19:30:38'),(2,'Crear usuario','users.create','Permite crear usuarios en el sistema','2020-06-27 19:30:38','2020-06-27 19:30:38'),(3,'Ver detalle de usuarios','users.show','Ver en detalle cada rol en el sistema','2020-06-27 19:30:38','2020-06-27 19:30:38'),(4,'Edicion de usuarios','users.edit','Editar Cualquier dato de un usuario del sistema','2020-06-27 19:30:38','2020-06-27 19:30:38'),(5,'Eliminar Usuario','users.destroy','Eliminar cualquier usuario del sistema','2020-06-27 19:30:38','2020-06-27 19:30:38'),(6,'Navegar roles','roles.index','Lista y navega todos los roles del sistema','2020-06-27 19:30:38','2020-06-27 19:30:38'),(7,'Crear rol','roles.create','Permite crear roles en el sistema','2020-06-27 19:30:38','2020-06-27 19:30:38'),(8,'Ver detalle de los roles','roles.show','Ver en detalle cada rol en el sistema','2020-06-27 19:30:38','2020-06-27 19:30:38'),(9,'Edicion de roles','roles.edit','Editar Cualquier dato de un rol del sistema','2020-06-27 19:30:38','2020-06-27 19:30:38'),(10,'Eliminar rol','roles.destroy','Eliminar cualquier rol del sistema','2020-06-27 19:30:38','2020-06-27 19:30:38'),(11,'Navegar productos','products.index','Permite crear produtos en el sistema','2020-06-27 19:30:38','2020-06-27 19:30:38'),(12,'Crear producto','products.create','Lista y navega todos los productos del sistema','2020-06-27 19:30:38','2020-06-27 19:30:38'),(13,'Ver detalle de los productos','products.show','Ver en detalle cada productos en el sistema','2020-06-27 19:30:38','2020-06-27 19:30:38'),(14,'Edicion de productos','products.edit','Editar Cualquier dato de un productos','2020-06-27 19:30:39','2020-06-27 19:30:39'),(15,'Eliminar producto','products.destroy','Eliminar cualquier producto del sistema','2020-06-27 19:30:39','2020-06-27 19:30:39'),(16,'Navegar por la vista empresa','empresa.index','sirve para consultar la empresa','2020-06-27 19:30:39','2020-06-27 19:30:39'),(17,'Navegar permisos','permission.index','Permite crear permisos en el sistema','2020-06-27 19:30:39','2020-06-27 19:30:39'),(18,'Crear permisos','permission.create','permite la creacion de un permiso','2020-06-27 19:30:39','2020-06-27 19:30:39'),(19,'Ver detalle de los permisos','permission.show','Ver en detalle cada permiso en el sistema','2020-06-27 19:30:39','2020-06-27 19:30:39'),(20,'Edicion de permiso','permission.edit','Editar Cualquier dato de un permiso','2020-06-27 19:30:39','2020-06-27 19:30:39'),(21,'Eliminar permiso','permission.destroy','Eliminar cualquier permiso del sistema','2020-06-27 19:30:39','2020-06-27 19:30:39'),(22,'Navegar por la vista Clase','clases.index','sirve para consultar las clases','2020-06-27 19:30:39','2020-06-27 19:30:39'),(23,'Crear Clase','clases.create','Crea una clase','2020-06-27 19:30:39','2020-06-27 19:30:39'),(24,'Ver detalle de la clase','clases.show','Ver en detalle cada clase','2020-06-27 19:30:39','2020-06-27 19:30:39'),(25,'Edicion de clase','clases.edit','Editar Cualquier dato de un clase','2020-06-27 19:30:39','2020-06-27 19:30:39'),(26,'Eliminar clase','clases.destroy','Eliminar cualquier clase del sistema','2020-06-27 19:30:39','2020-06-27 19:30:39'),(27,'Navegar por la vista Destino','destino.index','sirve para consultar los destinos','2020-06-27 19:30:39','2020-06-27 19:30:39'),(28,'Crear Destino','destino.create','Crea un destino','2020-06-27 19:30:39','2020-06-27 19:30:39'),(29,'Ver detalle del destino','destino.show','Ver en detalle cada clase','2020-06-27 19:30:39','2020-06-27 19:30:39'),(30,'Edicion de destino','destino.edit','Editar Cualquier dato de un clase','2020-06-27 19:30:39','2020-06-27 19:30:39'),(31,'Eliminar destino','destino.destroy','Eliminar cualquier clase del destino','2020-06-27 19:30:39','2020-06-27 19:30:39'),(32,'Navegar por la vista Marcas','brands.index','sirve para consultar las Marcas','2020-06-27 19:30:39','2020-06-27 19:30:39'),(33,'Crear Marca','brands.create','Crea una Marca','2020-06-27 19:30:39','2020-06-27 19:30:39'),(34,'Ver detalle de Marca','brands.show','Ver en detalle cada Marca','2020-06-27 19:30:39','2020-06-27 19:30:39'),(35,'Edicion de Marca','brands.edit','Editar Cualquier dato de una Marca','2020-06-27 19:30:40','2020-06-27 19:30:40'),(36,'Eliminar Marca','brands.destroy','Eliminar cualquier Marca de la lista','2020-06-27 19:30:40','2020-06-27 19:30:40'),(37,'Gestion de Aerolineas','aerolineas.index','Permite Gestionar las aerolineas del sistema','2020-06-27 19:30:47','2020-06-27 19:30:47');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persons`
--

DROP TABLE IF EXISTS `persons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `numero_documento` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numero_viajero_frecuente` int(11) DEFAULT NULL,
  `telefono_fijo` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `residencia` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `genero` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombres` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apellidos` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado_civil` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telefono_movil` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `custumer_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `persons_custumer_id_foreign` (`custumer_id`),
  CONSTRAINT `persons_custumer_id_foreign` FOREIGN KEY (`custumer_id`) REFERENCES `custumers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persons`
--

LOCK TABLES `persons` WRITE;
/*!40000 ALTER TABLE `persons` DISABLE KEYS */;
/*!40000 ALTER TABLE `persons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'Fugiat fugit necessitatibus eveniet ut magnam.','Enim ullam id voluptatem mollitia.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(2,'Aliquid et et culpa cumque non suscipit.','Qui est veniam alias quidem.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(3,'Numquam consequuntur ad laboriosam distinctio odio et.','Nihil iusto quo nihil sed assumenda reiciendis.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(4,'Aspernatur suscipit repellat voluptatum eos officiis quibusdam ratione.','Ullam et et quo assumenda nam sequi aut laudantium.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(5,'Adipisci dolor et blanditiis laborum recusandae doloribus quia.','Et mollitia quae consequatur aut placeat.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(6,'At reiciendis doloremque dolor quas sunt cupiditate.','Autem sint illum fugit quis et.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(7,'Molestias praesentium expedita quidem rerum minima.','Saepe consequuntur dicta sunt ut maxime possimus.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(8,'Tenetur alias quas ut labore.','Debitis soluta non nulla ut rem architecto ea.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(9,'Corrupti consequatur fugiat blanditiis consequatur.','Doloribus maiores qui at eius et est.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(10,'Eos nihil commodi quasi quas.','Excepturi animi exercitationem est rem odit magnam magnam.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(11,'Ut architecto similique voluptatum soluta iure.','Odio ad deserunt ratione nisi quas.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(12,'Consequatur culpa sit harum facilis minima et voluptatibus labore.','Earum adipisci eos id eaque quisquam.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(13,'Magni corporis occaecati eligendi cumque ea dolorem vitae rerum.','Tempore necessitatibus in non officia nemo.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(14,'Qui saepe consequatur nihil est.','Commodi recusandae molestiae nesciunt possimus autem hic.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(15,'Perspiciatis et repudiandae in unde quisquam omnis esse.','Rerum repellendus suscipit dolorem et est alias.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(16,'Voluptatem commodi voluptatem non eligendi unde suscipit.','Explicabo adipisci ipsam quo corrupti veritatis animi aut sed.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(17,'Provident est cum modi nisi nisi.','Error velit aut assumenda ratione recusandae aperiam consequatur.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(18,'Ex quasi veritatis delectus et libero optio ducimus quia.','Quaerat sint ut eum sit placeat porro accusamus.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(19,'Ipsam quo illo qui doloremque labore velit corrupti.','Sunt aliquam non est aut fugiat.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(20,'Perspiciatis nihil ea numquam quis in non.','Aut consequatur dolor incidunt nam sed voluptates.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(21,'Explicabo dolor ea dolorem.','Facilis numquam debitis non quod ad et.','2020-06-27 19:30:42','2020-06-27 19:30:42'),(22,'Quo ex animi beatae doloremque nesciunt corporis nam rerum.','Incidunt ducimus expedita at a.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(23,'Ducimus rerum voluptatem veniam ipsum.','Veritatis molestiae est ea voluptatem.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(24,'Quia quo ab ut libero.','Eos tempore ut eos.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(25,'Illum maiores cupiditate voluptatum nostrum.','Reiciendis qui aut occaecati eaque voluptatem dolor.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(26,'Vitae expedita qui error itaque rerum sed.','Eius vel eos temporibus quia.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(27,'Ut unde cumque adipisci.','Hic rerum quia non laboriosam iste commodi.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(28,'Dicta aut accusantium iusto numquam nesciunt ut minus optio.','Cumque facilis sed et praesentium sunt.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(29,'Et voluptatem quaerat molestiae quaerat eaque.','Et placeat optio aut ut velit aut.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(30,'Cum odio impedit aspernatur dolores odio.','Tempore aliquid aspernatur rem voluptatem et omnis velit.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(31,'Hic ea facilis quia atque velit.','Corrupti quia adipisci possimus consequuntur enim.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(32,'Fugit cumque incidunt sint.','Blanditiis provident quod quo ullam fugiat tempora non.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(33,'Doloremque sapiente commodi sapiente in.','At voluptate omnis provident eaque.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(34,'Inventore placeat vero sed autem.','Vitae porro ut sed a laboriosam.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(35,'Dolor aut qui alias alias veritatis.','Earum ea nulla alias sunt inventore aut iste.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(36,'Possimus consequatur qui dolores ipsam.','Dolor qui incidunt dolor veniam quaerat ab mollitia.','2020-06-27 19:30:43','2020-06-27 19:30:43'),(37,'Magni veritatis deserunt quod eligendi et fuga.','Provident officiis numquam deleniti.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(38,'Et dicta sed repellat sapiente nihil.','Dignissimos nam nulla laboriosam pariatur molestias libero.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(39,'Magni commodi quasi nemo sed.','Quibusdam dicta ea quo mollitia dolor.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(40,'Tenetur necessitatibus voluptatem quasi omnis omnis.','In nihil aut officia cumque dolorem neque.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(41,'Hic fugit laborum dignissimos pariatur.','Sed earum aut repellendus repellendus sed maxime aut.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(42,'Minus ex impedit dolore id aut iusto.','Ad atque debitis sit sequi velit saepe.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(43,'Nemo assumenda velit commodi qui consequatur.','Quod sequi saepe dolore quas consequatur.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(44,'Rerum nulla beatae excepturi unde sit illo harum.','Quasi tempore incidunt molestias.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(45,'Qui explicabo eveniet et.','Saepe nulla optio qui.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(46,'Odio est ut eum ad sunt deserunt.','Ut eaque quibusdam quasi alias non cupiditate praesentium.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(47,'Quibusdam repellendus sapiente nihil dolores.','Soluta alias esse sit officiis.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(48,'Hic aspernatur magnam quaerat dolorem odit ullam corporis incidunt.','Eligendi omnis voluptatem debitis.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(49,'Illo ut perferendis suscipit voluptas enim eaque harum.','Rem debitis praesentium nulla optio.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(50,'Et totam nihil qui itaque veniam et magni.','Hic veritatis debitis et dolorem.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(51,'Inventore omnis consequuntur ab suscipit et.','Molestiae id quos vel fugit et.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(52,'Magnam aut qui et fugiat nam amet.','Quidem a et sit voluptate.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(53,'Adipisci nesciunt omnis occaecati sequi ut reiciendis dignissimos.','Aut enim voluptatem eos voluptatem et.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(54,'Iure atque dolor odit neque.','Dolores ut ducimus vel quam suscipit qui.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(55,'Commodi quidem pariatur error minus.','Corporis dolor cupiditate facere at.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(56,'Labore et tempora veniam ut et alias rem.','Similique expedita est id.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(57,'Qui debitis beatae facere hic aut qui.','Placeat illum eum accusantium rerum amet.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(58,'Qui quos possimus explicabo necessitatibus aut qui quod.','Qui explicabo eos similique est inventore ut facilis.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(59,'Assumenda animi odio sed ullam neque corrupti eveniet.','Accusamus expedita iusto ut vero quia.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(60,'Sit quaerat quisquam eum ut atque aut.','Voluptatem est eligendi hic.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(61,'Consequuntur nulla itaque ab eum est deleniti rerum.','Et commodi nesciunt necessitatibus est et.','2020-06-27 19:30:44','2020-06-27 19:30:44'),(62,'Vitae possimus modi in molestiae autem aperiam.','Praesentium ea omnis eos sit.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(63,'Dolorum ex placeat et quia veniam dolorem.','Facilis eos corrupti earum.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(64,'Mollitia quia quisquam repellat placeat accusamus doloribus veritatis.','Reiciendis aut rem ipsum ut ut assumenda earum ut.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(65,'Quo eos illum similique nam quidem quod.','Totam est vitae dicta.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(66,'Quibusdam harum dicta ut doloremque sint.','Sunt quod consequatur dignissimos architecto quis saepe.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(67,'Eos ab et consectetur.','Sapiente ad provident quaerat reprehenderit.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(68,'Qui quae totam id.','Corrupti et quis ea.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(69,'Ducimus culpa natus eum quis mollitia inventore.','Quaerat eos nisi quia voluptatem laborum quam.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(70,'Officia autem maiores alias eum ad eveniet.','Necessitatibus rerum ipsum aliquam quisquam molestiae et dolores.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(71,'Earum nihil reiciendis sunt et.','Aut quis placeat neque velit est cumque accusantium aut.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(72,'Fugiat reprehenderit ut doloribus rerum sed.','Aspernatur sit et totam laboriosam ducimus sed sed.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(73,'Quasi nihil explicabo commodi corporis perferendis.','Hic facere blanditiis magni iusto est magni.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(74,'Perspiciatis sit tenetur eum.','Qui inventore facere velit sit.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(75,'Et ipsam reprehenderit enim non porro omnis voluptatem.','Ipsa vel consequuntur tempore minima animi qui voluptas.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(76,'Sit officiis dolor sequi animi ut modi.','Adipisci iste ratione aut harum quasi.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(77,'Eum reprehenderit omnis illo consequatur ad.','Blanditiis nostrum qui repudiandae et est quasi aperiam asperiores.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(78,'Necessitatibus atque et possimus ad necessitatibus repellendus autem.','Id aut ea eveniet dolorum perspiciatis.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(79,'Voluptatem quia deserunt quos quo alias possimus facilis sit.','Doloribus eveniet beatae recusandae esse cupiditate.','2020-06-27 19:30:45','2020-06-27 19:30:45'),(80,'Temporibus quos veniam sit quod.','Quia maiores laboriosam laudantium.','2020-06-27 19:30:45','2020-06-27 19:30:45');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rates`
--

DROP TABLE IF EXISTS `rates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) unsigned NOT NULL,
  `flight_id` int(10) unsigned NOT NULL,
  `costo` decimal(7,2) unsigned NOT NULL,
  `descripcion` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rates_flight_id_foreign` (`flight_id`),
  KEY `rates_class_id_foreign` (`class_id`),
  CONSTRAINT `rates_class_id_foreign` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`),
  CONSTRAINT `rates_flight_id_foreign` FOREIGN KEY (`flight_id`) REFERENCES `flights` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rates`
--

LOCK TABLES `rates` WRITE;
/*!40000 ALTER TABLE `rates` DISABLE KEYS */;
INSERT INTO `rates` VALUES (1,1,1,356.23,'con un 15% de desc',NULL,NULL),(2,1,3,11.00,'11',NULL,NULL),(3,1,4,252.00,'dsds',NULL,NULL);
/*!40000 ALTER TABLE `rates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservationHistories`
--

DROP TABLE IF EXISTS `reservationHistories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservationHistories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reservation_id` int(10) unsigned NOT NULL,
  `custumer_id` int(10) unsigned NOT NULL,
  `estado_pago` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reservationhistories_reservation_id_foreign` (`reservation_id`),
  KEY `reservationhistories_custumer_id_foreign` (`custumer_id`),
  CONSTRAINT `reservationhistories_custumer_id_foreign` FOREIGN KEY (`custumer_id`) REFERENCES `custumers` (`id`),
  CONSTRAINT `reservationhistories_reservation_id_foreign` FOREIGN KEY (`reservation_id`) REFERENCES `reservations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservationHistories`
--

LOCK TABLES `reservationHistories` WRITE;
/*!40000 ALTER TABLE `reservationHistories` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservationHistories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservations`
--

DROP TABLE IF EXISTS `reservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_id` int(10) unsigned NOT NULL,
  `flight_id` int(10) unsigned NOT NULL,
  `custumer_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reservations_payment_id_foreign` (`payment_id`),
  KEY `reservations_custumer_id_foreign` (`custumer_id`),
  KEY `reservations_flight_id_foreign` (`flight_id`),
  CONSTRAINT `reservations_custumer_id_foreign` FOREIGN KEY (`custumer_id`) REFERENCES `custumers` (`id`),
  CONSTRAINT `reservations_flight_id_foreign` FOREIGN KEY (`flight_id`) REFERENCES `flights` (`id`),
  CONSTRAINT `reservations_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservations`
--

LOCK TABLES `reservations` WRITE;
/*!40000 ALTER TABLE `reservations` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_user_role_id_index` (`role_id`),
  KEY `role_user_user_id_index` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1,21,'2020-06-27 19:30:41','2020-06-27 19:30:41'),(3,2,21,'2020-06-28 06:15:37','2020-06-28 06:15:37'),(4,2,16,'2020-06-29 19:35:44','2020-06-29 19:35:44'),(5,2,19,'2020-06-29 19:39:11','2020-06-29 19:39:11'),(6,2,15,'2020-07-01 06:32:23','2020-07-01 06:32:23');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `special` enum('all-access','no-access') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Admin','admin','Este rol posee todos los permisos en el sistema, unicamente si cuenta con all-access','2020-06-27 19:30:41','2020-06-27 19:30:41','all-access'),(2,'Administrador de Aerolíneas','mi-aerolinea.index','Puede administrar la aerolinea a la cual este como encargado','2020-06-28 06:15:03','2020-06-28 06:15:03',NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seats`
--

DROP TABLE IF EXISTS `seats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fila` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `letra` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reservado` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=300 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seats`
--

LOCK TABLES `seats` WRITE;
/*!40000 ALTER TABLE `seats` DISABLE KEYS */;
INSERT INTO `seats` VALUES (1,'1','A',0,NULL,NULL),(2,'2','A',0,NULL,NULL),(3,'4','A',0,NULL,NULL),(4,'5','A',0,NULL,NULL),(5,'6','A',0,NULL,NULL),(6,'7','A',0,NULL,NULL),(7,'8','A',0,NULL,NULL),(8,'9','A',0,NULL,NULL),(9,'10','A',0,NULL,NULL),(10,'11','A',0,NULL,NULL),(11,'12','A',0,NULL,NULL),(12,'13','A',0,NULL,NULL),(13,'14','A',0,NULL,NULL),(14,'15','A',0,NULL,NULL),(15,'16','A',0,NULL,NULL),(16,'17','A',0,NULL,NULL),(17,'18','A',0,NULL,NULL),(18,'19','A',0,NULL,NULL),(19,'20','A',0,NULL,NULL),(20,'21','A',0,NULL,NULL),(21,'22','A',0,NULL,NULL),(22,'24','A',0,NULL,NULL),(23,'25','A',0,NULL,NULL),(24,'26','A',0,NULL,NULL),(25,'27','A',0,NULL,NULL),(26,'28','A',0,NULL,NULL),(29,'29','A',0,NULL,NULL),(30,'30','A',0,NULL,NULL),(31,'31','A',0,NULL,NULL),(32,'32','A',0,NULL,NULL),(33,'33','A',0,NULL,NULL),(34,'34','A',0,NULL,NULL),(35,'35','A',0,NULL,NULL),(36,'36','A',0,NULL,NULL),(37,'37','A',0,NULL,NULL),(38,'38','A',0,NULL,NULL),(39,'39','A',0,NULL,NULL),(40,'40','A',0,NULL,NULL),(41,'41','A',0,NULL,NULL),(42,'42','A',0,NULL,NULL),(43,'43','A',0,NULL,NULL),(44,'44','A',0,NULL,NULL),(45,'45','A',0,NULL,NULL),(46,'46','A',0,NULL,NULL),(47,'47','A',0,NULL,NULL),(48,'48','A',0,NULL,NULL),(49,'49','A',0,NULL,NULL),(50,'1','B',0,NULL,NULL),(51,'2','B',0,NULL,NULL),(52,'3','B',0,NULL,NULL),(53,'4','B',0,NULL,NULL),(54,'5','B',0,NULL,NULL),(55,'6','B',0,NULL,NULL),(56,'7','B',0,NULL,NULL),(57,'8','B',0,NULL,NULL),(58,'9','B',0,NULL,NULL),(59,'10','B',0,NULL,NULL),(60,'11','B',0,NULL,NULL),(61,'12','B',0,NULL,NULL),(62,'13','B',0,NULL,NULL),(63,'14','B',0,NULL,NULL),(64,'15','B',0,NULL,NULL),(65,'16','B',0,NULL,NULL),(66,'17','B',0,NULL,NULL),(67,'18','B',0,NULL,NULL),(68,'19','B',0,NULL,NULL),(69,'20','B',0,NULL,NULL),(70,'21','B',0,NULL,NULL),(71,'22','B',0,NULL,NULL),(72,'23','B',0,NULL,NULL),(73,'24','B',0,NULL,NULL),(74,'25','B',0,NULL,NULL),(75,'26','B',0,NULL,NULL),(76,'27','B',0,NULL,NULL),(77,'28','B',0,NULL,NULL),(78,'29','B',0,NULL,NULL),(79,'30','B',0,NULL,NULL),(80,'31','B',0,NULL,NULL),(81,'32','B',0,NULL,NULL),(82,'33','B',0,NULL,NULL),(83,'34','B',0,NULL,NULL),(84,'35','B',0,NULL,NULL),(85,'36','B',0,NULL,NULL),(86,'37','B',0,NULL,NULL),(87,'38','B',0,NULL,NULL),(88,'39','B',0,NULL,NULL),(89,'40','B',0,NULL,NULL),(90,'41','B',0,NULL,NULL),(91,'42','B',0,NULL,NULL),(92,'43','B',0,NULL,NULL),(93,'44','B',0,NULL,NULL),(94,'45','B',0,NULL,NULL),(95,'46','B',0,NULL,NULL),(96,'47','B',0,NULL,NULL),(97,'48','B',0,NULL,NULL),(98,'49','B',0,NULL,NULL),(99,'50','B',0,NULL,NULL),(100,'1','D',0,NULL,NULL),(101,'2','D',0,NULL,NULL),(102,'3','D',0,NULL,NULL),(103,'4','D',0,NULL,NULL),(104,'5','D',0,NULL,NULL),(105,'6','D',0,NULL,NULL),(106,'7','D',0,NULL,NULL),(107,'8','D',0,NULL,NULL),(108,'9','D',0,NULL,NULL),(109,'10','D',0,NULL,NULL),(110,'11','D',0,NULL,NULL),(111,'12','D',0,NULL,NULL),(112,'13','D',0,NULL,NULL),(113,'14','D',0,NULL,NULL),(114,'15','D',0,NULL,NULL),(115,'16','D',0,NULL,NULL),(116,'17','D',0,NULL,NULL),(117,'18','D',0,NULL,NULL),(118,'19','D',0,NULL,NULL),(119,'20','D',0,NULL,NULL),(120,'21','D',0,NULL,NULL),(121,'22','D',0,NULL,NULL),(122,'23','D',0,NULL,NULL),(123,'24','D',0,NULL,NULL),(124,'25','D',0,NULL,NULL),(125,'26','D',0,NULL,NULL),(126,'27','D',0,NULL,NULL),(127,'28','D',0,NULL,NULL),(128,'29','D',0,NULL,NULL),(129,'30','D',0,NULL,NULL),(130,'31','D',0,NULL,NULL),(131,'32','D',0,NULL,NULL),(132,'33','D',0,NULL,NULL),(133,'34','D',0,NULL,NULL),(134,'35','D',0,NULL,NULL),(135,'36','D',0,NULL,NULL),(136,'37','D',0,NULL,NULL),(137,'38','D',0,NULL,NULL),(138,'39','D',0,NULL,NULL),(139,'40','D',0,NULL,NULL),(140,'41','D',0,NULL,NULL),(141,'42','D',0,NULL,NULL),(142,'43','D',0,NULL,NULL),(143,'44','D',0,NULL,NULL),(144,'45','D',0,NULL,NULL),(145,'46','D',0,NULL,NULL),(146,'47','D',0,NULL,NULL),(147,'48','D',0,NULL,NULL),(148,'49','D',0,NULL,NULL),(149,'50','D',0,NULL,NULL),(150,'1','E',0,NULL,NULL),(151,'2','E',0,NULL,NULL),(152,'3','E',0,NULL,NULL),(153,'4','E',0,NULL,NULL),(154,'5','E',0,NULL,NULL),(155,'6','E',0,NULL,NULL),(156,'7','E',0,NULL,NULL),(157,'8','E',0,NULL,NULL),(158,'9','E',0,NULL,NULL),(159,'10','E',0,NULL,NULL),(160,'11','E',0,NULL,NULL),(161,'12','E',0,NULL,NULL),(162,'13','E',0,NULL,NULL),(163,'14','E',0,NULL,NULL),(164,'15','E',0,NULL,NULL),(165,'16','E',0,NULL,NULL),(166,'17','E',0,NULL,NULL),(167,'18','E',0,NULL,NULL),(168,'19','E',0,NULL,NULL),(169,'20','E',0,NULL,NULL),(170,'21','E',0,NULL,NULL),(171,'22','E',0,NULL,NULL),(172,'23','E',0,NULL,NULL),(173,'24','E',0,NULL,NULL),(174,'25','E',0,NULL,NULL),(175,'26','E',0,NULL,NULL),(176,'27','E',0,NULL,NULL),(177,'28','E',0,NULL,NULL),(178,'29','E',0,NULL,NULL),(179,'30','E',0,NULL,NULL),(180,'31','E',0,NULL,NULL),(181,'32','E',0,NULL,NULL),(182,'33','E',0,NULL,NULL),(183,'34','E',0,NULL,NULL),(184,'35','E',0,NULL,NULL),(185,'36','E',0,NULL,NULL),(186,'37','E',0,NULL,NULL),(187,'38','E',0,NULL,NULL),(188,'39','E',0,NULL,NULL),(189,'40','E',0,NULL,NULL),(190,'41','E',0,NULL,NULL),(191,'42','E',0,NULL,NULL),(192,'43','E',0,NULL,NULL),(193,'44','E',0,NULL,NULL),(194,'45','E',0,NULL,NULL),(195,'46','E',0,NULL,NULL),(196,'47','E',0,NULL,NULL),(197,'48','E',0,NULL,NULL),(198,'49','E',0,NULL,NULL),(199,'50','E',0,NULL,NULL),(200,'1','G',0,NULL,NULL),(201,'2','G',0,NULL,NULL),(202,'3','G',0,NULL,NULL),(203,'4','G',0,NULL,NULL),(204,'5','G',0,NULL,NULL),(205,'6','G',0,NULL,NULL),(206,'7','G',0,NULL,NULL),(207,'8','G',0,NULL,NULL),(208,'9','G',0,NULL,NULL),(209,'10','G',0,NULL,NULL),(210,'11','G',0,NULL,NULL),(211,'12','G',0,NULL,NULL),(212,'13','G',0,NULL,NULL),(213,'14','G',0,NULL,NULL),(214,'15','G',0,NULL,NULL),(215,'16','G',0,NULL,NULL),(216,'17','G',0,NULL,NULL),(217,'18','G',0,NULL,NULL),(218,'19','G',0,NULL,NULL),(219,'20','G',0,NULL,NULL),(220,'21','G',0,NULL,NULL),(221,'22','G',0,NULL,NULL),(222,'23','G',0,NULL,NULL),(223,'24','G',0,NULL,NULL),(224,'25','G',0,NULL,NULL),(225,'26','G',0,NULL,NULL),(226,'27','G',0,NULL,NULL),(227,'28','G',0,NULL,NULL),(228,'29','G',0,NULL,NULL),(229,'30','G',0,NULL,NULL),(230,'31','G',0,NULL,NULL),(231,'32','G',0,NULL,NULL),(232,'33','G',0,NULL,NULL),(233,'34','G',0,NULL,NULL),(234,'35','G',0,NULL,NULL),(235,'36','G',0,NULL,NULL),(236,'37','G',0,NULL,NULL),(237,'38','G',0,NULL,NULL),(238,'39','G',0,NULL,NULL),(239,'40','G',0,NULL,NULL),(240,'41','G',0,NULL,NULL),(241,'42','G',0,NULL,NULL),(242,'43','G',0,NULL,NULL),(243,'44','G',0,NULL,NULL),(244,'45','G',0,NULL,NULL),(245,'46','G',0,NULL,NULL),(246,'47','G',0,NULL,NULL),(247,'48','G',0,NULL,NULL),(248,'49','G',0,NULL,NULL),(249,'50','G',0,NULL,NULL),(250,'1','H',0,NULL,NULL),(251,'2','H',0,NULL,NULL),(252,'3','H',0,NULL,NULL),(253,'4','H',0,NULL,NULL),(254,'5','H',0,NULL,NULL),(255,'6','H',0,NULL,NULL),(256,'7','H',0,NULL,NULL),(257,'8','H',0,NULL,NULL),(258,'9','H',0,NULL,NULL),(259,'10','H',0,NULL,NULL),(260,'11','H',0,NULL,NULL),(261,'12','H',0,NULL,NULL),(262,'13','H',0,NULL,NULL),(263,'14','H',0,NULL,NULL),(264,'15','H',0,NULL,NULL),(265,'16','H',0,NULL,NULL),(266,'17','H',0,NULL,NULL),(267,'18','H',0,NULL,NULL),(268,'19','H',0,NULL,NULL),(269,'20','H',0,NULL,NULL),(270,'21','H',0,NULL,NULL),(271,'22','H',0,NULL,NULL),(272,'23','H',0,NULL,NULL),(273,'24','H',0,NULL,NULL),(274,'25','H',0,NULL,NULL),(275,'26','H',0,NULL,NULL),(276,'27','H',0,NULL,NULL),(277,'28','H',0,NULL,NULL),(278,'29','H',0,NULL,NULL),(279,'30','H',0,NULL,NULL),(280,'31','H',0,NULL,NULL),(281,'32','H',0,NULL,NULL),(282,'33','H',0,NULL,NULL),(283,'34','H',0,NULL,NULL),(284,'35','H',0,NULL,NULL),(285,'36','H',0,NULL,NULL),(286,'37','H',0,NULL,NULL),(287,'38','H',0,NULL,NULL),(288,'39','H',0,NULL,NULL),(289,'40','H',0,NULL,NULL),(290,'41','H',0,NULL,NULL),(291,'42','H',0,NULL,NULL),(292,'43','H',0,NULL,NULL),(293,'44','H',0,NULL,NULL),(294,'45','H',0,NULL,NULL),(295,'46','H',0,NULL,NULL),(296,'47','H',0,NULL,NULL),(297,'48','H',0,NULL,NULL),(298,'49','H',0,NULL,NULL),(299,'50','H',0,NULL,NULL);
/*!40000 ALTER TABLE `seats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reservation_id` int(10) unsigned NOT NULL,
  `estado_pago` tinyint(1) NOT NULL DEFAULT 0,
  `passenger_id` int(10) unsigned NOT NULL,
  `tipo_boleto` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tickets_serial_unique` (`serial`),
  KEY `tickets_passenger_id_foreign` (`passenger_id`),
  KEY `tickets_reservation_id_foreign` (`reservation_id`),
  CONSTRAINT `tickets_passenger_id_foreign` FOREIGN KEY (`passenger_id`) REFERENCES `passengers` (`id`),
  CONSTRAINT `tickets_reservation_id_foreign` FOREIGN KEY (`reservation_id`) REFERENCES `reservations` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tickets`
--

LOCK TABLES `tickets` WRITE;
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Sydnie Wintheiser','danny91@example.com','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','EE0GFW4dph','2020-06-27 19:30:40','2020-06-27 19:30:40'),(2,'Dr. Juston Wintheiser','myra94@example.org','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','9i5bfs2yVO','2020-06-27 19:30:40','2020-06-27 19:30:40'),(3,'Gerson Torp Sr.','feichmann@example.org','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','FCDyHRWErg','2020-06-27 19:30:40','2020-06-27 19:30:40'),(4,'Maggie Heaney','reichel.emil@example.net','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','nXSN2Dipdx','2020-06-27 19:30:40','2020-06-27 19:30:40'),(5,'Jarrell Bailey','emilie14@example.org','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','S2lBSaC0vO','2020-06-27 19:30:40','2020-06-27 19:30:40'),(6,'Bernard Corkery I','leif.jacobs@example.org','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','aykEr2Ip3a','2020-06-27 19:30:40','2020-06-27 19:30:40'),(7,'Zoila Windler','jacobs.jaleel@example.net','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','ZQtB2aneev','2020-06-27 19:30:41','2020-06-27 19:30:41'),(8,'Miss Frederique Kiehn','zane49@example.com','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','omptPVIETn','2020-06-27 19:30:41','2020-06-27 19:30:41'),(9,'Mr. Jordy Hand IV','kurt.blanda@example.com','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','yELHZVBLbl','2020-06-27 19:30:41','2020-06-27 19:30:41'),(10,'Felicity DuBuque','stiedemann.gino@example.net','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','tOL2ki0HGg','2020-06-27 19:30:41','2020-06-27 19:30:41'),(11,'Kenny Emmerich','shammes@example.com','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','XCmcOv4uYs','2020-06-27 19:30:41','2020-06-27 19:30:41'),(12,'Amir Corwin','pbotsford@example.com','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','8wwTlTBtKN','2020-06-27 19:30:41','2020-06-27 19:30:41'),(13,'Felipa Mosciski','effertz.london@example.org','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','8EINxyXQ8N','2020-06-27 19:30:41','2020-06-27 19:30:41'),(14,'Dr. Dejuan Lubowitz','meghan.stoltenberg@example.org','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','nrPVxOVMbK','2020-06-27 19:30:41','2020-06-27 19:30:41'),(15,'Kara Bashirian','amorar@example.org','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','lzAf9dZQHu','2020-06-27 19:30:41','2020-06-27 19:30:41'),(16,'Jada Ortiz','gulgowski.isabel@example.org','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','mneLV3VDP7','2020-06-27 19:30:41','2020-06-27 19:30:41'),(17,'Deja Paucek','hjacobson@example.net','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','56pIwQjdns','2020-06-27 19:30:41','2020-06-27 19:30:41'),(18,'Shaylee Erdman','kuhlman.penelope@example.com','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','wJur8PDNVs','2020-06-27 19:30:41','2020-06-27 19:30:41'),(19,'Prof. Idella Auer','mcglynn.dora@example.com','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','FUAxmqockC','2020-06-27 19:30:41','2020-06-27 19:30:41'),(20,'Kamryn Armstrong','dicki.rae@example.com','2020-06-27 19:30:40','$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi','Al01yL4PBO','2020-06-27 19:30:41','2020-06-27 19:30:41'),(21,'Edwin Otoniel Guzman','gm12006@ues.edu.sv',NULL,'$2y$10$CozKB4JPQeGyw.EVW3QOvO9pWuviS68cdElLLnV86kd0XDHuHXy6y',NULL,'2020-06-27 19:30:41','2020-06-27 19:30:41');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-02 14:58:48
